## v0.7.1 (June 10, 2021)

### Admin Changes

* Logging enhancements and correctly dial local syslog
  ([!188](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/188),
  [!178](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/178)).
* Support overriding data directory within RunAs workflow
  ([!187](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/187)).
  
### Bug & Development Fixes

* Correct RPMs builds on RHEL 8/CentOS 8/Fedora
  ([!186](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/186)).
* Upgrade to Go version to *1.16.5*
  ([!189](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/189)).
* Target `arm64` if platform `aarch64` in Spack package
  ([!190](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/190)).
* Log JWT verification step
  ([!174](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/174)).
* Gather and log all RunAs validation script output
  ([!191](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/191)).

## v0.7.0 (May 26, 2021)

### Admin Changes

* Support for variable data_dir expanded in downscoped user's
  environment ([!168](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/168)).
* `jacamar-auth` ProcessID captured during logging
  ([!173](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/173)).
* Jacamar CI RPM is now relocatable
  ([!184](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/184)).

### Bug & Development Fixes

* Added Jacamar CI logo
  ([!179](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/179)).
* Interactions with GitLab's JWKS (JSON Web Key Set) now correctly handled
  directly by the `gitlabjwt` package
  ([!172](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/172)).
* Added support for runner release *13.12*
  ([!180](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/180)).
* Extend seccomp functionality to support testing a block-all by default model
  ([!175](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/175)).
* Basic high-level panic recovery
  ([!171](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/171)).
* Support quick Go test of specific packages within Docker
  ([!176](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/176)).
* Extended CI build artifact duration to 1 day
  ([!181](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/181)).
* Updated runner generated test data scripts
  ([!182](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/182)).

## v0.6.0 (April 29, 2021)

### Admin Changes

* Improved Slurm executor by utilizing `sbatch --wait` as opposed to relying
  on `sacct`
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143),
  [!164](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/164)).
* Extensive improvement to the Git command modification process
  ([!154](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/154)).
* Cobalt corrections to address issues reported with version *0.5.0* testing
  ([!135](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/135),
  [!150](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/150)).
* Remove support for `source_script` in the `[auth]` configuration
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143)).
* Correctly build JWKS URL when server found in path
  ([!157](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/157)).
* In order to use `jacamar` directly without `jacamar-auth`, the
  application will only function as expected with the
  `--no-auth` command, signifying no authorization is expected
  ([!160](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/160)).

### Bug & Development Fixes

* Added support for Runner release *13.11* and *13.10*
  ([!161](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/161),
  [!138](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/138)).
* The authorization workflow will now validate the supplied JSON
  Web Token at each stage of the job
  ([!147](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/147)).
* Added support for `make rpm-docker` and `make runner-docker` commands
  to build related RPMs locally via Docker
  ([!138](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/138),
  [!137](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/137)).
* The `internal/command` packages structure has been improved to better
  differentiate between dowscoping and standard execution methods
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143)).
* The first steps have been taken to improve the `internal/authuser`
  packages structure, more work will follow
  ([!160](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/160)).
* Added ``ModifyCmd(string, string...)`` to Runner and Commander interface
  ([!150](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/150)).
* Added the project path as a stateful variable
  ([!153](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/153)).
* Added support for `downscope = "su"` as it nearly follow the same
  command structure as `sudo`
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143)).
* Added optional `downscope_env` to the `[auth]` configuration
  ([!149](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/149))
* Migrated Pavilion testing from `sudo` to `su` due to oddities in
  testing `sudo -E su ...` commands in CI
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143)).
* Updated command `make test-docker` to align with other corrections
  ([!140](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/140/)).
* Updates to directory identification (in package ``authuser`) to align
  across builds and script directories
  ([!148](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/148)).
* Corrected ``command`` handling of goroutines causing blocking actions
  ([!165](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/165)).
* Configurable command directory
  ([!162](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/162)).

## v0.5.0 (April 05, 2021)

### Admin Changes

* Overhaul of supplementary group identification and expand testing
  to verify desired functionality for use with capabilities
  as well as all other supported downscoping mechanism
  ([!127](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/127)).
* New downscope option `downscope = "sudo"` added which invokes a crafted
  `sudo su` command that is used to subsequently launch a user-owned
  `jacamar` application process
  ([!103](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/103)).

  - As with much of Jacamar this is still under active development, and we
    do not advise use in production without extensive testing. Please report
    any issues you may experience when using this new functionality.

* Support for libseccomp added with configurable blocked system calls,
  and defaults established for `jacamar-auth`
  ([!119](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/119)).
* Corrected `SIGTERM` monitoring and command context handling that led
  to high CPU usage across a number of deployments
  ([!117](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/117)).
* Additional testing and error handling for Slurm
  ([!105](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/105>).
* Data directory configurations now support environment variables
  (`data_dir = "/$GPFS/.ci"`) resolved by the user
  ([!118](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/118)).
* Improved Git ASKPASS credential creation and remove helper from local
  `.git/config` into `$CI_PROJECT_DIR/../.credential` folder
  ([!124](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/124)).
* RunAs validation support override of the target `gitlab_account` used in
  conjunction with the CI Token Broker
  ([!122](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/122)).
* Fully qualify `jacamar` application when generating downscope command
  ([!101](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/101)).
* Improve support for `downscope = "none"` to allow single user deployment
  to leverage authorization level features
  ([!110](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/110)).
* Identify Bash shell in command creation, with optional configuration support
  ([!107](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/107)).
* Clarified error message associated with `data_dir` creation and expanded
  related testing
  ([!123](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/123)).
* Updated Slurm job script with `--login` prior to execution
  ([!131](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/131)).
* Create Slurm output files in advance of job submission as opposed to
  allowing `sbatch` to create its own
  ([!132](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/132)).

### Bug & Development Fixes

* Allow hyphens in username rules
  ([!106](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/106)).
* Updated all Pavilion2
  tests to leverage new functionality and provide more fine-tuned testing
  both local and in CI pipelines
  ([!126](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/126),
  [!130](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/130),
  [!129](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/129)).
* Support for Go 1.16
  ([!113](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/113)).
* Improve bytes buffer for command execution
  ([!104](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/104)).
* Removed support + usage of `jacamar-plugins`
  ([!115](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/115)).
* Updated OLCF testing pipeline
  ([!120](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/120)).
* Removed outdated ALCF testing pipeline and supporting files
  ([!112](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/112)).
* Tests added to ensure `source_script` functionality
  ([!121](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/121)).
* Test to verify credentials removal
  ([!111](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/111)).
* Check broker token response for expected patterns
  ([!109](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/109)).
* Remove translation test files upon completion
  ([!108](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/108)).

## v0.4.2 (February 24, 2021)

### Admin Changes

* When using the supported RPM, `jacamar-auth` application is now deployed
  to `/opt/jacamar/bin` to best support `CAP_SETUID`/`CAP_SETGID`
  workflows
  ([!97](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/97))
* Configurable `root_dir_creation = true` in Jacamar-Auth supports
  creation of top level user owned directory by a privileged user
  ([!99](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/99)).
* Support for Runner versions 13.8 and removal of federation patch
  ([!96](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/96)).

### Bug & Development Fixes

* Target server host for script augmentation of target URLs
  ([!98](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/98)).
* Improved static analysis and linting during CI process
  ([!95](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/95),
  [!94](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/94)).

## v0.4.1 (February 16, 2021)

### General Changes

* Added *CheckJWT* to the *rules* package
  ([!91](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/91)).

### Admin Changes

* Support new enhanced broker registration requirements
  ([!93](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/93)).
* Update GitLab-Runner to version `13.8.0`, with minimized
  patching requirements for testing purposes
  ([!90](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/90)).

### Bug & Development Fixes

* Correctly remove `CI_JOB_JWT` from user environment only when federation or
  broker service is enabled
  ([!89](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/89)).

## v0.4.0 (January 25, 2021)

### General Changes

* Migrated two internal packages to now support a public API for key
  functionality used across multiple EPC CI projects
  ([!58](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/58)).
* Addresses potential case in which failed Cobalt/Slurm jobs did
  not observe a defined NFS timeout/delay
  ([!81](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/81)).

### Admin Changes

* The cleanup stage (`clean` subcommand) will now require the configuration_
  file be provided with the `--configuration` argument
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71)).
* Integrated all Federation information/context into the RunAs validation
  scripts, removing Federation only scripting requirements
  ([!65](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/65),
  [!86](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/86)).
* Pre-RunAs list (allow/block) validation can now optionally be enabled via the
  configuration ([!68](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/68)).
* Jacamar subcommand (`jacamar translate`) added to translate a runner
  configuration from the forked deployment to a currently observed
  Jacamar configuration
  ([!63](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/63)).
* Added experimental plugin support for RunAs validation
  ([!76](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/76)).
* `jacamar--auth` now supports an `--unobfuscated` flag that
  allows all errors to appear regardless of stage.
  ([!83](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/83))
* Update supported GitLab-Runner to version `13.7.0`, with minimized
  patching requirements
  ([!72](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/72)).
* Added configurable timeout for Jacamar-Auth to wait before sending `SIGKILL`
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71)).
* Updated OLCF focused testing structure for Ascent
  ([!62](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/61)).

### Bug & Development Fixes

* Updated to Go release 1.15.6
  ([!64](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/64)).
* Correctly establish default values for configuration_ in all cases
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71)).
* Cleaner error handling within `jacamar-auth` to enforce default obfuscation
  for any potentially sensitive error
  ([!74](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/74)).
* Correctly leverage `_prefix` macro in RPM spec
  ([!79](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/79)).
* Added Go/GCC to Pavilion containers and updated associated test
  scripts to build Jacamar requirements
  ([!78](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/78),
  [!80](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/80)).
* System logging of `jacamar-auth` now handled through the Logrus
  packages ([!82](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/82)).
* Improve RPM related make commands
  ([!85](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/85)).

## v0.3.2 (December 11, 2020)

### Bug & Development Fixes

* Correctly monitor and wait on background child processes to
  complete that have been initiated by a user’s script
  ([!59](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/59)).
* Use Go’s `net/url` package when identifying target host for broker
  interactions.
