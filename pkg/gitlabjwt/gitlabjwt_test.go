package gitlabjwt

import (
	"bytes"
	"crypto/rsa"
	"errors"
	"math/big"
	"net/http"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_httpclient"
)

type jwtTest struct {
	client    *mock_httpclient.MockHTTPClient
	key       *rsa.PublicKey
	encoded   string
	jobID     string
	serverURL string
	targetEnv map[string]string

	assertError     func(*testing.T, error)
	assertValidator func(*testing.T, Validator)
	assertClaims    func(*testing.T, Claims)
}

const (
	// testingJWT header/signature correct but missing claims.
	testingJWT = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJqb2JfaWQiOiIxMjMiLCJmZWRlcmF0ZWRfdXNlcm5hbWUiOiJmZWRfdXNlciIsImF1dGhfdG9rZW4iOiJ0b2szbiIsInN1YiI6ImpvYl8xMjMiLCJleHAiOjMyNDkxODIwMTYwfQ.Z9Y1O4eyvhwtzH4O0WSPzenbbcmrmBI6Mr_ZYKRuiQJBfJmwZmQBUOqEYyNh1X39CuKUXEzdA8d2f8ewwxuDBgyHAvmzC0bFD7vTcemcPhVGnG_H_z2iRJn_dYwwhznl0-wbNFEPqWunjCsliQaLCsUrulKOzhfNHEusYS-ZZgQtyOL7Q0hied2JjWByVXDD1ebXKyOG8j-Y5A9iwhhd2y6-e8Y-Zi4ta_g4PlSkD7PixFnpKkuJpcEa20Uu6UkI9_cTEaqnCptrPEu2hwbI8yRCEGAnwFA-xG7BWaPQSNkZybpcgXO8zNAMpnfdicOf2ExryYJl5j75cTTIUIsy3A"
	// workingJWT header/signature/claims correct.
	workingJWT = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyIsImV4cCI6MzI0OTE4MjAxNjB9.WHWvWckMj-ZaK6tolpKCs4XJtlVwmSdnmyTk5F3Aqw4hHK-tzk9JrfLf1MeWtXpU1H2ZTUTCKjD57wYBZsLoy5sHySd5cny_ao_MwJMbsE7s9pKEilr_4o4S57aL9-2pD56jt4w1lezgic9gQXxjCXuiPVNpCZqixUPimZ2lD3Vm1Hy2PTc8lTlHozKtdq8pttvBA1TlgiDOMxmn0DaPdFs59iBhvaGCeWhB3ZMcARDtOQxtwhBHo0Th9gquGxXxDCYoxI6c_lNhWoAlSeD3fbWp_oT8XjEb5V5se7-plxOHaJSWaWPkw6Kx17T1VyvH9ddY17HBEW7crGIYiYrGHw"
	// badSubject header/signature correct but subject (sub) incorrect in claims.
	badSubject = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzQ1NiIsImV4cCI6MzI0OTE4MjAxNjB9.T5S8J5wvGrm0KrmjOxaz9-jVpXT9iaNnEXJOxz9kfPUqFkPsLPPZnyk8UG1c2YKyToUxqr58tO0jU-F_VdM3YJA-8PSeTn2aUaN3koLQpaPu7DjFSdohbqEC_H4R58Fcmn_ygAWRpzn0G3VTcyjNFQOwIED9aONlf0xQ19whFQiqRGqMadFHReMCHvTfd3eieOkGNlNlNXLIGaVPJu0eHmGo-GVTGMZNISKG6Mf4UfqE8s7D4_6AF4CTPXR9AH6i1aQfXjfeIX162fFRHF-GG4zpJD_tiIf35vrXuqe5_rAjRDuT-gatOY5hC1yMZumIIUQ_G66CeD7f7jCYa-X-Bg"
)

func TestFactory(t *testing.T) {
	tests := map[string]jwtTest{
		"properly formatted CI_JOB_JWT": {
			jobID:     "1",
			serverURL: "https://gitlab.example.gov/",
			encoded:   "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
			assertValidator: func(t *testing.T, v Validator) {
				assert.NotNil(t, v)
				assert.Equal(t, "https://gitlab.example.gov/-/jwks", v.(job).jwksURL)
			},
		},
		"server URL does not have backslash suffix": {
			jobID:     "1",
			serverURL: "https://gitlab.example.gov",
			encoded:   "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
			assertValidator: func(t *testing.T, v Validator) {
				assert.NotNil(t, v)
				assert.Equal(t, "https://gitlab.example.gov/-/jwks", v.(job).jwksURL)
			},
		},
		"server specified in URL path": {
			serverURL: "https://example.gov/gitlab/",
			assertValidator: func(t *testing.T, v Validator) {
				assert.NotNil(t, v)
				assert.Equal(t, "https://example.gov/gitlab/-/jwks", v.(job).jwksURL)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := Factory(tt.encoded, tt.jobID, tt.serverURL)

			if tt.assertValidator != nil {
				tt.assertValidator(t, got)
			}
		})
	}
}

func TestTrustedFactory(t *testing.T) {
	tests := map[string]jwtTest{
		"properly formatted CI_JOB_JWT": {
			serverURL: "https://gitlab.example.gov/",
			encoded:   "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
			assertValidator: func(t *testing.T, v Validator) {
				assert.NotNil(t, v)
				assert.Equal(t, "https://gitlab.example.gov/-/jwks", v.(job).jwksURL)
			},
		},
		"server URL does not have backslash suffix": {
			serverURL: "https://gitlab.example.gov",
			encoded:   "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
			assertValidator: func(t *testing.T, v Validator) {
				assert.NotNil(t, v)
				assert.Equal(t, "https://gitlab.example.gov/-/jwks", v.(job).jwksURL)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := TrustedFactory(tt.encoded, tt.serverURL)

			if tt.assertValidator != nil {
				tt.assertValidator(t, got)
			}
		})
	}
}

// https://groups.google.com/g/golang-nuts/c/J-Y4LtdGNSw?pli=1
type ClosingBuffer struct {
	*bytes.Buffer
}

func (cb *ClosingBuffer) Close() error {
	return nil
}

func Test_job_Parse(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_httpclient.NewMockHTTPClient(ctrl)
	m.EXPECT().Get(gomock.Eq("test.bad/-/jwks")).Return(&http.Response{
		Status:     "400 Bad Request",
		StatusCode: http.StatusBadRequest,
		Body: &ClosingBuffer{
			bytes.NewBufferString(`{"error":"400 Bad Request"}`),
		},
	}, nil).AnyTimes()
	m.EXPECT().Get(gomock.Eq("test.error/-/jwks")).Return(nil, errors.New("error msg")).AnyTimes()
	// Return test key with malformed field (int as opposed to string)
	m.EXPECT().Get(gomock.Eq("test.decoder/-/jwks")).Return(&http.Response{
		Status:     "200 Success",
		StatusCode: http.StatusOK,
		Body: &ClosingBuffer{
			bytes.NewBufferString(`{"keys":[{"kty":"123","kid":"decoder","e":"?","n":"?","use":"sig","alg":256}]}`),
		}},
		nil).AnyTimes()
	// Return two test keys with the kid = test
	m.EXPECT().Get(gomock.Eq("test.keys/-/jwks")).Return(&http.Response{
		Status:     "200 Success",
		StatusCode: http.StatusOK,
		Body: &ClosingBuffer{bytes.NewBufferString(
			`{"keys":[{"kty":"RSA","kid":"test","e":"ab","n":"ab","use":"sig","alg":"RS256"},{"kty":"RSA","kid":"test","e":"ab","n":"ab","use":"sig","alg":"RS256"}]}`),
		}},
		nil).AnyTimes()
	// Return test key with alg = rs512
	m.EXPECT().Get(gomock.Eq("test.alg/-/jwks")).Return(&http.Response{
		Status:     "200 Success",
		StatusCode: http.StatusOK,
		Body: &ClosingBuffer{bytes.NewBufferString(
			`{"keys":[{"kty":"RSA","kid":"test","e":"AQAB","n":"nzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA-kzeVOVpVWwkWdVha4s38XM_pa_yr47av7-z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr_Mrm_YtjCZVWgaOYIhwrXwKLqPr_11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e-lf4s4OxQawWD79J9_5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa-GSYOD2QU68Mb59oSk2OB-BtOLpJofmbGEGgvmwyCI9Mw","use":"sig","alg":"RS512"}]}`),
		}},
		nil).AnyTimes()
	m.EXPECT().Get(gomock.Eq("test.pass/-/jwks")).Return(&http.Response{
		Status:     "200 Success",
		StatusCode: http.StatusOK,
		Body: &ClosingBuffer{bytes.NewBufferString(
			`{"keys":[{"kty":"RSA","kid":"test","e":"AQAB","n":"nzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA-kzeVOVpVWwkWdVha4s38XM_pa_yr47av7-z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr_Mrm_YtjCZVWgaOYIhwrXwKLqPr_11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e-lf4s4OxQawWD79J9_5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa-GSYOD2QU68Mb59oSk2OB-BtOLpJofmbGEGgvmwyCI9Mw", "use":"sig","alg":"RS256"}]}`),
		}},
		nil).AnyTimes()
	m.EXPECT().Get(gomock.Eq("test.no-exponent/-/jwks")).Return(&http.Response{
		Status:     "200 Success",
		StatusCode: http.StatusOK,
		Body: &ClosingBuffer{bytes.NewBufferString(
			`{"keys":[{"kty":"RSA","kid":"test","n":"nzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA-kzeVOVpVWwkWdVha4s38XM_pa_yr47av7-z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr_Mrm_YtjCZVWgaOYIhwrXwKLqPr_11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e-lf4s4OxQawWD79J9_5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa-GSYOD2QU68Mb59oSk2OB-BtOLpJofmbGEGgvmwyCI9Mw", "use":"sig","alg":"RS256"}]}`),
		}},
		nil).AnyTimes()
	m.EXPECT().Get(gomock.Eq("test.no-modulus/-/jwks")).Return(&http.Response{
		Status:     "200 Success",
		StatusCode: http.StatusOK,
		Body: &ClosingBuffer{bytes.NewBufferString(
			`{"keys":[{"kty":"RSA","kid":"test","e":"AQAB","use":"sig","alg":"RS256"}]}`),
		}},
		nil).AnyTimes()
	m.EXPECT().Get(gomock.Eq("test.bad-modulus/-/jwks")).Return(&http.Response{
		Status:     "200 Success",
		StatusCode: http.StatusOK,
		Body: &ClosingBuffer{bytes.NewBufferString(
			`{"keys":[{"kty":"RSA","kid":"test","e":"AQAB","n":"@T3ST_-",use":"sig","alg":"RS256"}]}`),
		}},
		nil).AnyTimes()

	tests := map[string]jwtTest{
		"invalid base64 encoded jwt provided": {
			encoded: "eyJ:)y.eyJoZWx.sm0r7FO",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},

		"unable to Unmarshal header, integer provided instead of string": {
			encoded: "eyJhbGciOiJQUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6MTIzNH0.segment.segment",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unable to validate JWT header: json: cannot unmarshal number into Go struct field Header.kid of type string",
				)
			},
		},
		"unsupported header 'alg' defined": {
			encoded: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEyMzQifQ.segment.segment",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unable to validate JWT header: Key: 'Header.Alg' Error:Field validation for 'Alg' failed on the 'startswith' tag",
				)
			},
		},
		"malicious header 'kid' defined": {
			encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IiQobWFsaWNpb3VzKSJ9.segment.segment",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unable to validate JWT header: Key: 'Header.Kid' Error:Field validation for 'Kid' failed on the 'kid' tag",
				)
			},
		},
		"no header 'kid' defined": {
			encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.segment.segment",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unable to validate JWT header: Key: 'Header.Kid' Error:Field validation for 'Kid' failed on the 'kid' tag",
				)
			},
		},
		"jwks endpoint bad request": {
			encoded:   testingJWT,
			serverURL: "test.bad/-/jwks",
			client:    m,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to retrieve key from JWKS endpoint: request for JWKS failed with status 400",
				)
			},
		},
		"https request error encountered": {
			encoded:   testingJWT,
			serverURL: "test.error/-/jwks",
			client:    m,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to retrieve key from JWKS endpoint: unable to retrieve response from test.error/-/jwks: error msg",
				)
			},
		},
		"https request successfully, decoder error encountered": {
			encoded:   testingJWT,
			serverURL: "test.decoder/-/jwks",
			client:    m,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"kid identified but incorrect alg specified": {
			encoded:   testingJWT,
			serverURL: "test.alg/-/jwks",
			client:    m,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to retrieve key from JWKS endpoint: algorithm expected by JWKS (RS512) does not match JWT (RS256)",
				)
			},
		},
		"no E (exponent) found in keys": {
			encoded:   workingJWT,
			serverURL: "test.no-exponent/-/jwks",
			jobID:     "123",
			client:    m,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to retrieve key from JWKS endpoint: failed to identify public key, verify JWKS algorithm type")
			},
		},
		"no N (modulus) found in keys": {
			encoded:   workingJWT,
			serverURL: "test.no-modulus/-/jwks",
			jobID:     "123",
			client:    m,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to retrieve key from JWKS endpoint: failed to identify public key, verify JWKS algorithm type")
			},
		},
		"bad N (modulus) found in keys": {
			encoded:   workingJWT,
			serverURL: "test.bad-modulus/-/jwks",
			jobID:     "123",
			client:    m,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to retrieve key from JWKS endpoint: failed decode, invalid character 'u' looking for beginning of object key string")
			},
		},
		"successfully parse jwt and claims": {
			encoded:   workingJWT,
			serverURL: "test.pass/-/jwks",
			jobID:     "123",
			client:    m,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertClaims: func(t *testing.T, c Claims) {
				assert.Equal(t, Claims{
					UserLogin:   "user",
					UserEmail:   "user@example.com",
					ProjectPath: "group/project",
					ProjectID:   "2002",
					JobID:       "123",
					PipelineID:  "456",
					UserID:      "789",
					FedUserName: "fed_user",
					AuthToken:   "tok3n",
					NamespaceID: "1001",
					Subject:     "job_123",
					StandardClaims: jwt.StandardClaims{
						ExpiresAt: 32491820160,
					},
				}, c)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			j := job{
				encoded: tt.encoded,
				jobID:   tt.jobID,
				jwksURL: tt.serverURL,
				checkID: true,
			}
			if tt.client != nil {
				client = tt.client
			}

			got, err := j.Parse()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertClaims != nil {
				tt.assertClaims(t, got)
			}
		})
	}
}

func Test_job_parseClaims(t *testing.T) {
	n := new(big.Int)
	n, _ = n.SetString("20101790993208644745807976729182597941929355612162354360099435269825087678371993244844234893013558555686015831335725398637423399304205115261083991022355813201997154499053064318477614909646953959855907663206692927300016800053636628573275271404089122405985685162285559162700174320318326821436949689956974724260182115938767812249391575639780973664572557729842107578524708525191776956150194917696738395922018602710772475751229671360413648976296942707837850780316509559008920087532825564663621482064344153450826739561548502662708814824842358869389530164169290288156380027449103702069177196558531588515097343487007237750067", 10)
	public := &rsa.PublicKey{
		N: n,
		E: 65537,
	}

	tests := map[string]jwtTest{
		"successfully parse jwt and claims": {
			encoded: workingJWT,
			jobID:   "123",
			key:     public,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertClaims: func(t *testing.T, c Claims) {
				assert.Equal(t, Claims{
					UserLogin:   "user",
					ProjectPath: "group/project",
					JobID:       "123",
					FedUserName: "fed_user",
					AuthToken:   "tok3n",
					Subject:     "job_123",
					NamespaceID: "1001",
					ProjectID:   "2002",
					UserEmail:   "user@example.com",
					PipelineID:  "456",
					UserID:      "789",
					StandardClaims: jwt.StandardClaims{
						ExpiresAt: 32491820160,
					},
				}, c)
			},
		},
		"different job id provided via jwt and runner": {
			encoded: testingJWT,
			jobID:   "456",
			key:     public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "stolen JWT detected")
			},
		},
		"invalid job claims in jwt": {
			encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6InNsdWcvdGVzdCIsImpvYl9pZCI6MTIzLCJleHAiOjMyNDkxODIwMTYwfQ.GCZ02BpMdbUkXrXCd1zBZWWco7p8QMPXjRdxCb9ICGeGxxtxF4eUeYgjz-5XFLyLUuL3JbxuT5atm8ckMR_DUr6Ami6knDGFvWUjHD007RT9lUghBdqDOp--2eTNRbI4ivwU87oC-sfKx9F9X9CTTN85fLQesx6STm7GQRzHMFJCmMN2p0fdxAZMQcmoSJioe7YYLxPvNyu4ylHYZHA6WJ0FzEwUj23R1Cwm74H59b4O47DSILH8T49lEJRvp_WGYkFPhREnldYcQtWX0BJHL-hrpYfUy6WjQdviPj_X7jnKmsG5madX_dkvWDRRQ_0SLfeqiQlG6pqpAEjjMGlnoA",
			jobID:   "123",
			key:     public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"json: cannot unmarshal number into Go struct field Claims.job_id of type string",
				)
			},
		},
		"expired jwt provided": {
			encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6InNsdWcvdGVzdCIsImpvYl9pZCI6IjEyMyIsImV4cCI6OTY2NzI0OTY4fQ.jYxEuuvZx-ULXSke3R06RE7nZx3hm7_zkGzmqHYlUcNcrghMqanaMEMNQRT-frl6-JHRjyJt-Mf5ZR51GPh9FGdbKO5v0qkVNhzkyKswdf89sipJdibspihHhpYf4K9KHsjo-yyoNGU6AwMjYDPOs5pL1iOvtT9-_YCkUOqPJ2Caru7qurLbEXJiGQ_EETORm49SnSAvRHBwz70hVi4AIvwfyYOumL6kV811pBsRM6ZLcexo4SO01iwzlf3P37iyZgi6LFNEyQ8KAP0Qd3qXbX2dAR8YG15yKbi25iWNk-0FyvxCK7CaK21zHhonCZ41crFibPwm5w-az0yug0nd0w",
			jobID:   "123",
			key:     public,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"empty jwt payload": {
			encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.e30.GFqmrJJBN_fEZCFUS6u0k395XN3u4diHxc1ThpcyaGffhpR-hzL7I5XlugJFwtWy53f2y01r_pchLJmj30v_7bZLhgASTWo5DO_dKJVqrKq1JCjRGhOqy00EPTH9Oxq2RAlzpR9x3SeFTbrYe-xgbSQEJypcPTzh8o0c9gb1gXUSSWdysxTzwNOkyyuAviUpwc678hol5ofm2tlk-IiUOKmzz4bwZFBYaR28XdQav2HAlqM0kv6qUdfHpG4UtUFCLrb1aORqdeMQsY-4B1lcl4JV1Cz2vxiai0HVbcvqcTXEG_RSK7GQ_9ulqW_LTVGoqDzZDbhK44LfVBBh-I6MPg",
			key:     public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to establish GitLabClaims from JWT")
			},
		},
		"incorrect jwt subject": {
			encoded: badSubject,
			jobID:   "123",
			key:     public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"Key: 'Claims.Subject' Error:Field validation for 'Subject' failed on the 'sub' tag",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			j := job{
				encoded: tt.encoded,
				jobID:   tt.jobID,
				checkID: true,
			}

			got, err := j.parseClaims(tt.key)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertClaims != nil {
				tt.assertClaims(t, got)
			}
		})
	}
}
