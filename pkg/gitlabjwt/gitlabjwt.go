// Package gitlabjwt maintains a supported workflow for validating both the signature and
// other important claims found in the CI_JOB_JWT. This JWT is validated against your
// GitLab instances JWKS endpoint (e.g., https://gitlab.com/-/jwks) and the package
// will only function with JWTs whose subject is the specific CI job.
//
// Example code:
//
//      func VerifyJWT(encoded, jobID string) (gitlabjwt.Claims, error) {
//          gj := gitlabjwt.Factory(
//  			encoded,
//	    		jobID,
//		    	"https://gitlab.com",
//		    )
//			return gj.Parse()
//      }
//
package gitlabjwt

import (
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-playground/validator/v10"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/rules"
)

// Validator implements support for parsing and interacting with a GitLab supplied JWT.
type Validator interface {
	// Parse verifies the signature of a CI job JWT as well as the validity of all claims
	// in the payload by ensuring their expected structure.
	Parse() (Claims, error)
}

// Claims details CI job level information established based upon a verified CI_JOB_JWT.
type Claims struct {
	// AuthToken is an optional token established during federation by the auth source.
	AuthToken string `json:"auth_token" validate:"authToken"`
	// FedUserName is an optional username established during federation by the auth source.
	FedUserName string `json:"federated_username" validate:"username"`
	// JobID is the CI job ID provided by the associated GitLab server.
	// Compared against `ci_job_id` in the broker jwt.
	JobID string `json:"job_id" validate:"required,numeric" env:"JWT_JOB_ID"`
	// NamespaceID is the GitLab numerical CI namespace (e.g. group/sub-group) ID.
	NamespaceID string `json:"namespace_id" validate:"required,numeric" env:"JWT_NAMESPACE_ID"`
	// PipelineID is the GitLab numerical CI pipeline ID.
	PipelineID string `json:"pipeline_id" validate:"required,numeric" env:"JWT_PIPELINE_ID"`
	// ProjectID is the GitLab numerical project ID.
	ProjectID string `json:"project_id" validate:"required,numeric" env:"JWT_PROJECT_ID"`
	// ProjectPath is the GitLab project path (e.g. group/project)
	ProjectPath string `json:"project_path" validate:"projectPath" env:"JWT_PROJECT_PATH"`
	// Subject (sub) identifies the primary purpose of the jwt.
	Subject string `json:"sub" validate:"sub"`
	// UserEmail is the primary email account for the user who triggered the job.
	UserEmail string `json:"user_email" validate:"email" env:"JWT_USER_EMAIL"`
	// UserLogin is the GitLab username of the user who triggered the job.
	UserLogin string `json:"user_login" validate:"required,username" env:"JWT_USER_LOGIN"`
	// UserID is the GitLab numerical user ID.
	UserID string `json:"user_id" validate:"required,numeric" env:"JWT_USER_ID"`

	// StandardClaims - https://tools.ietf.org/html/rfc7519#section-4.1
	jwt.StandardClaims
}

// Header represents key aspects of a JWT's header segment used during validation.
type Header struct {
	Alg string `json:"alg" validate:"startswith=RS,len=5,alphanum"`
	Kid string `json:"kid" validate:"kid"`
	Typ string `json:"typ" validate:"eq=JWT"`
}

type WebKey struct {
	Alg string    `json:"alg"`
	Kid string    `json:"kid"`
	E   *exponent `json:"e"`
	N   *modulus  `json:"n"`
}

type WebKeySet struct {
	Keys []WebKey `json:"keys"`
}

type job struct {
	encoded string
	jobID   string
	jwksURL string
	checkID bool
}

type httpClient interface {
	Get(url string) (*http.Response, error)
}

var (
	client httpClient
	v      *validator.Validate
)

// Factory generates a Validator for the environmentally supplied encoded JWT.
// A trusted Job ID as well base server URL (https://gitlab.example.gov)\
// must be supplied as it won't be verified. The jobID is used to ensure the
// provided JWT aligns with a second source.
func Factory(encoded, jobID, serverURL string) Validator {
	j := factory(encoded, jobID, serverURL)
	j.checkID = true
	return j
}

// TrustedFactory generates a Validator for the environmentally supplied encoded JWT.
// No secondary validation (e.g., Job ID) is required since this JWT is either
// trusted or supplied without any additional references.
func TrustedFactory(encoded, serverURL string) Validator {
	return factory(encoded, "", serverURL)
}

func factory(encoded, jobID, serverURL string) job {
	u, _ := url.Parse(serverURL)
	u.Path = strings.TrimRightFunc(u.Path, func(r rune) bool {
		return r == '/'
	}) + "/-/jwks"

	return job{
		encoded: encoded,
		jobID:   jobID,
		jwksURL: u.String(),
	}
}

func (j job) Parse() (Claims, error) {
	header, err := checkHeader(j.encoded)
	if err != nil {
		return Claims{}, fmt.Errorf("unable to validate JWT header: %w", err)
	}

	key, err := j.fetchKey(header.Kid, header.Alg)
	if err != nil {
		return Claims{}, fmt.Errorf("unable to retrieve key from JWKS endpoint: %w", err)
	}

	return j.parseClaims(key)
}

func (j job) fetchKey(kid, alg string) (interface{}, error) {
	resp, err := client.Get(j.jwksURL)
	if err != nil {
		return nil, fmt.Errorf("unable to retrieve response from %s: %w", j.jwksURL, err)
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("request for JWKS failed with status %d", resp.StatusCode)
	}

	defer resp.Body.Close()
	jwks := new(WebKeySet)
	if err = json.NewDecoder(resp.Body).Decode(jwks); err != nil {
		return nil, fmt.Errorf("failed decode, %w", err)
	}
	key := jwks.findFirstKey(kid)

	if key == (WebKey{}) {
		return nil, fmt.Errorf("no matching entry for kid %s found", kid)
	}
	if key.Alg != alg {
		return nil, fmt.Errorf(
			"algorithm expected by JWKS (%s) does not match JWT (%s)",
			key.Alg,
			alg,
		)
	}

	return key.establishRSA()
}

func (j job) parseClaims(key interface{}) (jc Claims, err error) {
	_, err = jwt.ParseWithClaims(j.encoded, &jc, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			// We should already catch this case when fetching the keys from JWKS.
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return key, nil
	})
	if err != nil {
		return Claims{}, err
	} else if jc == (Claims{}) {
		return Claims{}, errors.New("failed to establish GitLabClaims from JWT")
	}
	if j.checkID && jc.JobID != j.jobID {
		return Claims{}, errors.New("stolen JWT detected")
	}
	err = v.Struct(jc)

	return
}

func checkHeader(encoded string) (Header, error) {
	h := Header{}
	seg, err := jwt.DecodeSegment(strings.Split(encoded, ".")[0])
	if err != nil {
		return h, err
	}

	if err = json.Unmarshal(seg, &h); err != nil {
		return h, err
	}
	err = v.Struct(h)

	return h, err
}

func (wks WebKeySet) findFirstKey(kid string) WebKey {
	for _, k := range wks.Keys {
		if k.Kid == kid {
			return k
		}
	}
	return WebKey{}
}

func decodeBase64(encoded string) ([]byte, error) {
	return base64.RawURLEncoding.DecodeString(encoded)
}

type modulus struct {
	data *big.Int
}

func (m *modulus) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}

	buf, err := decodeBase64(s)
	if err != nil {
		return err
	} else if buf == nil {
		return nil
	}
	m.data = new(big.Int).SetBytes(buf)

	return nil
}

type exponent struct {
	data int
}

func (e *exponent) UnmarshalJSON(data []byte) error {
	var v interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}

	s, err := convertStr(v)
	if err != nil {
		return errors.New("failed to convert JWKS provided e (exponent)")
	}

	buf, err := decodeBase64(s)
	if err != nil {
		return err
	}
	e.data = int(new(big.Int).SetBytes(buf).Int64())

	return nil
}

func convertStr(v interface{}) (string, error) {
	switch v := v.(type) {
	case int:
		return strconv.Itoa(v), nil
	case string:
		return v, nil
	default:
		return "", fmt.Errorf("conversion to string from %T not supported", v)
	}
}

func (wk WebKey) establishRSA() (*rsa.PublicKey, error) {
	if wk.N == nil || wk.E == nil {
		return nil, errors.New("failed to identify public key, verify JWKS algorithm type")
	}

	return &rsa.PublicKey{
		N: wk.N.data,
		E: wk.E.data,
	}, nil
}

func init() {
	client = &http.Client{Timeout: time.Second * 10}

	v = validator.New()
	_ = v.RegisterValidation("kid", rules.CheckKID)
	_ = v.RegisterValidation("username", rules.CheckUsername)
	_ = v.RegisterValidation("projectPath", rules.CheckProjectPath)
	_ = v.RegisterValidation("sub", rules.CheckSubject)
	_ = v.RegisterValidation("authToken", rules.CheckAuthToken)
}
