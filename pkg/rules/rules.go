// Package rules maintains established validation for shared aspects relating
// to either Jacamar specifically or the ECP CI effort as a whole. All checks
// that have been defined realize the validator.Func interface for the
// github.com/go-playground/validator/v10 and are meant to be a component in
// any validation strategy.
//
// Example code:
//
//      type Header struct {
//      	Alg string `json:"alg" validate:"len=5,alphanum"`
//	        Kid string `json:"kid" validate:"kid"`
//	        Typ string `json:"typ" validate:"eq=JWT"`
//      }
//
//      func Assert(h Header) error {
//          v := validator.New()
//	        _ = v.RegisterValidation("kid", rules.CheckKID)
//          return v.Struct(h)
//      }
//
package rules

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
)

// CheckUsername examines the provided username for validity based upon adduser(8).
// A username is treated as optional. Implements validator.Func.
func CheckUsername(v validator.FieldLevel) bool {
	matched, _ := regexp.MatchString(`^([a-zA-Z0-9_-]{0,31})$`, v.Field().String())
	return matched
}

// CheckSHA256 ensures the string matches the structure of a SHA256 checksum,
// implements validator.Func.
func CheckSHA256(v validator.FieldLevel) bool {
	matched, _ := regexp.MatchString(`^[A-Fa-f0-9]{64}$`, v.Field().String())
	return matched
}

// CheckProjectPath ensures the values adheres to expectations of a GitLab group/project.,
// implements validator.Func. Path can contain only letters, digits, '_', '-' and '.'.
// Cannot start with '-', end in '.git' or end in '.atom'.
func CheckProjectPath(v validator.FieldLevel) bool {
	s := v.Field().String()

	if strings.HasPrefix(s, "-") ||
		strings.HasSuffix(s, ".git") ||
		strings.HasSuffix(s, ".atom") {
		return false
	}

	matched, _ := regexp.MatchString(`^[a-zA-Z0-9_.\-/]{1,300}$`, s)
	return matched
}

// CheckKID ensures a valid Key ID in a JWT header, implement validator.Func.
func CheckKID(v validator.FieldLevel) bool {
	// 200 characters is arbitrary, I just wanted to put a cap of some sort.
	matched, _ := regexp.MatchString(`^[a-zA-Z0-9_\-]{1,200}$`, v.Field().String())
	return matched
}

// CheckSubject ensures the subject (sub) of a GitLab job jwt is valid,
// implements validator.Func.
func CheckSubject(v validator.FieldLevel) bool {
	if v.Parent().Kind() != reflect.Struct {
		return false
	}

	id := v.Parent().FieldByName("JobID").String()
	if id == "" {
		return false
	}
	return v.Field().String() == fmt.Sprintf("job_%s", id)
}

// CheckGitLabToken ensures a valid GitLab job/personal/deploy token, implements
// validator.Func.
func CheckGitLabToken(v validator.FieldLevel) bool {
	matched, _ := regexp.MatchString(`^[a-zA-Z0-9_\-]{20}$`, v.Field().String())
	return matched
}

// CheckAuthToken ensures a valid authorization token for federation supplied,
// an authorization is not required. Implements validator.Func.
func CheckAuthToken(v validator.FieldLevel) bool {
	// Currently no known maximum size.
	matched, _ := regexp.MatchString(`^[a-zA-Z0-9_\-]*$`, v.Field().String())
	return matched
}

// CheckJWT provide basic sanity check of characters found in a valid JSON Web Token,
// implements validator.Func.
func CheckJWT(v validator.FieldLevel) bool {
	// Currently no known maximum size.
	matched, _ := regexp.MatchString(
		`^[A-Za-z0-9-_]*\.[A-Za-z0-9-_]*\.[A-Za-z0-9-_]*$`,
		v.Field().String(),
	)
	return matched
}

// CheckDirectory verifies the validity of an expected Unix path without checking for
// it's existence on the system.
func CheckDirectory(v validator.FieldLevel) bool {
	str := v.Field().String()
	if len(str) == 0 || len([]byte(str)) > 4096 {
		return false
	}
	matched, _ := regexp.MatchString(`^(/[\w\-.^ ]+)+/?$`, str)
	return matched
}
