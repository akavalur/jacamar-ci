package rules_test

import (
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/rules"
)

func TestCheckUserName(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("username", rules.CheckUsername)

	type myTest struct {
		Name string `validate:"username"`
	}

	tests := map[string]struct {
		username string
		wantErr  bool
	}{
		"empty": {
			username: "",
			wantErr:  false,
		},
		"greater than 32 character": {
			username: "abcdefghijklmnopqrstuvwxyz7890123",
			wantErr:  true,
		},
		"hyphen in middle": {
			username: "user-name01",
			wantErr:  false,
		},
		"alphanumeric": {
			username: "user123",
			wantErr:  false,
		},
		"underscore in middle": {
			username: "user_name01",
			wantErr:  false,
		},
		"space in middle": {
			username: "user name01",
			wantErr:  true,
		},
		"bash command": {
			username: "$(env)",
			wantErr:  true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Name: tt.username,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckUsername() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestCheckSHA256(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("sha256", rules.CheckSHA256)

	type myTest struct {
		Sha256 string `validate:"sha256"`
	}

	tests := map[string]struct {
		checksum string
		wantErr  bool
	}{
		"empty": {
			checksum: "",
			wantErr:  true,
		},
		"valid sha256": {
			checksum: "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317",
			wantErr:  false,
		},
		"invalid characters": {
			checksum: "$(hello)",
			wantErr:  true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Sha256: tt.checksum,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckSHA256() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestProjectPath(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("projectPath", rules.CheckProjectPath)

	type myTest struct {
		Path string `validate:"projectPath"`
	}

	tests := map[string]struct {
		path    string
		wantErr bool
	}{
		"empty": {
			path:    "",
			wantErr: true,
		},
		"end with .git": {
			path:    "group/project.git",
			wantErr: true,
		},
		"end with .atom": {
			path:    "group/project.atom",
			wantErr: true,
		},
		"begin with -": {
			path:    "-group/project",
			wantErr: true,
		},
		"invalid characters": {
			path:    "group/$(project)/1",
			wantErr: true,
		},
		"valid project path": {
			path:    "group/sub-group.1/my_project",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Path: tt.path,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckProjectPath() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestKID(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("kid", rules.CheckKID)

	type myTest struct {
		ID string `validate:"kid"`
	}

	tests := map[string]struct {
		id      string
		wantErr bool
	}{
		"empty": {
			id:      "",
			wantErr: true,
		},
		"too long (201)": {
			id:      "zvPaE5BJDxWqMqGeeuQxar4m6OmG0ebsI4pKkbZc6NQOXl7LP6hgPxlyktgn7Lxe7J95GxL1iUKlBEhXdsEsppt79qGiX6TVJQOaRVxNJH9NuZNS33CoiH6ZBRtb8Uu1VITGo5qyOmD0aA3BT755pf4KugYH64b7c99OtVknfm2f46PmPBk3cFOTQkWqQvRw2ErqbXAhF",
			wantErr: true,
		},
		"invalid characters": {
			id:      "group/$(project)/1",
			wantErr: true,
		},
		"valid kid (gitlab.com/-/jwks - 0)": {
			id:      "kewiQq9jiC84CvSsJYOB-N6A8WFLSV20Mb-y7IlWDSQ",
			wantErr: false,
		},
		"valid kid (gitlab.com/-/jwks - 1)": {
			id:      "4i3sFE7sxqNPOT7FdvcGA1ZVGGI_r-tsDXnEuYT4ZqE",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				ID: tt.id,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckKID() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestSubject(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("sub", rules.CheckSubject)

	type myTest struct {
		JobID string
		Sub   string `validate:"sub"`
	}

	tests := map[string]struct {
		sub     string
		id      string
		wantErr bool
	}{
		"empty sub": {
			sub:     "",
			id:      "123",
			wantErr: true,
		},
		"empty id": {
			sub:     "job_123",
			id:      "",
			wantErr: true,
		},
		"matching": {
			sub:     "job_123",
			id:      "123",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Sub:   tt.sub,
				JobID: tt.id,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckSubject() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("no JobID field in struct", func(t *testing.T) {
		type secTest struct {
			Sub string `validate:"sub"`
		}

		v := secTest{
			Sub: "test",
		}

		err := validate.Struct(v)
		assert.Error(t, err)
	})
}

func TestGitLabToken(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("gitlabToken", rules.CheckGitLabToken)

	type myTest struct {
		Token string `validate:"gitlabToken"`
	}

	tests := map[string]struct {
		token   string
		wantErr bool
	}{
		"empty": {
			token:   "",
			wantErr: true,
		},
		"valid, alphanumeric": {
			token:   "EArXoCj8gmiAa8eZzPiR",
			wantErr: false,
		},
		"valid, underscore encountered": {
			token:   "PG_buQjszuoCoxxayMZG",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Token: tt.token,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckGitLabToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestAuthToken(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("authToken", rules.CheckAuthToken)

	type myTest struct {
		Token string `validate:"authToken"`
	}

	tests := map[string]struct {
		token   string
		wantErr bool
	}{
		"empty": {
			token:   "",
			wantErr: false,
		},
		"valid, alphanumeric + underscore + hyphen": {
			token:   "EArXoCj8g_miAa8e-ZzP_iR",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Token: tt.token,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckAuthToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestJWT(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("jwt", rules.CheckJWT)

	type myTest struct {
		JWT string `validate:"jwt"`
	}

	tests := map[string]struct {
		jwt     string
		wantErr bool
	}{
		"empty": {
			jwt:     "",
			wantErr: true,
		},
		"valid random jwt": {
			jwt:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			wantErr: false,
		},
		"missing segment": {
			jwt:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ",
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				JWT: tt.jwt,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckAuthToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestDirectory(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("directory", rules.CheckDirectory)

	type myTest struct {
		File string `validate:"directory"`
	}

	tests := map[string]struct {
		file    string
		wantErr bool
	}{
		"empty": {
			file:    "",
			wantErr: true,
		},
		"simple filepath + script": {
			file:    "/dir/test/file.bash",
			wantErr: false,
		},
		"filepath ony": {
			file:    "/dir/test/some.dir/test_123/new-example/",
			wantErr: false,
		},
		"unexpanded variable": {
			file:    "/dir/$TEST/",
			wantErr: true,
		},
		"bash command": {
			file:    "$(env)",
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				File: tt.file,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckDirectory() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}
