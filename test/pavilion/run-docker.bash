#!/usr/bin/env bash

# Execute Pavilion tests within Docker containers.

set -eo pipefail
set +o noclobber

[ -z "${ROOT_DIR}" ] && ROOT_DIR=$(pwd)
export PAV_CONFIG_DIR="${ROOT_DIR}/test/pavilion/docker"

ci_dir="/builds/ecp-ci/jacamar-ci"

#############################
# General testing functions #
#############################

permissions() {
  # Ensure downscope user has access to view/create test directories.
  chmod -R 755 "${ROOT_DIR}/test/scripts"
  chmod 777 "${ROOT_DIR}/test/pavilion/docker/working_dir"
}

build() {
  echo "Building Jacamar Binaries..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -w ${ci_dir} \
    -t ${BUILD_IMG} \
    bash -c "make build"
}

clean() {
  pushd "${PAV_CONFIG_DIR}/working_dir"
    ls | grep -v .gitignore | xargs rm -rf
  popd
}

###########################
# Shell testing functions #
###########################

auth() {
  permissions

  echo "Testing Jacamar Authorization..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t  ${PAVILION_IMG} \
    bash -c "nohup python -u ${ci_dir}/tools/mock-gl-api/gl-api.py \
              > ${ci_dir}/test/pavilion/docker/working_dir/mock_api & \
            cd ${ci_dir}/test/pavilion/docker \
            && pav series test-auth \
            && sleep 5 \
            && pav wait \
            && pav status --limit 1000 | if grep -q FAIL; then exit 1; fi"
}

jacamar() {
  permissions

  echo "Testing Jacamar + Shell Executor..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -u "user" \
    -t ${PAVILION_IMG} \
    bash -c "nohup python -u ${ci_dir}/tools/mock-gl-api/gl-api.py \
              > ${ci_dir}/test/pavilion/docker/working_dir/mock_api & \
            cd ${ci_dir}/test/pavilion/docker \
            && pav series test-jacamar \
            && sleep 5 \
            && pav wait \
            && pav status --limit 1000 | if grep -q FAIL; then exit 1; fi"
}

su_test() {
  permissions

  echo "Testing Jacamar Authorization + Su Downscoping..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t ${PAVILION_IMG} \
    bash -c "nohup python -u ${ci_dir}/tools/mock-gl-api/gl-api.py \
              > ${ci_dir}/test/pavilion/docker/working_dir/mock_api & \
            cd ${ci_dir}/test/pavilion/docker \
            && pav series test-su \
            && sleep 5 \
            && pav wait \
            && pav status --limit 1000 | if grep -q FAIL; then exit 1; fi"
}

seccomp() {
  permissions

  echo "Testing Jacamar Seccomp..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t  ${PAVILION_IMG} \
    bash -c "nohup python -u ${ci_dir}/tools/mock-gl-api/gl-api.py \
              > ${ci_dir}/test/pavilion/docker/working_dir/mock_api & \
            cd ${ci_dir}/test/pavilion/docker \
            && pav series test-seccomp \
            && sleep 5 \
            && pav wait \
            && pav status --limit 1000 | if grep -q FAIL; then exit 1; fi"
}

capabilities() {
  permissions

  echo "Testing Capabilities + Setuid Downscoping..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t ${PAVILION_IMG} \
    bash -c "bash ${ci_dir}/test/pavilion/shared/cap-prep.bash \
            && su - jacamar -c 'ROOT_DIR=${ci_dir} ${ci_dir}/test/pavilion/run-docker.bash capabilities_user'"
}

capabilities_user() {
  nohup python -u ${ci_dir}/tools/mock-gl-api/gl-api.py \
    > ${ci_dir}/test/pavilion/docker/working_dir/mock_api &
  export CI_PROJECT_DIR=${ci_dir}
  cd ${ci_dir}/test/pavilion/docker \
    && pav series test-capabilities \
    && sleep 5 \
    && pav wait \
    && pav status --limit 1000 | if grep -q FAIL; then exit 1; fi
}

###########################
# Slurm testing functions #
###########################

sacctmgr_update() {
  sacctmgr -i create user user name=user || echo "Added user"
  supervisorctl restart slurmctld > /dev/null
}

slurm() {
  permissions

  echo "Testing Jacamar + Slurm (CLI) Executor..."
  docker run \
    -h ernie \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t "${SLURM_IMG}" \
    bash -c "${ci_dir}/test/pavilion/run-docker.bash sacctmgr \
            && su - user -c 'ROOT_DIR=${ci_dir} ${ci_dir}/test/pavilion/run-docker.bash slurm_user'"
}

slurm_user() {
  export CI_PROJECT_DIR=${ci_dir}
  cd ${ci_dir}/test/pavilion/docker \
    && pav series test-slurm \
    && sleep 5 \
    && pav wait \
    && pav status --limit 1000 | if grep -q FAIL; then exit 1; fi
}

case "${1}" in
  auth)
    auth
    ;;
  build)
    build
    ;;
  capabilities)
    capabilities
    ;;
  capabilities_user)
    capabilities_user
    ;;
  clean)
    clean
    ;;
  jacamar)
    jacamar
    ;;
  sacctmgr)
    sacctmgr_update
    ;;
  slurm)
    slurm
    ;;
  slurm_user)
    slurm_user
    ;;
  su)
    su_test
    ;;
  seccomp)
    seccomp
    ;;
  *)
    echo "Invalid argument: ${0}"
    exit 1
    ;;
esac
