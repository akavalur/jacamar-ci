#!/usr/bin/env bash

# Execute Pavilion tests on Ascent.

set -eo pipefail
set +o noclobber

[ -z "${ROOT_DIR}" ] && ROOT_DIR=$(pwd)
export PAV_CONFIG_DIR="${ROOT_DIR}/test/pavilion/olcf"

[ -z "${CI_PROJECT_DIR}" ] && export CI_PROJECT_DIR=${ROOT_DIR}

clean() {
  pushd "${PAV_CONFIG_DIR}/working_dir"
    ls | grep -v .gitignore | xargs rm -rf
  popd
}

ascent() {
  chmod -R 700 "${PAV_CONFIG_DIR}/../shared/scripts"

  python test/pavilion/shared/sequence.py \
    "${PAV_CONFIG_DIR}/tests/ascent.yaml" \
    "${PAV_CONFIG_DIR}"

  pav status --all --limit 1000
  pav status -a -l 1000 | if grep -q FAIL; then exit 1; fi
}

case "${1}" in
  clean)
    clean
    ;;
  ascent)
    ascent
    ;;
  *)
    echo "Invalid argument: ${0} (clean|test)"
    exit 1
    ;;
esac
