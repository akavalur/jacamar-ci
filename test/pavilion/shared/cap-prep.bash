#!/usr/bin/env bash

set -eo pipefail
set +o noclobber

# *IMPORTANT* this script is designed to prepare capabilities
# focused testing for pav-docker-capabilities. Do no attempt
# to run outside the associated container environment.

mkdir -p /opt/jacamar/bin
chmod 700 -R /opt
chown jacamar:jacamar -R /opt

# cap_setuid+setgid
cp /usr/local/sbin/jacamar-auth /opt/jacamar/bin/jacamar-auth
setcap cap_setuid,cap_setgid+ep /opt/jacamar/bin/jacamar-auth

mkdir /ecp
chmod 701 /ecp
mkdir /ecp/user
chown user:user /ecp/user
chmod 700 /ecp/user
chown jacamar:jacamar /ecp

# cap_setuid+setgid+chown
cp /usr/local/sbin/jacamar-auth /opt/jacamar/bin/jacamar-chown
setcap cap_chown,cap_setuid,cap_setgid+ep /opt/jacamar/bin/jacamar-chown

mkdir /ecp-chown
chmod 701 /ecp-chown
chown jacamar:jacamar /ecp-chown
