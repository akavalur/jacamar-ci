from spack import *


class Go(Package):
    """The golang compiler and build environment (binary deployment for ppc64le)"""
    homepage = "https://golang.org"
    url = 'https://dl.google.com/go/go1.14.4.linux-ppc64le.tar.gz'

    extendable = True

    version('1.16.5', sha256='b12c23023b68de22f74c0524f10b753e7b08b1504cb7e417eccebdd3fae49061')
    version('1.16', sha256='27a1aaa988e930b7932ce459c8a63ad5b3333b3a06b016d87ff289f2a11aacd6')
    version('1.15.6', sha256='d4174fc217e749ac049eacc8827df879689f2246ac230d04991ae7df336f7cb2')
    version('1.15.1', sha256='50bdca87edc30bdc12956dab765c1b85249373a156a7a00eb998cdc70790fb94')
    version('1.14.6', sha256='8eb4c84e7b6aa9edb966c467dd6764d131a57d27afbd87cc8f6d10535df9e898')
    version('1.14.4', sha256='b335f85bc935ca3f553ad1bac37da311aaec887ffd8a48cb58a0abb0d8adf324')

    provides('golang')

    depends_on('git', type=('build', 'link', 'run'))

    def install(self, spec, prefix):
        tar = which("tar")
        tar('-C', prefix, '-xzf', self.stage.archive_file, '--strip', '1')
