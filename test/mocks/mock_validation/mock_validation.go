// Code generated by MockGen. DO NOT EDIT.
// Source: internal/authuser/validation/validation.go

// Package mock_validation is a generated GoMock package.
package mock_validation

import (
	gomock "github.com/golang/mock/gomock"
	logrus "github.com/sirupsen/logrus"
	validation "gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	gitlabjwt "gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
	reflect "reflect"
)

// MockRunAsValidator is a mock of RunAsValidator interface
type MockRunAsValidator struct {
	ctrl     *gomock.Controller
	recorder *MockRunAsValidatorMockRecorder
}

// MockRunAsValidatorMockRecorder is the mock recorder for MockRunAsValidator
type MockRunAsValidatorMockRecorder struct {
	mock *MockRunAsValidator
}

// NewMockRunAsValidator creates a new mock instance
func NewMockRunAsValidator(ctrl *gomock.Controller) *MockRunAsValidator {
	mock := &MockRunAsValidator{ctrl: ctrl}
	mock.recorder = &MockRunAsValidatorMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockRunAsValidator) EXPECT() *MockRunAsValidatorMockRecorder {
	return m.recorder
}

// Execute mocks base method
func (m *MockRunAsValidator) Execute(pay gitlabjwt.Claims, username string, sysLog *logrus.Entry) (validation.RunAsOverride, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Execute", pay, username, sysLog)
	ret0, _ := ret[0].(validation.RunAsOverride)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Execute indicates an expected call of Execute
func (mr *MockRunAsValidatorMockRecorder) Execute(pay, username, sysLog interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Execute", reflect.TypeOf((*MockRunAsValidator)(nil).Execute), pay, username, sysLog)
}
