#!/usr/bin/env bash

# Test locally maintained Spack packages as part of a CI job.

set -eo pipefail

# Libseccomp installed through apt. Spack package gitlab-runner finds it through this env var
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/lib/x86_64-linux-gnu/pkgconfig/

git clone https://github.com/spack/spack --branch develop --single-branch spack

pushd spack
  git checkout ${SPACK_COMMIT}
popd

source spack/share/spack/setup-env.sh

pushd ${CI_PROJECT_DIR}/test/spack/gitlab
    spack env activate .
    spack repo add ${CI_PROJECT_DIR}/build/package/spack/jacamar-repo
    spack external find git go tar
    spack install --fail-fast jacamar-ci ^gitlab-runner+jacamar
    spack test run
    spack test results -l
popd
