#!/usr/bin/env bash

# Test and build RPM file from CI jobs.

set -eo pipefail

[ -z "$CI_PROJECT_DIR" ] && export CI_PROJECT_DIR=$(pwd)

RUNNER_VERSION=13.12.0
RUNNER_REF=7a6612da06043f908b740629bbe3f0d9c59a5dad

lint() {
  rpmlint ${CI_PROJECT_DIR}/build/package/rpm/gitlab-runner-ecp.spec
}

build() {
  # rpmdev-setuptree
  mkdir -p ${HOME}/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}

  cp \
    ${CI_PROJECT_DIR}/build/package/rpm/gitlab-runner-ecp.spec \
    ${HOME}/rpmbuild/SPECS/gitlab-runner-ecp.spec

  cp \
    ${CI_PROJECT_DIR}/build/package/rpm/patches/runner_trusted.patch \
    ${HOME}/rpmbuild/SOURCES/runner_trusted.patch

  pushd ${HOME}/rpmbuild
    rpmbuild -ba SPECS/gitlab-runner-ecp.spec \
      --define "_topdir `pwd`" \
      --define "_version ${RUNNER_VERSION}" \
      --define "_ref ${RUNNER_REF}"
  popd

  # relocate RPMs
  mkdir -p rpmbuild/
  cp -R ${HOME}/rpmbuild/{SPECS,RPMS,SRPMS} rpmbuild/
}

docker-run() {
  ci_dir="/builds/ecp-ci/jacamar-ci"

  echo "Running Runner-RPM Build in Docker..."
  docker run \
    -v ${CI_PROJECT_DIR}:${ci_dir} \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -w ${ci_dir} \
    -t ${BUILD_IMG} \
    bash -c "make runner-release"

  rm -rf rpmbuild/
}

release() {
  mkdir -p ${CI_PROJECT_DIR}/rpms

  cp \
    ${HOME}/rpmbuild/{RPMS/$(uname -m),SRPMS}/* \
    ${CI_PROJECT_DIR}/rpms

  GOARCH=$(go env GOARCH)

  tar \
    -cJf \
    gitlab-runner-ecp-v${RUNNER_VERSION}-${GOARCH}.tar.xz \
    rpms/

  sha256sum gitlab-runner-ecp-v${RUNNER_VERSION}-${GOARCH}.tar.xz
}

case "${1}" in
  ci)
    lint
    build
    ;;
  docker)
    docker-run
    ;;
  release)
    build
    release
    ;;
  *)
    echo "Invalid argument: ${0} (ci|docker|release)"
    exit 1
    ;;
esac
