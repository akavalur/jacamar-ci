#!/usr/bin/env bash

# Test and build RPM file from CI jobs.

set -eo pipefail

[ -z "$CI_PROJECT_NAME" ] && export CI_PROJECT_NAME="jacamar-ci"
[ -z "$CI_PROJECT_DIR" ] && export CI_PROJECT_DIR=$(pwd)

lint() {
  rpmlint ${CI_PROJECT_DIR}/build/package/rpm/jacamar-ci.spec
}

build() {
  # rpmdev-setuptree
  mkdir -p ${CI_PROJECT_DIR}/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}

  tmpdir=$(dirname $(mktemp -u))

  tar \
    --transform="s,${CI_PROJECT_NAME}/,jacamar-ci-v${VERSION}/," \
    -C "${CI_PROJECT_DIR}/.." \
    -czf \
    ${tmpdir}/jacamar-ci-v${VERSION}.tar.gz \
    ${CI_PROJECT_NAME}

  mv \
    ${tmpdir}/jacamar-ci-v${VERSION}.tar.gz \
    ${CI_PROJECT_DIR}/rpmbuild/SOURCES/jacamar-ci-v${VERSION}.tar.gz

  cp \
    ${CI_PROJECT_DIR}/build/package/rpm/jacamar-ci.spec \
    ${CI_PROJECT_DIR}/rpmbuild/SPECS/jacamar-ci.spec

  pushd ${CI_PROJECT_DIR}/rpmbuild
    rpmbuild -ba SPECS/jacamar-ci.spec \
      --define "_topdir `pwd`" \
      --define "_version ${VERSION}" \
      --define "debug_package %{nil}"
  popd
}

docker-run() {
  ci_dir="/builds/ecp-ci/jacamar-ci"

  echo "Running RPM Build in Docker..."
  docker run \
    -v ${CI_PROJECT_DIR}:${ci_dir} \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -w ${ci_dir} \
    -t ${BUILD_IMG} \
    bash -c "make release"

  rm -rf rpmbuild/
}

release() {
  mkdir -p ${CI_PROJECT_DIR}/rpms

  cp \
      ${CI_PROJECT_DIR}/rpmbuild/{RPMS/$(uname -m),SRPMS}/* \
      ${CI_PROJECT_DIR}/rpms

  GOARCH=$(go env GOARCH)

  tar \
    -cJf \
    jacamar-ci-v${VERSION}-${GOARCH}.tar.xz \
    rpms/

  sha256sum jacamar-ci-v${VERSION}-${GOARCH}.tar.xz
}

case "${1}" in
  ci)
    lint
    build
    ;;
  docker)
    docker-run
    ;;
  release)
    build
    release
    ;;
  *)
    echo "Invalid argument: ${0} (ci|docker|release)"
    exit 1
    ;;
esac
