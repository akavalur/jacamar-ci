#!/usr/bin/env bash

# Re-build all mock files (https://github.com/golang/mock)

set -eo pipefail
set -o xtrace

# Internal Packages
mockgen -source=internal/runmechanisms/runmechanisms.go \
    -destination=test/mocks/mock_runmechanisms/mock_run.go \
    -package=mock_runmechanisms
mockgen -source=internal/authuser/authuser.go \
    -destination=test/mocks/mock_authuser/mock_authuser.go \
    -package=mock_authuser
mockgen -source=internal/executors/executors.go \
    -destination=test/mocks/mock_executors/mock_executors.go \
    -package=mock_executors
mockgen -source=internal/configure/configure.go \
    -destination=test/mocks/mock_configure/mock_configure.go \
    -package=mock_configure
mockgen -source=internal/batch/batch.go \
    -destination=test/mocks/mock_batch/mock_batch.go \
    -package=mock_batch
mockgen -source=internal/logging/logging.go \
    -destination=test/mocks/mock_logging/mock_logging.go \
    -package=mock_logging
mockgen -source=internal/command/command.go \
    -destination=test/mocks/mock_command/mock_command.go \
    -package=mock_command
mockgen -source=internal/brokercomms/broker.go \
    -destination=test/mocks/mock_brokercomms/mock_boker.go \
    -package=mock_brokercomms
mockgen -source=internal/authuser/validation/validation.go \
    -destination=test/mocks/mock_validation/mock_validation.go \
    -package=mock_validation

# Public Packages
mockgen -source=pkg/gitlabjwt/gitlabjwt.go \
    -destination=test/mocks/mock_gitlabjwt/gitlabjwt_mock.go \
    -package=mock_gitlabjwt
