package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		log.Fatal("missing script as argument")
	}

	b, err := ioutil.ReadFile(flag.Arg(0))
	if err != nil {
		log.Fatal(err)
	}

	for _, line := range strings.Split(string(b), "\n") {
		if strings.HasPrefix(line, ": | eval") {
			for _, v := range strings.Split(line, "\\n") {
				fmt.Println(v)
			}
		}
	}
}
