SHELL := bash
.ONESHELL:
.DELETE_ON_ERROR:
.DEFAULT_GOAL := build
MAKEFLAGS += --no-builtin-rules

# Project & structure variables.
PKG = gitlab.com/ecp-ci/jacamar-ci
INTERNAL_PKG = ${PKG}/internal
COVER_REPORT := cover.out
BUILDDIR ?= binaries/
ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif
INSTALL_PREFIX := $(if $(DESTDIR),$(DESTDIR)/$(PREFIX),$(PREFIX))

# Container images
export BUILD_IMG=registry.gitlab.com/ecp-ci/jacamar-ci/centos7-builder:1.16.5
export PAVILION_IMG=registry.gitlab.com/ecp-ci/jacamar-ci/pav-tester:1.16.5
export SLURM_IMG=registry.gitlab.com/ecp-ci/jacamar-ci/slurm-tester:20.11.3
export GO_IMG=registry.gitlab.com/ecp-ci/jacamar-ci/go-tester:1.16.5

# Version variables.
GITCOMMIT := $(shell ./test/scripts/gitcommit.bash)
GITBRANCH := $(shell git rev-parse --abbrev-ref HEAD 2>/dev/null)
GOVERSION := $(shell go version | cut -c12- | awk '{ gsub (" ", "-", $$0); print}')
BUILDDATE := $(shell date -u +"%Y-%m-%dT%T%z")
ifndef GITCOMMIT
VERSION ?= $(shell cat VERSION)
else
VERSION ?= $(shell cat VERSION).pre.${GITCOMMIT}
endif

# Go variables.
GOCMD := go
CGO_ENABLED = 1
LDFLAGS = -X ${INTERNAL_PKG}/version.version=${VERSION} \
		  -X ${INTERNAL_PKG}/version.gitCommit=${GITCOMMIT} \
		  -X ${INTERNAL_PKG}/version.gitBranch=${GITBRANCH} \
		  -X ${INTERNAL_PKG}/version.goVersion=${GOVERSION} \
		  -X ${INTERNAL_PKG}/version.buildDate=${BUILDDATE} \
		  -s \
		  -w

.PHONY: all
all:
	$(MAKE) build
	$(MAKE) install

###################
# Build & Install #
###################

.PHONY: build
build:
	mkdir -p ${BUILDDIR}
	${GOCMD} build -trimpath -ldflags "${LDFLAGS}" -tags netgo -o ${BUILDDIR}jacamar-auth cmd/jacamar-auth/main.go
	CGO_ENABLED=0 ${GOCMD} build -trimpath -ldflags "${LDFLAGS}" -tags netgo,osusergo,getent -o ${BUILDDIR}jacamar cmd/jacamar/main.go

# build-static should only be used to testing purposes
.PHONY: build-static
build-static:
	mkdir -p ${BUILDDIR}
	${GOCMD} build -trimpath -ldflags "${LDFLAGS} -linkmode external -extldflags -static" -tags netgo,getent -o ${BUILDDIR}jacamar-auth cmd/jacamar-auth/main.go
	CGO_ENABLED=0 ${GOCMD} build -trimpath -ldflags "${LDFLAGS}" -tags netgo,osusergo,getent -o ${BUILDDIR}jacamar cmd/jacamar/main.go

.PHONY: install
install:
	install -d $(INSTALL_PREFIX)/bin
	install -m 755 ${BUILDDIR}jacamar $(INSTALL_PREFIX)/bin
	install -m 755 ${BUILDDIR}jacamar-auth $(INSTALL_PREFIX)/bin

#############
# Packaging #
#############

.PHONY: rpm-ci
rpm-ci:
	VERSION=${VERSION} bash ./test/scripts/package/rpm.bash ci

.PHONY: rpm-docker
rpm-docker:
	VERSION=${VERSION} bash ./test/scripts/package/rpm.bash docker

.PHONY: release
release:
	$(MAKE) clean
	VERSION=${VERSION} bash ./test/scripts/package/rpm.bash release

.PHONY: runner-rpm-ci
runner-rpm-ci:
	bash ./test/scripts/package/runner-rpm.bash ci

.PHONY: runner-docker
runner-docker:
	bash ./test/scripts/package/runner-rpm.bash docker

.PHONY: runner-release
runner-release:
	$(MAKE) clean
	bash ./test/scripts/package/runner-rpm.bash release

###########
# Testing #
###########

.PHONY: test 
test:
	${GOCMD} test -p 1 -coverprofile ${COVER_REPORT} -cover -tags netgo -timeout 2m -v ./... \
		&& ${GOCMD} tool cover -func=${COVER_REPORT}

.PHONY: test-package
test-package:
	${GOCMD} test -p 1 -tags netgo -timeout 2m -v ./${PACKAGE}

.PHONY: test-docker
test-docker:
	@bash ./test/scripts/test_docker.bash test ${PACKAGE}

.PHONY: test-quality
test-quality: 
	# https://github.com/golangci/golangci-lint
	golangci-lint --version
	golangci-lint run

.PHONY: test-security
test-security:
	# https://github.com/securego/gosec
	gosec --version
	gosec -exclude-dir=tools -exclude-dir=test  -exclude-dir=build ./...

.PHONY: coverage
coverage:
	${GOCMD} tool cover -func=${COVER_REPORT}

####################
# Pavilion Testing #
####################

.PHONY: pav-docker-auth
pav-docker-auth:
	@bash ./test/pavilion/run-docker.bash auth

.PHONY: pav-docker-build
pav-docker-build:
	@bash ./test/pavilion/run-docker.bash build

.PHONY: pav-docker-capabilities
pav-docker-capabilities:
	@bash ./test/pavilion/run-docker.bash capabilities

.PHONY: pav-docker-jacamar
pav-docker-jacamar:
	@bash ./test/pavilion/run-docker.bash jacamar

.PHONY: pav-docker-slurm
pav-docker-slurm:
	@bash ./test/pavilion/run-docker.bash slurm

.PHONY: pav-docker-su
pav-docker-su:
	@bash ./test/pavilion/run-docker.bash su

.PHONY: pav-docker-seccomp
pav-docker-seccomp:
	@bash ./test/pavilion/run-docker.bash seccomp

.PHONY: pav-docker-clean
pav-docker-clean:
	@bash ./test/pavilion/run-docker.bash clean

# OLCF Testing
.PHONY: pav-olcf-ascent
pav-olcf-ascent:
	@bash ./test/pavilion/run-olcf.bash ascent

.PHONY: pav-ascent-clean
pav-olcf-clean:
	@bash ./test/pavilion/run-olcf.bash clean

#########
# Misc. #
#########

.PHONY: clean
clean:
	${GOCMD} clean
	rm -rf $(BUILDDIR) rpmbuild/ rpms/
	rm -f ${COVER_REPORT}
	rm -rf jacamar-ci-v*.tar.xz gitlab-runner-ecp-v*.tar.xz

.PHONY: version
version:
	@echo Version: ${VERSION}
	@echo Git Commit: ${GITCOMMIT}
	@echo Git Branch: ${GITBRANCH}
	@echo Go Version: ${GOVERSION}
	@echo Built: ${BUILDDATE}

# Rebuild all mocks (https://github.com/golang/mock)
.PHONY: mocks
mocks:
	bash ./test/scripts/mocks.bash
