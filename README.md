# Jacamar CI

[![pipeline status](https://gitlab.com/ecp-ci/jacamar-ci/badges/develop/pipeline.svg)](https://gitlab.com/ecp-ci/jacamar-ci/-/commits/develop)
[![coverage report](https://gitlab.com/ecp-ci/jacamar-ci/badges/develop/coverage.svg)](https://gitlab.com/ecp-ci/jacamar-ci/-/commits/develop)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/ecp-ci/jacamar-ci)](https://goreportcard.com/report/gitlab.com/ecp-ci/jacamar-ci)

Jacamar CI is the HPC focused CI/CD driver for GitLab’s
[custom executor](https://docs.gitlab.com/runner/executors/custom.html).
The core goal of this project is to establish a maintainable, yet extensively configurable
tool that will allow for the use of [GitLab’s](https://gitlab.com) robust testing model
on unique HPC test resources. Allowing code teams to integrate potentially existing
pipelines on powerful scientific development environments.

![Jacamar CI Logo](build/images/jacamar_ci_logo_md.png)

## Documentation

For complete documentation regarding all aspects of deployment and administration of
Jacamar CI please see [ecp-ci.gitlab.io](https://ecp-ci.gitlab.io/docs/admin.html#jacamar-ci).

## Quick Start

If new to Jacamar CI we recommend viewing the
[administrator tutorial](https://ecp-ci.gitlab.io/docs/admin/jacamar/tutorial.html)
before continuing. This hands-on tutorial will only take ~15 minutes. It will
provide you with a basic introduction to deployment, configuration of the
custom executor, and provide an easy to manage process to testing the majority
of functionality safely with a container/virtual environment.

### Installation

See our [documentation](https://ecp-ci.gitlab.io/docs/admin/jacamar/deployment.html#packages)
on installing the release RPM. However, if you wish to build from source you will
require:

* Bash
* Git versions 2.9+
* Go versions 1.16+ (see [official instructions](https://golang.org/doc/install))
* libc
* [libseccomp](https://github.com/seccomp/libseccomp)
* Make

```Console
git clone https://gitlab.com/ecp-ci/jacamar-ci.git
cd jacamar-ci
make build
make install PREFIX=/usr/local
```

The official GitLab Runner supports the custom executor with their releases;
however, there is a [minor patch](https://gitlab.com/ecp-ci/jacamar-ci/-/blob/develop/build/package/rpm/patches/runner_trusted.patch)
required in order to establish a minimal set of trusted (cannot
be influenced by the user) job variables. As such you will also need to build
and install a patched version of the runner. To best support this temporary
process we provided a runner RPM and have also created a local
[Spack](https://spack.io/) repository:

```Console
spack repo add $(pwd)/build/package/Spack/jacamar-repo
spack install gitlab-runner+jacamar
```

Following the above commands will create a `gitlab-runner` package you can use
for testing. Remember in our case the version of the runner only matters
with regard to the `gitlab-runner run` command (though there will need to
be a runner available on the user's `PATH` for pipelines involving
artifacts/caching).

Please note that once this project is ready for production we aim to completely
remove  all modifications to the runner and simply conform to an upstream release.

### Configuration

If you've administered a GitLab Runner before, the
[configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)
will be familiar. With Jacamar CI, it is no different as it not only relies upon
specific management of the upstream runner but also its own configuration. Both
configurations leverage [TOML](https://github.com/toml-lang/toml).

#### GitLab Runner

When using the [custom executor](https://docs.gitlab.com/runner/executors/custom.html)
there are additional one time configuration requirements that must be accounted for.
This example targets the full path to our `jacamar-auth` application as we wish to
leverage the supported [authorization and downscoping mechanisms](https://ecp-ci.gitlab.io/docs/admin/jacamar/auth.html).
Please note that the `*_args` are required as shown.

```TOML
[[runners]]
  name = "Custom Executor Example"
  url = "https://gitlab.example.com/"
  token = "T0k3n"
  executor = "custom"
  [runners.custom]
    config_exec = "/opt/jacamar/bin/jacamar-auth"
    config_args = ["config", "--configuration", "/etc/gitlab-runner/custom-config.toml"]

    prepare_exec = "/opt/jacamar/bin/jacamar-auth"
    prepare_args = ["prepare"]

    run_exec = "/opt/jacamar/bin/jacamar-auth"
    run_args = ["run"]

    cleanup_exec = "/opt/jacamar/bin/jacamar-auth"
    cleanup_args = ["cleanup", "--configuration", "/etc/gitlab-runner/custom-config.toml"]
```

#### Jacamar

You will notice a Jacamar CI specific configuration is required
(`--configuration /etc/gitlab-runner/custom-config.toml`). This is to
support the plethora of
[configuration options](https://ecp-ci.gitlab.io/docs/admin/jacamar/configuration.html#jacamar-ci-config):

```TOML
[general]
executor = "slurm"
data_dir = "/ecp"

[auth]
downscope = "setuid"
```
This example is simplified, demonstrating on the minimally required
configuration options in order to run. We highly encourage you to explore
our documentation for a comprehensive look at options available and
recommended practices.

# Attributions

* The logo was created by @admgc and is a remix of the
  [Rufous-tailed jacamar](https://en.wikipedia.org/wiki/Jacamar#/media/File:Rufous-tailed_jacamar_(Galbula_ruficauda)_male_2.JPG)
  by [Charles J. Sharp](https://www.wikidata.org/wiki/Q54800218)
  from [Sharp Photography](http://www.sharpphotography.co.uk/),
  licensed under [CC BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0).
  