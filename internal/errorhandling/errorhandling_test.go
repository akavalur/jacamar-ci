package errorhandling

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestMessageErr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(gomock.Eq(stderrPrefix), gomock.Eq("prepare error")).Times(1)
	m.EXPECT().Stderr(gomock.Eq(stderrPrefix), gomock.Eq("cleanup error")).Times(1)
	m.EXPECT().Stderr(gomock.Eq(stderrPrefix), gomock.Eq(usrAuthorization+usrPostfix)).Times(1)
	m.EXPECT().Warn(gomock.Eq(stderrPrefix), gomock.Eq(usrRunMechanism+usrPostfix)).Times(1)
	m.EXPECT().Stderr(gomock.Eq(stderrPrefix), gomock.Eq("job context error")).Times(1)
	m.EXPECT().Stderr(gomock.Eq(stderrPrefix), gomock.Eq("auth error II")).Times(1)
	m.EXPECT().Warn(gomock.Eq(stderrPrefix), gomock.Eq(usrSeccomp+usrPostfix)).Times(1)
	m.EXPECT().Warn(gomock.Eq(stderrPrefix), gomock.Eq(usrPanic+usrPostfix)).Times(1)

	tests := map[string]struct {
		c    arguments.ConcreteArgs
		auth configure.Auth
		e    error
	}{
		"base error during prepare stage": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			e: errors.New("prepare error"),
		},
		"base error during cleanup stage": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			e: errors.New("cleanup error"),
		},
		"obfuscated auth error config stage": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			e: NewAuthError(errors.New("auth error")),
		},
		"obfuscated run mechanism error run stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			e: NewRunMechanismError(errors.New("run error")),
		},
		"un-obfuscated job context error cleanup stage": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			e: NewJobCtxError(errors.New("job context error")),
		},
		"UnobfuscatedError enabled": {
			c: arguments.ConcreteArgs{
				Config:            &arguments.ConfigCmd{},
				UnobfuscatedError: true,
			},
			e: NewAuthError(errors.New("auth error II")),
		},
		"obfuscated seccomp error run stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			e: NewSeccompError(errors.New("seccomp error")),
		},
		"obfuscated panic error run stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			e: NewPanicError(errors.New("panic error")),
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			MessageError(tt.c, m, tt.e)
		})
	}
}

func TestIsObfuscatedErr(t *testing.T) {
	var tmpErr ObfuscatedErr

	tests := map[string]struct {
		e    error
		want bool
	}{
		"base error": {
			e:    errors.New("test"),
			want: false,
		},
		"broker error": {
			e:    NewBrokerError(errors.New("broker")),
			want: true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if got := IsObfuscatedErr(tt.e, &tmpErr); got != tt.want {
				t.Errorf("IsObfuscatedErr() = %v, want %v", got, tt.want)
			}
		})
	}
}
