package batch

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type batchTests struct {
	job      Job
	settings Settings
	batch    configure.Batch
	arg      string
	msg      *mock_logging.MockMessenger

	prepFile func(t *testing.T, s string)

	assertError   func(t *testing.T, err error)
	assertString  func(t *testing.T, s string)
	assertManager func(t *testing.T, mng Manager)
}

func TestJob_checkIllegalArgs(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(
		gomock.Eq(`Illegal argument detected. Please remove: --Error=foo`),
	)
	m.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: -o --Time=1h"),
	)

	tests := map[string]batchTests{
		"empty illegal arguments and empty user arguments": {
			job: Job{},
			msg: m, // none
		},
		"arguments defined but not illegal argument detected": {
			job: Job{
				illegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
				userArgs:    []string{"-a", "test", "-b"},
			},
			msg: m, //none
		},
		"multiple illegal arguments found": {
			job: Job{
				illegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
				userArgs:    []string{"-o", "output", "-b", "--Time=1h"},
			},
			msg: m,
		},
		"single illegal argument found": {
			job: Job{
				illegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
				userArgs:    []string{"-a", "test", "--Error=foo"},
			},
			msg: m,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			tt.job.checkIllegalArgs(tt.msg)
		})
	}
}

func Test_NewBatchJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(
		"No %s variable detected, please check your CI job if this is unexpected.",
		"SCHEDULER_PARAMETERS",
	).Times(1)

	tests := map[string]batchTests{
		"valid Manager created, no SchedulerBin or ArgumentsVariable defined": {
			settings: Settings{
				BatchCmd:    "batch",
				StateCmd:    "state",
				StopCmd:     "stop",
				IllegalArgs: []string{"-a"},
			},
			batch: configure.Batch{},
			msg:   m,
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd(), "batch")
				assert.Equal(t, mng.StateCmd(), "state")
				assert.Equal(t, mng.StopCmd(), "stop")
				assert.Equal(
					t,
					mng.UserArgs(),
					"",
					"no user arguments should be defined",
				)
			},
		},
		"valid Manager created, SchedulerBin and ArgumentsVariables defined": {
			settings: Settings{
				BatchCmd: "batch",
				StateCmd: "state",
				StopCmd:  "stop",
			},
			batch: configure.Batch{
				SchedulerBin:      "/ci",
				ArgumentsVariable: []string{"PARAMS"},
			},
			msg: m,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd(), "/ci/batch")
				assert.Equal(t, mng.StateCmd(), "/ci/state")
				assert.Equal(t, mng.StopCmd(), "/ci/stop")
				assert.Equal(t, mng.UserArgs(), "--foo=bar -a")
			},
		},
		"valid Manager created, SchedulerBin but no ArgumentsVariables": {
			settings: Settings{
				BatchCmd: "batch",
				StateCmd: "state",
				StopCmd:  "stop",
			},
			batch: configure.Batch{
				SchedulerBin: "/ci",
			},
			msg: m,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd(), "/ci/batch")
				assert.Equal(t, mng.StateCmd(), "/ci/state")
				assert.Equal(t, mng.StopCmd(), "/ci/stop")
				assert.Equal(t, mng.UserArgs(), "--foo=bar -a")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.arg != "" {
				if len(tt.batch.ArgumentsVariable) != 0 {
					os.Setenv(envparser.UserEnvPrefix+tt.batch.ArgumentsVariable[0], tt.arg)
					defer os.Unsetenv(envparser.UserEnvPrefix + tt.batch.ArgumentsVariable[0])
				} else {
					os.Setenv(envparser.UserEnvPrefix+"SCHEDULER_PARAMETERS", tt.arg)
					defer os.Unsetenv(envparser.UserEnvPrefix + "SCHEDULER_PARAMETERS")
				}
			}

			mng := NewBatchJob(tt.settings, tt.batch, tt.msg)

			if tt.assertManager != nil {
				tt.assertManager(t, mng)
			}
		})
	}
}

func TestJob_NFSTimeout(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(
		"Invalid nfs_timeout configuration specified by runner configuration: %s",
		"time: invalid duration \"string\"",
	).Times(1)

	tests := map[string]batchTests{
		"valid time duration string provided": {
			arg: "1s",
			msg: m,
		},
		"invalid time duration string provided": {
			arg: "string",
			msg: m,
		},
		"empty string provided": {
			arg: "",
			msg: m,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			tt.job.NFSTimeout(tt.arg, tt.msg)
		})
	}
}

func TestJob_MonitorTermination(t *testing.T) {
	jobDone := make(chan struct{})
	defer close(jobDone)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	t.Run("routine closed after job is completed", func(t *testing.T) {
		j := Job{}
		go func() {
			time.Sleep(100 * time.Millisecond)
			jobDone <- struct{}{}
		}()
		j.MonitorTermination(ctx, jobDone, "1")
	})
	t.Run("job canceled and error encountered", func(t *testing.T) {
		j := Job{
			stopCmd: "cancel",
		}
		go func() {
			time.Sleep(100 * time.Millisecond)
			cancel()
		}()
		j.MonitorTermination(ctx, jobDone, "101")
	})
}
