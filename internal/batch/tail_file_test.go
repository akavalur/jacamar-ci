package batch

import (
	"io/ioutil"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

const (
	two    = 2 * time.Second
	ten    = 10 * time.Second
	twelve = 12 * time.Second
)

type tailTests struct {
	file      string
	maxWait   time.Duration
	filenames []string
	msg       logging.Messenger

	done         chan struct{}
	manageFile   func(string)
	closeChannel bool

	assertError      func(*testing.T, error)
	mockWatchFactory func(string, chan struct{}, logging.Messenger)
}

func Test_TailFiles(t *testing.T) {
	f, _ := ioutil.TempFile(t.TempDir(), "tailFiles")

	tests := map[string]tailTests{
		"no filenames provided": {
			filenames: []string{},
			done:      make(chan struct{}),
			maxWait:   ten,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err, "no filenames should not results in any error")
			},
		},
		"valid + invalid filename provided": {
			filenames: []string{f.Name(), "/random/file/123.txt"},
			done:      make(chan struct{}),
			maxWait:   ten,
			assertError: func(t *testing.T, err error) {
				assert.Equal(
					t,
					err.Error(),
					"unable to locate /random/file/123.txt before timeout",
				)
			},
		},
		"invalid time duration provided": {
			filenames: []string{},
			done:      make(chan struct{}),
			maxWait:   -1,
			assertError: func(t *testing.T, err error) {
				assert.Equal(t, err.Error(), "invalid time (-1ns) provided, must be >= 0")
			},
		},
		"no maxTime configured + invalid filename": {
			filenames: []string{"/random/file/123.txt"},
			done:      make(chan struct{}),
			maxWait:   0,
			assertError: func(t *testing.T, err error) {
				assert.Equal(
					t,
					err.Error(),
					"unable to locate /random/file/123.txt before timeout",
				)
			},
		},
		"invoke empty watchFactory": {
			filenames: []string{f.Name()},
			done:      make(chan struct{}),
			maxWait:   0,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			mockWatchFactory: func(file string, done chan struct{}, msg logging.Messenger) {
				return
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			defer close(tt.done)
			if tt.mockWatchFactory != nil {
				watchFactory = tt.mockWatchFactory
			}

			j := Job{}
			err := j.TailFiles(tt.filenames, tt.done, tt.maxWait, logging.NewMessenger())

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_waitFile(t *testing.T) {
	f, _ := ioutil.TempFile(t.TempDir(), "waitFile")

	tests := map[string]tailTests{
		"unable to find file + valid max wait time": {
			file:    "/random/file/123.txt",
			maxWait: twelve, // 12s
			assertError: func(t *testing.T, err error) {
				assert.Equal(t, err.Error(), "file /random/file/123.txt not found")
			},
		},
		"unable to find file + no max wait time": {
			file:    "/random/file/123.txt",
			maxWait: 0,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"test file exists + valid max timeout": {
			file:    f.Name(),
			maxWait: twelve,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"no test file declared": {
			file:    "",
			maxWait: twelve,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := waitFile(tt.file, tt.maxWait)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_watch(t *testing.T) {
	f1, _ := ioutil.TempFile(t.TempDir(), "watch")
	f2, _ := ioutil.TempFile(t.TempDir(), "watch")

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	noFile := mock_logging.NewMockMessenger(ctrl)
	noFile.EXPECT().Warn("open /random/file/123.txt: no such file or directory").AnyTimes()
	helloWorld := mock_logging.NewMockMessenger(ctrl)
	helloWorld.EXPECT().Stdout("Hello").Times(1)
	helloWorld.EXPECT().Stdout("World!").Times(1)

	tests := map[string]tailTests{
		"valid file provided and contents written": {
			file: f1.Name(),
			done: make(chan struct{}),
			msg:  helloWorld,
			manageFile: func(filename string) {
				time.Sleep(two)
				f, _ := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 700)
				_, _ = f.WriteString("Hello\nWorld!\n")
				time.Sleep(two)
			},
		},
		"no filename provided": {
			file: "",
			done: make(chan struct{}),
		},
		"invalid filename provided": {
			file: "/random/file/123.txt",
			done: make(chan struct{}),
			msg:  noFile,
		},
		"valid file provided and removed unexpectedly": {
			file: f2.Name(),
			done: make(chan struct{}),
			// Simulating the w.Error event cannot be done predictably.
			msg: logging.NewMessenger(),
			manageFile: func(filename string) {
				time.Sleep(two)
				_ = os.Remove(filename)
			},
		},
	}

	for name, tt := range tests {
		var wg sync.WaitGroup
		wg.Add(1)

		go func() {
			defer wg.Done()
			if tt.manageFile != nil {
				tt.manageFile(tt.file)
			}
			time.Sleep(two)
			close(tt.done)
		}()

		t.Run(name, func(t *testing.T) {
			watch(tt.file, tt.done, tt.msg)
		})
		wg.Wait()
	}
}

func TestCreateFiles(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn("Error while attempting to create file (%s): %s", "/invalid/file/name", "open /invalid/file/name: no such file or directory").Times(1)

	tests := map[string]struct {
		files       []string
		msg         logging.Messenger
		assertFiles func(*testing.T, []string)
	}{
		"create files successfully": {
			files: []string{"/tmp/file1", "/tmp/file2"},
			msg:   m,
			assertFiles: func(t *testing.T, s []string) {
				for _, f := range s {
					info, err := os.Stat(f)
					assert.NoError(t, err, "unable to state file")
					assert.Equal(t, "-rw-------", info.Mode().String())
				}
			},
		},
		"error encountered while attempting to create files": {
			files: []string{"/invalid/file/name"},
			msg:   m,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			CreateFiles(tt.files, tt.msg)

			if tt.assertFiles != nil {
				tt.assertFiles(t, tt.files)
			}
		})
	}
}
