package batch

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/radovskyb/watcher"

	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

var watchFactory = watch

func (j Job) TailFiles(
	filenames []string,
	done chan struct{},
	maxWait time.Duration,
	msg logging.Messenger,
) error {
	if maxWait < 0 {
		return fmt.Errorf("invalid time (%v) provided, must be >= 0", maxWait)
	}

	for _, file := range filenames {
		if err := waitFile(file, maxWait); err != nil {
			return fmt.Errorf("unable to locate %s before timeout", file)
		}
	}

	var wg sync.WaitGroup
	wg.Add(len(filenames))
	for _, file := range filenames {
		f := file
		go func() {
			defer wg.Done()
			watchFactory(f, done, msg)
		}()
	}
	wg.Wait()

	return nil
}

// waitFile waits until provided time for a file to exist.
func waitFile(filename string, maxWait time.Duration) error {
	sleep := 10 * time.Second // 10 second, default duration.

	for !fileExists(filename) {
		maxWait = maxWait - time.Duration(sleep)
		if maxWait < 0 {
			return fmt.Errorf("file %s not found", filename)
		}
		time.Sleep(sleep)
	}
	return nil
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// watch uses the defined watcher library to examine the provided file for any changes and
// print them to standard out.
func watch(filename string, done chan struct{}, msg logging.Messenger) {
	w := watcher.New()

	w.SetMaxEvents(1)
	w.FilterOps(watcher.Write)

	file, err := os.Open(filepath.Clean(filename))
	if err != nil {
		msg.Warn(err.Error())
		return
	}

	/* #nosec */
	// file is not written too
	defer file.Close()

	go func() {
		defer w.Close()
		finished := false

		for {
			scanner := bufio.NewScanner(file)
			for scanner.Scan() {
				text := scanner.Text()
				msg.Stdout(text)
			}

			if finished {
				return
			}

			_, err := file.Seek(0, os.SEEK_CUR)
			if err != nil {
				return
			}

			select {
			case <-w.Event:
				continue
			case <-w.Error:
				msg.Warn("Error while attempting to watch: %s, output interrupted", filename)
				finished = true
			case <-w.Closed:
				finished = true
			case <-done:
				finished = true
			}
		}
	}()

	if err := w.Add(filename); err != nil {
		msg.Warn(err.Error())
		return
	}

	// Start the watching process - it'll check for changes every 5s.
	if err := w.Start(time.Second * 5); err != nil {
		msg.Warn(err.Error())
	}
}

// CreateFiles takes a list of files (prefer full path) and generates
// and empty file. Errors are printed as warnings.
func CreateFiles(files []string, msg logging.Messenger) {
	for _, file := range files {
		fp, _ := filepath.Abs(file)
		/* #nosec */
		// variable file path required
		_, err := os.OpenFile(fp, os.O_CREATE, 0600)
		if err != nil {
			msg.Warn("Error while attempting to create file (%s): %s", file, err.Error())
		}
	}
}
