// Package augmenter offers a rule enforced mechanism for updating
// the GitLab generated CI job script to conform to required behaviors.
package augmenter

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
)

// Rules defines strict regulations for any actions that will be directed towards a
// provided script. No values are required and default actions are defined in all cases.
type Rules struct {
	// UnrestrictedCmdline allows for unfettered usages of tokens via the command line in
	// previously identified locations (git, artifact-uploader, and artifact-downloader).
	// Enabling this will allow these identified commands to run without restriction and
	// should only be done in control environments or when proc(5) has been mounted
	// to restrict access.
	UnrestrictedCmdline bool
	// AllowUserCredentials indicates if Git will allow credential storage are part of
	// the CI process. This behavior is dictated by the user's global Git configuration
	// and as such may not be present. On shared/static resources enabling this may
	// break concurrently running job.
	AllowUserCredentials bool
	// TrustedHost is the trusted upstream hostname (e.g., gitlab.example.com )used in
	// find-and-replace functionality.
	TrustedHost string
	// TargetHost replaces all instances of TrustedURL. You must provide both Trusted
	// and Target values.
	TargetHost string
	// TrustedJobToken will act as a trust worthy source for the existing CI_JOB_TOKEN to
	// assist in replacing all instance without the need to rely on a static list of
	// variables. At this time it is required to supply a value if you are going to
	// also attempt to introduce a TargetJobToken.
	TrustedJobToken string
	// TargetJobToken replaces all instances of the TrustedJobToken. You must provide both
	// Trusted and Target values.
	TargetJobToken string
	// RedactedEnvVars can be defined with a list of environment variable keys of which
	// whose values will be redacted from the script. All keys are case sensitive.
	RedactedEnvVars  []string
	redactedPrefixes []string
}

const (
	artifacts  = `$\'gitlab-runner\' "artifacts-`
	gitCIUser  = `gitlab-ci-token`
	gitConfigs = `$\'git\' "config" "-f"`
	gitHelpers = `"credential.helper" "\"\""\n`
	gitRemote  = `$\'git\' "remote"`
)

// JobScript accepts a filepath  of a CI job script as well as the associated
// sub-stage (https://docs.gitlab.com/runner/executors/custom.html#run) identified
// during run_exec as well as a trust CI_PROJECT_DIR equivalent.
// These details are used in conjunction with the constraints
// established by the rules to ingest the script and restructure it accordingly.
// The finalized script is returned in its completion as an un-encoded string.
func (r Rules) JobScript(filepath, stage string, env envparser.ExecutorEnv) (string, error) {
	contents, err := openScript(filepath)
	if err != nil {
		return "", err
	}

	// At this time we do not support any changes to the prepare_script.
	if stage != "prepare_script" {
		r.redactedPrefixes = r.prepRedacted()
		for i, line := range contents {
			if strings.HasPrefix(line, ": | eval") {
				eval := r.realize(
					stage,
					env,
					strings.Split(line, `\n`),
				)
				contents[i] = strings.Join(eval, `\n`)
			}
		}
	}

	return strings.Join(contents, "\n"), nil
}

func (r Rules) prepRedacted() (redacted []string) {
	for _, s := range r.RedactedEnvVars {
		// Being explicit with the export statement should ensure we only
		// attempt to locate and redact runner generated variables.
		redacted = append(redacted, "export "+s+"=$\\'")
	}
	return redacted
}

func (r Rules) realize(stage string, env envparser.ExecutorEnv, eval []string) []string {
	switch {
	case !r.UnrestrictedCmdline:
		eval = r.restrictCmdLine(stage, env, eval)
		fallthrough
	case !r.AllowUserCredentials && stage == "get_sources":
		eval = r.restrictUserCreds(eval)
		fallthrough
	case len(r.redactedPrefixes) != 0:
		eval = r.redactVars(eval)
		fallthrough
	case r.TrustedHost != "" && r.TargetHost != "":
		eval = replaceSlice(eval, urlHost(r.TrustedHost), urlHost(r.TargetHost))
		fallthrough
	case r.TrustedJobToken != "" && r.TargetJobToken != "":
		eval = replaceSlice(eval, r.TrustedJobToken, r.TargetJobToken)
	}

	return eval
}

func urlHost(s string) string {
	u, err := url.Parse(s)
	if err != nil || u.Host == "" {
		return s
	}

	return u.Host
}

// restrictCmdLine realizes the specific implementation of the UnrestrictedCmdline rule.
func (r Rules) restrictCmdLine(stage string, env envparser.ExecutorEnv, eval []string) []string {
	if potentialGitStage(stage) {
		eval = append(eval, "")
		copy(eval[2:], eval[1:])
		eval[1] = `export GIT_ASKPASS=$\'` + gitAskpassFile(env) + `\'`
	}

	if stage == "get_sources" {
		eval = containsAction(eval, gitRemote, gitRemoveToken)
	}

	if strings.Contains(stage, "artifacts") {
		eval = containsAction(eval, artifacts, artifactsExportToken)
	}

	return eval
}

// potentialGitStage identifies if the stage has the potential for Git command.
func potentialGitStage(stage string) bool {
	if stage == "get_sources" || stage == "after_script" || stage == "build_script" {
		return true
	}

	// We are currently unclear on the functionality of all potential step_* scripts,
	// as such it is safest to treat them all as we had a build_script.
	return strings.HasPrefix(stage, "step_")
}

func containsAction(eval []string, substr string, action func(string) string) []string {
	for i, val := range eval {
		if strings.Contains(val, substr) {
			eval[i] = action(val)
		}
	}
	return eval
}

func replaceSlice(s []string, old, new string) []string {
	for i := range s {
		s[i] = strings.ReplaceAll(s[i], old, new)
	}
	return s
}

// restrictUserCreds realizes the specific implementation of the AllowUserCredentials rule by identifying and
// updating an identifiable Git configuration rule in the appropriate script:
// `$\'git\' "config" "-f" "/builds-dir/project.tmp/git-template/config" "???"\n`
// Since the target configuration file directory is not known we extract it from a matching command and
// use it when constructing the required configurations. If no matching command found the process
// will complete without error/modification to the slice under evaluation..
func (r Rules) restrictUserCreds(eval []string) []string {
	updated := false // If eval has been updated and process can be stopped?

	for i := range eval {
		if strings.HasPrefix(eval[len(eval)-1-i], gitConfigs) {
			tmpLine := strings.Replace(eval[len(eval)-1-i], gitConfigs, "", 1)
			splits := strings.SplitN(tmpLine, `"`, -1)
			for _, entry := range splits {
				if strings.Contains(entry, "git-template") {
					eval[len(eval)-1-i] = strings.Join([]string{
						gitConfigs,
						entry,
						gitHelpers,
						eval[len(eval)-1-i],
					}, " ")
					updated = true
				}
			}
		}

		if updated {
			break
		}
	}
	return eval
}

// redactVars realizes the specific implementation of the RedactedEnvVars rule.
func (r Rules) redactVars(eval []string) []string {
	for _, pre := range r.redactedPrefixes {
		for i, val := range eval {
			if strings.HasPrefix(val, pre) {
				eval[i] = pre + "VALUE_REMOVED\\'"
				// skip once a single redaction has been found ?
			}
		}
	}
	return eval
}

// gitRemoveToken parses any provided git command for a token in the https:// url
// and removes it. Example url where the goal is to remove 'ciJobToken':
// "https://gitlab-ci-token:ciJobToken@gitlab.example.com/user/scratch-space.git"
func gitRemoveToken(s string) string {
	i := strings.Index(s, gitCIUser)
	j := strings.Index(s[i:], "@")
	if i > -1 && j > -1 {
		pre := s[:i+len(gitCIUser)]
		post := (s[i:])[j:]
		s = pre + post
	}
	return s
}

func openScript(filepath string) (s []string, err error) {
	/* #nosec */
	// variable file path required
	b, err := ioutil.ReadFile(filepath)
	if err != nil {
		return
	}
	s = strings.Split(string(b), "\n")
	return
}

// artifactsExportToken removes the use of --id to provide a token to the
// artifact uploader/downloader and instead relies on export (Bash).
func artifactsExportToken(s string) string {
	// $\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/"
	// "--token" "ciJobToken" "--id" "12"
	re := regexp.MustCompile(`"--token"\s"(.*?)"`)
	match := re.FindAllStringSubmatch(s, -1)
	for _, v := range match {
		var sb strings.Builder
		sb.WriteString(`export CI_JOB_TOKEN=$\'`)
		sb.WriteString(v[1])
		sb.WriteString(`\' && `)
		sb.WriteString(strings.ReplaceAll(s, v[0], ""))
		s = sb.String()
	}

	return s
}

// CreateGitAskpass safely generates a script for handling Git credentials
// (see https://git-scm.com/docs/gitcredentials) that will provide the token
// back to any git command so long as the GIT_ASKPASS environment variable
// is set. Invoking this function is required to realize the AllowUserCredentials
// rule but remain distinct to support distinct workflows that come with
// executing CI on a behalf of a user.
func CreateGitAskpass(env envparser.ExecutorEnv, msg logging.Messenger) error {
	gitFP := gitAskpassFile(env)
	credFolder := strings.TrimSuffix(gitFP, "/pass")
	if _, err := os.Stat(credFolder); os.IsNotExist(err) {
		// Create the missing folders.
		if err := os.MkdirAll(credFolder, usertools.FilePermissions); err != nil {
			return err
		}
	}

	gitContents := fmt.Sprintf("#!/usr/bin/env bash\n\nbuiltin echo %s\n", env.RequiredEnv.JobToken)

	if err := usertools.CreateScript(gitFP, gitContents); err != nil {
		return err
	}

	if envparser.GitTrace() {
		msg.Stdout("GIT_ASKPASS script created created: %s", gitFP)
	}

	return nil
}

// CleanupGitAskpass removes the previously creates GIT_ASKPASS related script. Any error
// encountered in the process will be ignored.
func CleanupGitAskpass(env envparser.ExecutorEnv, msg logging.Messenger) {
	if env.StatefulEnv.BuildsDir != "" {
		file := gitAskpassFile(env)
		if _, err := os.Stat(file); err == nil {
			err = os.Remove(file)
			if err == nil && envparser.GitTrace() {
				msg.Stdout("GIT_ASKPASS script removed: %s", file)
			}
		}
	}
}

// gitAskpassFile returns the fully path to any credentials script created for the job.
func gitAskpassFile(env envparser.ExecutorEnv) string {
	var sb strings.Builder
	sb.WriteString(env.StatefulEnv.BuildsDir)
	sb.WriteString("/.credentials/")
	if env.StatefulEnv.ProjectPath != "" {
		sb.WriteString(env.StatefulEnv.ProjectPath)
		sb.WriteString("/")
	}
	sb.WriteString("pass")

	return filepath.Clean(sb.String())
}

// LoginShellScript updates the provided Bash script to ensure a login shell is provided.
func LoginShellScript(file string) error {
	content, err := openScript(file)
	if err != nil {
		return err
	}

	// Basic sanity check so we don't cause a panic or edit a script without a bang operator.
	if len(content) < 1 || !strings.Contains(content[0], "bash") {
		return errors.New("unexpect script detected, only runner generated currently supported")
	}

	content[0] = `#!/bin/bash --login`
	output := strings.Join(content, "\n")

	/* #nosec */
	// creating script, 700 permissions required
	err = ioutil.WriteFile(file, []byte(output), 0700)

	return err
}
