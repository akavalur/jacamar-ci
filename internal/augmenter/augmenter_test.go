package augmenter

import (
	"bytes"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type rTests struct {
	stage    string
	filepath string
	token    string
	eval     []string

	env envparser.ExecutorEnv
	msg logging.Messenger
	r   Rules

	mockSomething func(*testing.T, string)
	assertError   func(*testing.T, error)
	assertString  func(*testing.T, string)
	assertSlice   func(*testing.T, []string)
}

func TestRules_JobScript(t *testing.T) {
	tests := map[string]rTests{
		"nonexistent filepath provided": {
			filepath: "/file/does/not/exist",
			stage:    "prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"existing prepare_script, ensure no changes are made currently": {
			filepath: "../../test/testdata/job-scripts/ci/prepare_script",
			stage:    "prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, `#!/usr/bin/env bash

set -eo pipefail
set +o noclobber
: | eval $'echo "Running on $(hostname)..."\n'
exit 0
`, s)
			},
		},
		"identify and remove target redacted environment variables only": {
			filepath: "../../test/testdata/job-scripts/ci/redactedenvvar_test",
			stage:    "after_script",
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "101",
				},
			},
			r: Rules{
				UnrestrictedCmdline:  true,
				AllowUserCredentials: true,
				RedactedEnvVars:      []string{"FOO", "BAR"},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash")
				cmd.Stdin = bytes.NewBufferString(s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "functional script expected")
				assert.Equal(
					t,
					"pass VALUE_REMOVED VALUE_REMOVED\n",
					string(out),
					"expected variables not removed",
				)
			},
		},
		"verify GIT_ASKPASS variable set for job script": {
			filepath: "../../test/testdata/job-scripts/ci/askpass_test",
			stage:    "build_script",
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "202",
				},
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: "/builds/0/test",
				},
			},
			r: Rules{
				UnrestrictedCmdline:  false,
				AllowUserCredentials: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash")
				cmd.Stdin = bytes.NewBufferString(s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "functional script expected")
				assert.Equal(
					t,
					"/builds/0/test/.credentials/pass\n",
					string(out),
					"expected variables not removed",
				)
			},
		},
		"modify get_sources scripts to remove token from URLs": {
			filepath: "../../test/testdata/job-scripts/13.0.0/get_sources",
			stage:    "get_sources",
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "303",
				},
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: "/builds/0/test",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				re := regexp.MustCompile(
					`"origin"\s"https://gitlab-ci-token@gitlab\.example\.com/user/scratch-space\.git"`,
				)
				match := re.FindAllStringIndex(s, -1)
				assert.Equal(
					t,
					2,
					len(match),
					"expected that two add remote commands have been updated",
				)
			},
		},
		"ensure that --token has been removed from artifact command": {
			filepath: "../../test/testdata/job-scripts/13.0.0/get_sources",
			stage:    "download_artifacts",
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "404",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.False(
					t,
					strings.Contains(s, "--token"),
					"no --token argument should be found",
				)
				assert.True(t, strings.Contains(s, "export CI_JOB_TOKEN="))
			},
		},
		"verify rules established for broker (replace token & url)": {
			filepath: "../../test/testdata/job-scripts/ci/broker_rules",
			stage:    "after_script",
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "505",
				},
			},
			r: Rules{
				TrustedHost:     "gitlab.example.gov",
				TargetHost:      "broker.example.gov",
				TrustedJobToken: "ciJobToken",
				TargetJobToken:  "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.NotContains(t, s, "ciJobToken")
				assert.NotContains(t, s, "gitlab.example.gov")
			},
		},
		"verify GIT_ASKPASS variable set for job step_script": {
			filepath: "../../test/testdata/job-scripts/ci/askpass_test",
			stage:    "step_script",
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "606",
				},
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: "/builds/0/test",
				},
			},
			r: Rules{
				UnrestrictedCmdline:  false,
				AllowUserCredentials: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash")
				cmd.Stdin = bytes.NewBufferString(s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "functional script expected")
				assert.Equal(t, "/builds/0/test/.credentials/pass\n", string(out))
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockSomething != nil {
				tt.mockSomething(t, tt.filepath)
			}

			got, err := tt.r.JobScript(tt.filepath, tt.stage, tt.env)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_openScript(t *testing.T) {
	tests := map[string]rTests{
		"nonexistent filepath provided": {
			filepath: "/file/does/not/exist",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"simple, existing prepare_script": {
			filepath: "../../test/testdata/job-scripts/13.0.0/prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Equal(t, "#!/usr/bin/env bash", s[0])
				assert.Equal(t, "", s[1])
				assert.Equal(t, "exit 0", s[5])
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := openScript(tt.filepath)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertSlice != nil {
				tt.assertSlice(t, got)
			}
		})
	}
}

func TestCreateGitAskpass(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	os.Setenv("CUSTOM_ENV_GIT_TRACE", "1")
	defer os.Unsetenv("CUSTOM_ENV_GIT_TRACE")

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Stdout("GIT_ASKPASS script created created: %s", gomock.Any()).AnyTimes()

	tempDir, err := ioutil.TempDir(t.TempDir(), "gitaskpass")
	assert.NoError(t, err)
	_ = os.Mkdir(tempDir+"/.credentials", 0700)

	tests := map[string]rTests{
		"a .credentials directory does not exists": {
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "101",
				},
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: tempDir + "/",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"generate valid credentials script": {
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID:    "202",
					JobToken: "T0k3n",
				},
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: tempDir,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash", s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "error attempting to execute generated script")
				assert.Equal(t, "T0k3n\n", string(out))
			},
		},
		"overwrite an existing valid credentials script": {
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID:    "303",
					JobToken: "NEW_T0k3n",
				},
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: tempDir,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash", s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "error attempting to execute generated script")
				assert.Equal(t, "NEW_T0k3n\n", string(out))
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockSomething != nil {
				tt.mockSomething(t, tt.env.StatefulEnv.BuildsDir)
			}

			err := CreateGitAskpass(tt.env, msg)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}

			if tt.assertString != nil {
				// Overload in this case to support file verification.
				tt.assertString(t, gitAskpassFile(tt.env))
			}
		})
	}
}

func Test_gitRemoveToken(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "receive generic https url",
			s:    "https://gitlab-ci-token:ciJobToken@gitlab.example.com/user/scratch-space.git",
			want: "https://gitlab-ci-token@gitlab.example.com/user/scratch-space.git",
		}, {
			name: "receive generic http url",
			s:    "http://gitlab-ci-token:ciJobToken@gitlab.example.com/user/scratch-space.git",
			want: "http://gitlab-ci-token@gitlab.example.com/user/scratch-space.git",
		}, {
			name: "job token has already been removed",
			s:    "http://gitlab-ci-token@gitlab.example.com/user/scratch-space.git",
			want: "http://gitlab-ci-token@gitlab.example.com/user/scratch-space.git",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := gitRemoveToken(tt.s); got != tt.want {
				t.Errorf("gitRemoveToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_artifactsExportToken(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "artifact-uploader command, token provided",
			s:    `$\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/" "--token" "ciJobToken" "--id" "12"`,
			want: `export CI_JOB_TOKEN=$\'ciJobToken\' && $\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/"  "--id" "12"`,
		}, {
			name: "artifact-downloader command, token provided",
			s:    `$\'gitlab-runner\' "artifacts-downloader" "--token" "ciJobToken"`,
			want: `export CI_JOB_TOKEN=$\'ciJobToken\' && $\'gitlab-runner\' "artifacts-downloader" `,
		}, {
			name: "artifact-uploader command, no token provided",
			s:    `$\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/" "--id" "12"`,
			want: `$\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/" "--id" "12"`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := artifactsExportToken(tt.s); got != tt.want {
				t.Errorf("artifactsExportToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_gitAskpassFile(t *testing.T) {
	t.Run("ensure default credentials folder returned", func(t *testing.T) {
		got := gitAskpassFile(envparser.ExecutorEnv{
			StatefulEnv: envparser.StatefulEnv{
				BuildsDir:   "/builds/dir/",
				ProjectPath: "group/sub/project",
			},
			RequiredEnv: envparser.RequiredEnv{
				JobID: "101",
			},
		})
		assert.Equal(t, "/builds/dir/.credentials/group/sub/project/pass", got, "no suffix provided")
	})
}

func Test_urlHost(t *testing.T) {
	tests := map[string]struct {
		s    string
		want string
	}{
		"invalid url provided": {
			// Ensuring the validity of a url string is not in the scope of the package.
			s:    "notaurl",
			want: "notaurl",
		},
		"host only defined": {
			s:    "gitlab.example.com",
			want: "gitlab.example.com",
		},
		"empty string": {
			s:    "",
			want: "",
		},
		"traditional url": {
			s:    "https://gitlab.example.com",
			want: "gitlab.example.com",
		},
		"traditional url with port": {
			s:    "https://gitlab.example.com:3000",
			want: "gitlab.example.com:3000",
		},
		"traditional url with slash": {
			s:    "https://gitlab.example.com/",
			want: "gitlab.example.com",
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if got := urlHost(tt.s); got != tt.want {
				t.Errorf("urlHost() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLoginShellScript(t *testing.T) {
	tests := map[string]struct {
		file        string
		prepFile    func(*testing.T, string) string
		assertFile  func(*testing.T, string)
		assertError func(*testing.T, error)
	}{
		"no script found": {
			file: "/file/does/not/exist",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"empty file": {
			prepFile: func(t *testing.T, s string) string {
				newFile := t.TempDir() + "/job.bash"
				err := ioutil.WriteFile(newFile, []byte(""), 0700)
				assert.NoError(t, err, "failure preparing test script")
				return newFile
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unexpect script detected, only runner generated currently supported")
			},
		},
		"update simple example script": {
			file: "../../test/testdata/job-scripts/ci/prepare_script",
			prepFile: func(t *testing.T, s string) string {
				newFile := t.TempDir() + "/job.bash"
				cmd := exec.Command("cp", s, newFile)
				_, err := cmd.CombinedOutput()
				assert.NoError(t, err, "failure preparing test script")
				return newFile
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertFile: func(t *testing.T, s string) {
				b, err := ioutil.ReadFile(s)
				if err != nil {
					return
				}

				assert.Equal(t, `#!/bin/bash --login

set -eo pipefail
set +o noclobber
: | eval $'echo "Running on $(hostname)..."\n'
exit 0
`, string(b))
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepFile != nil {
				tt.file = tt.prepFile(t, tt.file)
			}

			err := LoginShellScript(tt.file)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertFile != nil {
				tt.assertFile(t, tt.file)
			}
		})
	}
}

func TestRules_restrictUserCreds(t *testing.T) {
	tests := map[string]rTests{
		"update get_sources script": {
			eval: []string{
				`export CI_RUNNER_EXECUTABLE_ARCH=$\'linux/amd64\'\n`,
				`export GIT_LFS_SKIP_SMUDGE=1\n`,
				`echo $\'\\x1b[32;1mFetching changes with git depth set to 50...\\x1b[0;m\'\n`,
				`$\'mkdir\' "-p" "/tmp/user/scratch-space.tmp/git-template"\n`,
				`$\'git\' "config" "-f" "/tmp/user/scratch-space.tmp/git-template/config" "fetch.recurseSubmodules" "false"\n`,
				`$\'git\' "config" "-f" "/tmp/user/scratch-space.tmp/git-template/config" "http.https://gitlab.example.com.sslCAInfo" "$CI_SERVER_TLS_CA_FILE"\n`,
				`$\'git\' "init" "/tmp/user/scratch-space" "--template" "/tmp/user/scratch-space.tmp/git-template"\n$\'cd\' "/tmp/user/scratch-space"\n`,
				`if $\'git\' "remote" "add" "origin" "https://gitlab-ci-token:ciJobToken@gitlab.example.com/user/scratch-space.git" >/dev/null 2>/dev/null; then\n`,
			},
			assertSlice: func(t *testing.T, i []string) {
				assert.Len(t, i, 8)
				assert.Contains(t, i[5], "credential.helper")
			},
		},
		"empty eval provided": {
			eval: []string{},
			assertSlice: func(t *testing.T, i []string) {
				assert.Len(t, i, 0)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := tt.r.restrictUserCreds(tt.eval)

			if tt.assertSlice != nil {
				tt.assertSlice(t, got)
			}
		})
	}
}

func TestCleanupGitAskpass(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Run("no builds directory defined (lacking state)", func(t *testing.T) {
		// Nothing should occur...
		CleanupGitAskpass(envparser.ExecutorEnv{}, nil)
	})
	t.Run("file cannot be removed", func(t *testing.T) {
		// Nothing should occur...
		CleanupGitAskpass(envparser.ExecutorEnv{}, nil)
	})
	t.Run("file removed, GIT_TRACE", func(t *testing.T) {
		folder := t.TempDir()
		env := envparser.ExecutorEnv{
			StatefulEnv: envparser.StatefulEnv{
				BuildsDir: folder,
			},
		}

		err := CreateGitAskpass(env, nil)
		if err != nil {
			assert.NoError(t, err, "error generating test files")
			return
		}

		_ = os.Setenv("CUSTOM_ENV_GIT_TRACE", "1")
		defer os.Unsetenv("CUSTOM_ENV_GIT_TRACE")

		msg := mock_logging.NewMockMessenger(ctrl)
		msg.EXPECT().Stdout("GIT_ASKPASS script removed: %s", gomock.Any())

		CleanupGitAskpass(env, msg)
	})
}
