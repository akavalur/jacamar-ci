package version

import (
	"strings"
)

var (
	version   string
	gitCommit string
	gitBranch string
	goVersion string
	buildDate string
)

// Obtain returns properly formatted version (--version) details.
func Obtain() string {
	var sb strings.Builder

	sb.WriteString("Version: " + version + "\n")

	if gitCommit != "" {
		sb.WriteString("Git Commit: " + gitCommit + "\n")
	}
	if gitBranch != "" {
		sb.WriteString("Git Branch: " + gitBranch + "\n")
	}

	sb.WriteString("Go Version: " + goVersion + "\n")
	sb.WriteString("Built: " + buildDate + "\n")

	return sb.String()
}

// Version returns Jacamar's current build version.
func Version() string {
	return version
}
