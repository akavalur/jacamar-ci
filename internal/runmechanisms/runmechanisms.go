package runmechanisms

import (
	"context"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

// Runner implements a supported interface with a goal of allowing commands (provided
// in stdin form) to be executed by an underlying layer remaining consistent across
// job execution.
type Runner interface {
	// JobScriptOutput interacts the underlying mechanism to execute the (GitLab generated)
	// script, integrating the executor specified stdin. The expected script (file path)
	// is provided and the runner will ensure that both the target execution layer as well as
	// admin defined downscope is observed:
	// 		<Commander defined environment> $ <stdin> <runner command (optional)> <script>
	// It is not true in all cases that the Runner will interject specific commands that
	// relate to script execution. All output from the job script's execution will be piped
	// to stdout/stderr.
	JobScriptOutput(script string, stdin ...interface{}) error
	// JobScriptReturn functions almost identical to JobScriptOutput but returning
	// stdout/stderr as a string.
	JobScriptReturn(script string, stdin ...interface{}) (string, error)
	// PipeOutput executes the provided stdin. All output from the command is piped directly
	// to stdout/stderr. Bypasses the runner command and instead rely solely on the its
	// underlying base/environment layer to avoid execution on such resources.
	PipeOutput(stdin string) error
	// ReturnOutput executes the provided stdin. All output from the command is returned as
	// a string. Bypasses the runner command and instead rely solely on the its underlying
	// base/environment layer to avoid execution on such resources.
	ReturnOutput(stdin string) (string, error)
	// CommandDir changes the directory where command will be executed.
	CommandDir(dir string)
	// SigtermReceived returns a boolean based upon if a SIGTERM has been captured.
	SigtermReceived() bool
	// RequestContext returns the commander context, used to identify if a SIGTERM has been captured
	// and Jacamar must being a graceful of shutdown process.
	RequestContext() context.Context
	// TransferScript leverages any underlying commander to provides the job scripts
	// contents via environment variables.
	TransferScript(path, stage string, env envparser.ExecutorEnv, cfg configure.Auth) error
	// BuildsDir returns the directory for reporting to GitLab.
	BuildsDir() string
	// CacheDir returns the directory for reporting to GitLab.
	CacheDir() string
	// ModifyCmd updates the underlying execute the named program with the given arguments.
	ModifyCmd(name string, arg ...string)
}
