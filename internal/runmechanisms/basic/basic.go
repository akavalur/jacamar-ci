// Package basic implements a non-interfering runner interface that
// redirects all input to the underlying commander for execution.
package basic

import (
	"context"
	"fmt"
	"net/url"
	"os"
	"reflect"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

type mechanism struct {
	cmdr      command.Commander
	buildsDir string
	cacheDir  string
}

func (m *mechanism) JobScriptOutput(script string, stdin ...interface{}) error {
	if m.cmdr.SigtermReceived() {
		// SIGTERM captured, don't launch new job scripts.
		return nil
	}

	return m.PipeOutput(m.prepStdin(script, stdin...))
}

func (m *mechanism) JobScriptReturn(script string, stdin ...interface{}) (string, error) {
	if m.cmdr.SigtermReceived() {
		// SIGTERM captured, don't launch new job scripts.
		return "", nil
	}

	return m.ReturnOutput(m.prepStdin(script, stdin...))
}

func (m *mechanism) prepStdin(script string, stdin ...interface{}) string {
	var sb strings.Builder

	r := reflect.ValueOf(stdin)
	if !r.IsZero() {
		sb.WriteString(fmt.Sprint(stdin...))
		sb.WriteString(" ")
	}

	sb.WriteString(script)

	return sb.String()
}

func (m *mechanism) PipeOutput(stdin string) error {
	return m.cmdr.PipeOutput(stdin)
}

func (m *mechanism) ReturnOutput(stdin string) (string, error) {
	return m.cmdr.ReturnOutput(stdin)
}

func (m *mechanism) CommandDir(dir string) {
	m.cmdr.CommandDir(dir)
}

func (m *mechanism) SigtermReceived() bool {
	return m.cmdr.SigtermReceived()
}

func (m *mechanism) RequestContext() context.Context {
	return m.cmdr.RequestContext()
}

func (m *mechanism) BuildsDir() string {
	return m.buildsDir
}

func (m *mechanism) CacheDir() string {
	return m.cacheDir
}

func (m *mechanism) TransferScript(
	path string,
	stage string,
	env envparser.ExecutorEnv,
	cfg configure.Auth,
) error {
	rules := augmenter.Rules{
		UnrestrictedCmdline:  false,
		AllowUserCredentials: false,
	}

	if cfg.Broker.URL != "" {
		if err := brokerRules(&rules, env, cfg); err != nil {
			return err
		}
	}

	if cfg.RunAs.Federated || cfg.Broker.URL != "" {
		rules.RedactedEnvVars = append(rules.RedactedEnvVars, "CI_JOB_JWT")
	}

	contents, err := rules.JobScript(path, stage, env)
	if err != nil {
		return err
	}

	m.cmdr.AppendEnv(envparser.EstablishScriptEnv(contents, cfg.MaxEnvChars))

	return nil
}

func (m *mechanism) ModifyCmd(name string, arg ...string) {
	m.cmdr.ModifyCmd(name, arg...)
}

func brokerRules(
	rules *augmenter.Rules,
	env envparser.ExecutorEnv,
	cfg configure.Auth,
) error {
	trustURL, err := url.Parse(env.RequiredEnv.ServerURL)
	if err != nil {
		return fmt.Errorf("unable to parse trusted server URL: %w", err)
	}

	tarURL, err := url.Parse(cfg.Broker.URL)
	if err != nil {
		return fmt.Errorf("unable to parse configured target URL: %w", err)
	}

	if tarURL.Scheme != trustURL.Scheme {
		// We cannot safely support different schemes even for testing.
		return fmt.Errorf(
			"URL schemes for configured target (%s) does not match trusted (%s)",
			cfg.Broker.URL,
			env.RequiredEnv.ServerURL,
		)
	}

	rules.TrustedJobToken = os.Getenv("TRUSTED_CI_JOB_TOKEN")
	rules.TargetJobToken = env.StatefulEnv.BrokerToken
	rules.TrustedHost = trustURL.Host
	rules.TargetHost = tarURL.Host

	return nil
}

// NewMechanism generates a basic Runner interface that will insert
// no influence over stdin/script provided.
func NewMechanism(cmdr command.Commander, auth authuser.Authorized) *mechanism {
	return &mechanism{
		cmdr:      cmdr,
		buildsDir: auth.BuildsDir(),
		cacheDir:  auth.CacheDir(),
	}
}
