package validation

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func TestNewRunAs(t *testing.T) {
	testEnv := map[string]string{
		envparser.UserEnvPrefix + "RUNAS_USER":     "tester",
		envparser.UserEnvPrefix + "BAD_RUNAS_USER": "$(sudo rm -rf /)",
	}
	tst.SetEnv(testEnv)
	defer tst.UnsetEnv(testEnv)

	tests := map[string]struct {
		auth        configure.Auth
		assertError func(*testing.T, error)
		assertRunAs func(*testing.T, RunAsValidator)
	}{
		"no validation script/plugin defined": {
			assertError: tst.AssertNoError,
			assertRunAs: func(t *testing.T, r RunAsValidator) {
				assert.Nil(t, r)
			},
		},
		"invalid runas user detected in environment": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: "script",
					RunAsVariable:    "BAD_RUNAS_USER",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"invalid user provided account (defined by BAD_RUNAS_USER variable): Key: '' Error:Field validation for '' failed on the 'username' tag",
				)
			},
			assertRunAs: func(t *testing.T, r RunAsValidator) {
				assert.Nil(t, r)
			},
		},
		"script runas, no target user defined": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: "/example/script/file",
					RunAsVariable:    "",
				},
			},
			assertError: tst.AssertNoError,
			assertRunAs: func(t *testing.T, r RunAsValidator) {
				// We only need to verify the type here, we have other tests
				// that better examine functionality.
				assert.Equal(t, reflect.TypeOf(runAsScript{}), reflect.TypeOf(r))
			},
		},
		"script runas, target user defined": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: "/example/script/file",
					RunAsVariable:    "RUNAS_USER",
				},
			},
			assertError: tst.AssertNoError,
			assertRunAs: func(t *testing.T, r RunAsValidator) {
				assert.Equal(t, reflect.TypeOf(runAsScript{}), reflect.TypeOf(r))
			},
		},
		"plugins no longer supported": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationPlugin: "/example/plugin/file",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "plugin support has been removed from Jacamar, please migrate to validation_script")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := NewRunAs(tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRunAs != nil {
				tt.assertRunAs(t, got)
			}
		})
	}
}
