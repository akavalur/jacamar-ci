package validation

import (
	"io/ioutil"
	"path/filepath"
	"reflect"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

var testJWT gitlabjwt.Claims

type runasTests struct {
	auth        configure.Auth
	jwtClaims   gitlabjwt.Claims
	curUsername string

	assertError         func(*testing.T, error)
	assertRunAsOverride func(*testing.T, RunAsOverride)
}

func Test_runasEnv(t *testing.T) {
	t.Run("all initializer value should translate to environment variables", func(t *testing.T) {
		i := runAsCfg{
			targetUser: "targetUser",
		}.create(testJWT, "currentUser")

		got := runasEnv(i)
		assert.Equal(t, []string{
			"RUNAS_TARGET_USER=targetUser",
			"RUNAS_CURRENT_USER=currentUser",
			"JWT_JOB_ID=jobID",
			"JWT_NAMESPACE_ID=namespaceID",
			"JWT_PIPELINE_ID=pipelineID",
			"JWT_PROJECT_ID=projectID",
			"JWT_PROJECT_PATH=project/path",
			"JWT_USER_EMAIL=user@example.com",
			"JWT_USER_LOGIN=user",
			"JWT_USER_ID=userID",
		}, got)
	})

	t.Run("federation enabled, should override current username", func(t *testing.T) {
		fedJWT := testJWT
		fedJWT.FedUserName = "federatedUser"
		fedJWT.AuthToken = "T0k3n"

		i := runAsCfg{
			targetUser: "targetUser",
			federated:  true,
		}.create(fedJWT, "currentUser")

		got := runasEnv(i)
		assert.Equal(t, []string{
			"RUNAS_TARGET_USER=targetUser",
			"RUNAS_CURRENT_USER=federatedUser",
			"FEDERATED_AUTH_TOKEN=T0k3n",
			"FEDERATED_USERNAME=federatedUser",
			"JWT_JOB_ID=jobID",
			"JWT_NAMESPACE_ID=namespaceID",
			"JWT_PIPELINE_ID=pipelineID",
			"JWT_PROJECT_ID=projectID",
			"JWT_PROJECT_PATH=project/path",
			"JWT_USER_EMAIL=user@example.com",
			"JWT_USER_LOGIN=user",
			"JWT_USER_ID=userID",
		}, got)
	})
}

func Test_runasScript_Execute(t *testing.T) {
	testScript, _ := filepath.Abs("../../../test/scripts/unit/runas.bash")

	testEnv := map[string]string{
		envparser.UserEnvPrefix + "RUNAS_USER":   "pass",
		envparser.UserEnvPrefix + "RUNAS_TARGET": "target",
		envparser.UserEnvPrefix + "RUNAS_RANDOM": "random",
	}
	tst.SetEnv(testEnv)
	defer tst.UnsetEnv(testEnv)

	// Test logging using Pavilion2
	ll := logrus.New()
	ll.Out = ioutil.Discard
	sysLog := logrus.NewEntry(ll)

	tests := map[string]runasTests{
		"passing, valid script user + target": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					RunAsVariable:    "RUNAS_USER",
				},
			},
			curUsername: "user",
			assertError: tst.AssertNoError,
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "pass", over.Username)
			},
		},
		"no current user specified": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					RunAsVariable:    "RUNAS_TARGET",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unexpected system error encountered, no current user can be identified",
				)
			},
		},
		"passing, valid user only": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
				},
			},
			curUsername: "gitlab",
			assertError: tst.AssertNoError,
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "new", over.Username)
			},
		},
		"failing, invalid user + target": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					RunAsVariable:    "RUNAS_RANDOM",
				},
			},
			curUsername: "bad",
			assertError: tst.AssertError,
		},
		"failing, unable to unmarshall json returned": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
				},
			},
			curUsername: "json",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"invalid character ':' after top-level value",
				)
			},
		},
		"passing, shared group provided": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
				},
			},
			curUsername: "shared",
			assertError: tst.AssertNoError,
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "new", over.Username)
			},
		},
		"correct sha256, no payload or target username provided": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					SHA256:           "88293f1cc0017dba4b9e516301de078160cd5e2ceccc73ae6e7c83515a0e2392",
				},
			},
			curUsername: "none",
			assertError: tst.AssertNoError,
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "none", over.Username)
			},
		},
		"incorrect sha256 encountered for file": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					SHA256:           "123456",
				},
			},
			curUsername: "user",
			assertError: tst.AssertError,
		},
		"federation enabled and username declared in auth": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					Federated:        true,
				},
			},
			curUsername: "not_a_user",
			jwtClaims: gitlabjwt.Claims{
				FedUserName: "gitlab",
				AuthToken:   "t0k3n",
			},
			assertError: tst.AssertNoError,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			rs, err := NewRunAs(tt.auth)
			assert.NoError(t, err)

			got, err := rs.Execute(tt.jwtClaims, tt.curUsername, sysLog)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRunAsOverride != nil {
				tt.assertRunAsOverride(t, got)
			}
		})
	}
}

func Test_finalizeOverride(t *testing.T) {
	type args struct {
		initializer RunAsInit
		over        RunAsOverride
	}
	tests := map[string]struct {
		args    args
		want    RunAsOverride
		wantErr bool
	}{
		"override username present": {
			args: args{
				over: RunAsOverride{
					Username: "good_user",
				},
			},
			want: RunAsOverride{
				Username: "good_user",
			},
		},
		"invalid username provided in override": {
			args: args{
				over: RunAsOverride{
					Username: "$(user)",
				},
			},
			wantErr: true,
		},
		"no override, use current user": {
			args: args{
				initializer: RunAsInit{
					CurrentUser: "current",
					JobJWT: gitlabjwt.Claims{
						UserLogin: "login",
					},
				},
			},
			want: RunAsOverride{
				Username:      "current",
				GitLabAccount: "login",
			},
		},
		"gitlab_account override provided": {
			args: args{
				initializer: RunAsInit{
					CurrentUser: "current",
					JobJWT: gitlabjwt.Claims{
						UserLogin: "login",
					},
				},
				over: RunAsOverride{
					GitLabAccount: "over",
				},
			},
			want: RunAsOverride{
				Username:      "current",
				GitLabAccount: "over",
			},
		},
		"valid data_dir override provided": {
			args: args{
				initializer: RunAsInit{
					CurrentUser: "current",
					JobJWT: gitlabjwt.Claims{
						UserLogin: "login",
					},
				},
				over: RunAsOverride{
					DataDir: "/example/dir",
				},
			},
			want: RunAsOverride{
				Username:      "current",
				GitLabAccount: "login",
				DataDir:       "/example/dir",
			},
		},
		"invalid data_dir override provided": {
			args: args{
				initializer: RunAsInit{
					CurrentUser: "current",
					JobJWT: gitlabjwt.Claims{
						UserLogin: "login",
					},
				},
				over: RunAsOverride{
					DataDir: "\\invalid",
				},
			},
			wantErr: true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := finalizeOverride(tt.args.initializer, tt.args.over)

			if (err != nil) != tt.wantErr {
				t.Errorf("finalizeOverride() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("finalizeOverride() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func init() {
	testJWT = gitlabjwt.Claims{
		JobID:       "jobID",
		NamespaceID: "namespaceID",
		PipelineID:  "pipelineID",
		ProjectID:   "projectID",
		ProjectPath: "project/path",
		UserEmail:   "user@example.com",
		UserID:      "userID",
		UserLogin:   "user",
	}
}
