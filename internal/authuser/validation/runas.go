package validation

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"reflect"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/rules"
)

type runAsCfg struct {
	file   string
	sha256 string
	// targetUser is a user proposed login (e.g. service account)
	targetUser string
	federated  bool
}

type runAsScript struct {
	runAsCfg
}

// RunAsInit values are used to established user context for the upcoming authorization.
type RunAsInit struct {
	// TargetUser is a user proposed account (via the CI environment) meant to be the
	// replaced for the CurrentUser if the process is approved.
	TargetUser string `env:"RUNAS_TARGET_USER"`
	// CurrentUser is the currently identified local user account of the CI trigger user.
	// This can differ from the JWT's UserLogin depending on configuration of the authorization,
	// and should be observed when attempting to approve a local user account.
	CurrentUser string `env:"RUNAS_CURRENT_USER"`
	// AuthToken the IdP supplied token during a federated workflow.
	AuthToken string `env:"FEDERATED_AUTH_TOKEN"`
	// FedUsername the IdP supplied username during a federated workflow.
	FedUsername string `env:"FEDERATED_USERNAME"`
	JobJWT      gitlabjwt.Claims
}

func (rs runAsScript) Execute(
	pay gitlabjwt.Claims,
	username string,
	sysLog *logrus.Entry,
) (over RunAsOverride, err error) {
	initializer, err := rs.runAsCfg.runAsPrep(pay, username)
	if err != nil {
		return over, err
	}

	ok, output := invokeRunAsScript(rs.file, initializer)
	sysLog.Debug(fmt.Sprintf("RunAs script raw output: %s", output))
	if !ok {
		return over, fmt.Errorf("runas validation script failed: %s", output)
	}

	err = parseScriptStdout(output, &over)
	if err != nil {
		return over, err
	}

	return finalizeOverride(initializer, over)
}

func finalizeOverride(initializer RunAsInit, over RunAsOverride) (RunAsOverride, error) {
	if over.Username == "" {
		if initializer.TargetUser != "" {
			over.Username = initializer.TargetUser
		} else {
			over.Username = initializer.CurrentUser
		}
	}

	if over.GitLabAccount == "" {
		over.GitLabAccount = initializer.JobJWT.UserLogin
	}

	if err := over.validator(); err != nil {
		return RunAsOverride{}, err
	}

	return over, nil
}

func (rc runAsCfg) runAsPrep(pay gitlabjwt.Claims, username string) (RunAsInit, error) {
	if username == "" {
		return RunAsInit{},
			errors.New("unexpected system error encountered, no current user can be identified")
	}

	err := fileStatus(rc.file, rc.sha256)
	return rc.create(pay, username), err
}

func (rc runAsCfg) create(pay gitlabjwt.Claims, username string) RunAsInit {
	ra := RunAsInit{
		TargetUser:  rc.targetUser,
		CurrentUser: username,
		JobJWT:      pay,
	}

	if rc.federated {
		// Only need to provide if federation is enabled/expected.
		ra.AuthToken = pay.AuthToken
		ra.FedUsername = pay.FedUserName
		if ra.FedUsername != "" {
			// If a federated username is provided it should be assumed
			// this is the current username (e.g. overwrite user login).
			ra.CurrentUser = ra.FedUsername
		}
	}

	return ra
}

// getRunAsUser attempts to find a user provided name in the custom executor
// provided environment. Nonexistent key/values will not results in an error.
func getRunAsUser(key string) (username string, err error) {
	if key != "" {
		username = strings.TrimSpace(os.Getenv(key))
	}

	v := validator.New()
	_ = v.RegisterValidation("username", rules.CheckUsername)
	err = v.Var(username, "username")

	return
}

// runasEnv builds environment to be append to the command for RunAs.
func runasEnv(initializer RunAsInit) []string {
	return cmdEnv(
		reflect.TypeOf(initializer),
		reflect.ValueOf(&initializer).Elem(),
	)
}

// invokeRunAsScript executes the provided script passing it the verifier and username
// as arguments ($ script verifier username), returning if true (exit status = 0)
// and  false (!=0) along with any stdout.
func invokeRunAsScript(script string, initializer RunAsInit) (bool, []byte) {
	var cmd *exec.Cmd
	/* #nosec */
	// launching subprocess with variables required
	if initializer.TargetUser != "" {
		cmd = exec.Command(script, initializer.TargetUser, initializer.CurrentUser)
	} else {
		cmd = exec.Command(script, initializer.CurrentUser)
	}

	// We want to avoid passing potential tokens to more process than necessary.
	tarEnv := runasEnv(initializer)
	for _, val := range os.Environ() {
		if !envparser.SupportedPrefix(val) {
			tarEnv = append(tarEnv, val)
		}
	}
	cmd.Env = tarEnv

	return runCommand(cmd)
}

// validator is used to ensure that all payload values confirm to Jacamar expectations.
func (ro RunAsOverride) validator() error {
	v := validator.New()
	_ = v.RegisterValidation("username", rules.CheckUsername)

	// We only need to validate if a DataDir override is proposed.
	if ro.DataDir != "" {
		_ = v.RegisterValidation("directory", rules.CheckDirectory)
	} else {
		_ = v.RegisterValidation("directory", func(fl validator.FieldLevel) bool {
			return true
		})
	}

	return v.Struct(ro)
}
