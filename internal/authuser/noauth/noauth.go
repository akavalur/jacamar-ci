package noauth

import (
	"fmt"
	"os/user"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/datadir"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
)

var curUsr func() (*user.User, error)

type targetCtx struct {
	usrCtx authuser.UserContext
}

func (t targetCtx) Username() string {
	return t.usrCtx.Username
}

func (t targetCtx) HomeDir() string {
	return t.usrCtx.HomeDir
}

func (t targetCtx) IntegerID() (uid int, gid int) {
	return t.usrCtx.UID, t.usrCtx.GID
}

func (t targetCtx) Groups() []uint32 {
	// These are only required to support downscoping and cannot be supported in no-auth.
	return []uint32{}
}

func (t targetCtx) BaseDir() string {
	return t.usrCtx.BaseDir
}

func (t targetCtx) BuildsDir() string {
	return t.usrCtx.BuildsDir
}

func (t targetCtx) CacheDir() string {
	return t.usrCtx.CacheDir
}

func (t targetCtx) ScriptDir() string {
	return t.usrCtx.ScriptDir
}

func (t targetCtx) BuildState() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		Username:    t.usrCtx.Username,
		BaseDir:     t.usrCtx.BaseDir,
		BuildsDir:   t.usrCtx.BuildsDir,
		CacheDir:    t.usrCtx.CacheDir,
		ScriptDir:   t.usrCtx.ScriptDir,
		ProjectPath: t.usrCtx.JWT.ProjectPath,
	}
}

func (t targetCtx) PrepareNotification() string {
	return fmt.Sprintf(
		"Running as %s UID: %d GID: %d\n",
		t.usrCtx.Username,
		t.usrCtx.UID,
		t.usrCtx.GID,
	)
}

func (t targetCtx) GitLabAccount() string {
	return t.usrCtx.GitlabAccount
}

func (t targetCtx) JobJWT() gitlabjwt.Claims {
	return t.usrCtx.JWT
}

// Factory defines requirements and interface for the entire no-authorization workflow. All values
// must be defined before attempting use.
type Factory struct {
	Env   envparser.ExecutorEnv
	Opt   configure.Options
	Valid authuser.Validators
	usr   *user.User
}

// EstablishUser generates a valid Authorized interface for us in a no-auth workflow either using the current
// user's context or established stateful variables. Once the process is completed the user context is
// immutable and should only be accessed through the provided interface.
func (f Factory) EstablishUser() (authuser.Authorized, error) {
	u := &authuser.UserContext{}

	// Without authorization the current user is always used.
	usr, err := curUsr()
	if err != nil {
		return nil, err
	}
	f.usr = usr

	if f.Env.StatefulEnv.Username != "" {
		if err = u.ProcessFromState(usr, f.Env.StatefulEnv); err != nil {
			return nil, err
		}
	} else {
		if err = f.currentUser(u); err != nil {
			return nil, err
		}
	}

	return targetCtx{
		usrCtx: *u,
	}, nil
}

func (f Factory) currentUser(u *authuser.UserContext) error {
	// No-auth will only validate the JWT during initial user identification (config_exec).
	if err := u.VerifySetJWT(f.Valid.JobJWT); err != nil {
		return fmt.Errorf("unable to parse supplied CI_JOB_JWT: %w", err)
	}

	u.Username = f.usr.Username
	u.HomeDir = f.usr.HomeDir

	if err := u.SetIntIDs(f.usr.Uid, f.usr.Gid); err != nil {
		return err
	}

	if err := datadir.IdentifyDirectories(u, f.Opt, f.Env.RequiredEnv); err != nil {
		return fmt.Errorf("unable to identify target directories: %w", err)
	}

	return nil
}

func init() {
	curUsr = authuser.CurrentUser
}
