// Package authuser (authorize user) maintains the interface for the complete authorization
// flow, leveraging  the job's configuration as well as context provided by the custom executor
// to ensure a fully authorized user is identified for the CI job.
package authuser

import (
	"fmt"
	"os/user"
	"strconv"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
)

// Authorized implements an interface allowing read-only access to the established
// user context. All information is identified during a job's configuration stage
// and remains consistent throughout the life of the job.
type Authorized interface {
	// Username for the validated user.
	Username() string
	// HomeDir for the validate user.
	HomeDir() string
	// IntegerID returns the valid user's UID, GID as integer values.
	IntegerID() (uid int, gid int)
	// Groups returns users group ids.
	Groups() []uint32
	// BaseDir directory for CI job and command interactions.
	BaseDir() string
	// BuildsDir is the working directory created on the local file system.
	BuildsDir() string
	// CacheDir is the working directory created on the local file system.
	CacheDir() string
	// ScriptDir is the directory for script storage and command execution.
	ScriptDir() string
	// BuildState populates and return the StatefulEnv.
	BuildState() envparser.StatefulEnv
	// PrepareNotification returns details on the current job's validated user to
	// used in the prepare message.
	PrepareNotification() string
	// GitLabAccount returns the user's target GitLab account for usage during the
	// config_exec stage only! This account name may be a service account based
	// upon any override provided through the RunAs functionality.
	GitLabAccount() string
	// JobJWT returns a subset of the claims from a validated JSON web token
	// associated with the current CI job. This is only present when authorization
	// has not been disabled.
	JobJWT() gitlabjwt.Claims
}

// UserContext contains validated user details for the current CI job.
type UserContext struct {
	Username        string
	HomeDir         string
	UID             int
	GID             int
	Groups          []uint32
	BaseDir         string // Base directory for CI job and command interactions.
	BuildsDir       string // Working directory where the job will be created.
	CacheDir        string // Directory where local cache will be stored.
	ScriptDir       string // Location for script generation and command execution.
	GitlabAccount   string // Target GitLab account for usage with service user + broker
	DataDirOverride string // Proposed override for configured data_dir
	JWT             gitlabjwt.Claims
}

// CurrentUser returns the current user.
func CurrentUser() (*user.User, error) {
	return user.Current()
}

// ProcessFromState update basic UserContext using the supplied user object in conjunction
// with Stateful environment variables identified.
func (u *UserContext) ProcessFromState(usr *user.User, s envparser.StatefulEnv) error {
	u.HomeDir = usr.HomeDir
	u.Username = s.Username
	u.BaseDir = s.BaseDir
	u.BuildsDir = s.BuildsDir
	u.CacheDir = s.CacheDir
	u.ScriptDir = s.ScriptDir

	if err := u.SetIntIDs(usr.Uid, usr.Gid); err != nil {
		return err
	}

	return nil
}

// SetIntIDs converts the supplied uid & gid to integers and updates the UserContext.
func (u *UserContext) SetIntIDs(uid, gid string) error {
	iUid, err := strconv.Atoi(uid)
	if err != nil {
		return fmt.Errorf("unable to convert valid user's UID (%s)", uid)
	}
	iGid, err := strconv.Atoi(gid)
	if err != nil {
		return fmt.Errorf("unable to convert valid user's GID (%s)", gid)
	}

	u.UID = iUid
	u.GID = iGid

	return nil
}

// VerifySetJWT using the supplied validator, check to ensure the JWT is signed and contains
// an expected payload. Update the UserContext accordingly.
func (u *UserContext) VerifySetJWT(jobJWT gitlabjwt.Validator) error {
	jwtClaims, err := jobJWT.Parse()
	if err != nil {
		return fmt.Errorf("unable to parse supplied CI_JOB_JWT: %w", err)
	}
	u.JWT = jwtClaims

	return nil
}

type Validators struct {
	RunAs  validation.RunAsValidator
	JobJWT gitlabjwt.Validator
}

// EstablishValidators create a series of Validators based upon configuration and identifiable job context.
func EstablishValidators(opt configure.Options, env envparser.ExecutorEnv) (Validators, error) {
	rs, err := validation.NewRunAs(opt.Auth)
	if err != nil {
		return Validators{}, fmt.Errorf("failed to establish RunAs validator: %w", err)
	}

	return Validators{
		RunAs: rs,
		JobJWT: gitlabjwt.Factory(
			env.RequiredEnv.CIJobJWT,
			env.RequiredEnv.JobID,
			env.RequiredEnv.ServerURL,
		),
	}, nil
}
