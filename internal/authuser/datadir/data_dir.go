package datadir

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
)

// IdentifyDirectories updates the appropriate directories in the UserContext (BaseDir,
// BuildsDir, CacheDir), returning an error if encountered in the process. A fully
// validated user's context is required before invoking this method. No directories
// are created as part of this process and variables (with the exclusion of $HOME)
// will not be resolved.
func IdentifyDirectories(u *authuser.UserContext, opt configure.Options, req envparser.RequiredEnv) error {
	if u.HomeDir == "" || u.Username == "" {
		return fmt.Errorf(
			"incomplete user context detected, aborting build directory identification",
		)
	}

	dataDir := opt.General.DataDir
	if u.DataDirOverride != "" {
		// Observe override value if supplied by admin earlier in process.
		dataDir = u.DataDirOverride
	}

	if dataDir == "$HOME" {
		u.BaseDir = u.HomeDir + "/.jacamar-ci"
	} else if dataDir != "" {
		if err := variableDirRules(opt, dataDir); err != nil {
			return err
		}

		u.BaseDir = filepath.Clean(strings.TrimSpace(dataDir) + "/" + u.Username)
	} else {
		// Missing upstream context for the defined (config.toml) build/cache directory.
		return fmt.Errorf(
			"undefined 'data_dir' unsupported, please updated your configuration",
		)
	}

	u.BuildsDir = buildDir(u.BaseDir, req)
	u.CacheDir = cacheDir(u.BaseDir)
	u.ScriptDir = scriptDir(u.BaseDir, req, u.JWT)

	if opt.General.CustomBuildDir {
		// Root directory creation + custom directory not supported.
		if opt.Auth.RootDirCreation {
			return errors.New("unsupported configuration, root_dir_creation cannot be used with custom builds")
		}
		customBuildsDir(u, req)
	}

	return nil
}

// buildDir returns target for CI job's DefaultBuildDir:
// <base_dir>/builds/<runner-short>/<concurrent-id>.
func buildDir(base string, req envparser.RequiredEnv) string {
	return strings.Join([]string{base,
		"/builds/", req.RunnerShort,
		"/", zeroPadConcurrent(req),
	}, "")
}

// cacheDir returns target for CI job's DefaultCacheDir:
// <base_dir>/cache.
func cacheDir(base string) string {
	return strings.Join([]string{base, "/cache"}, "")
}

// scriptDir returns target for CI job's DefaultCacheDir:
// <base_dir>/scripts/<project-path>/<CI_JOB_ID>.
func scriptDir(base string, req envparser.RequiredEnv, jwtClaims gitlabjwt.Claims) string {
	return strings.Join([]string{base,
		"/scripts/", req.RunnerShort,
		"/", zeroPadConcurrent(req),
		"/", jwtClaims.ProjectPath,
		"/", req.JobID,
	}, "")
}

// customBuildsDir updates the BuildsDir if appropriate environmental variable identified.
func customBuildsDir(u *authuser.UserContext, req envparser.RequiredEnv) {
	dir, ok := os.LookupEnv(envparser.UserEnvPrefix + "CUSTOM_CI_BUILDS_DIR")
	if !ok {
		return
	}
	base := filepath.Clean(strings.TrimSpace(dir))
	u.BuildsDir = buildDir(strings.Join([]string{base, "/", u.Username}, ""), req)
}

func zeroPadConcurrent(req envparser.RequiredEnv) string {
	cID := req.ConcurrentID
	for len(cID) < 3 {
		cID = "0" + cID
	}
	return cID
}

func variableDirRules(opt configure.Options, dataDir string) error {
	if strings.Contains(dataDir, "$") {
		if opt.Auth.RootDirCreation {
			return errors.New(
				"unsupported configuration, root_dir_creation cannot be used with variable builds",
			)
		}
		if !opt.General.VariableDataDir {
			return errors.New(
				"unsupported configuration, variable_data_dir must be enabled for variable builds",
			)
		}
	}

	return nil
}
