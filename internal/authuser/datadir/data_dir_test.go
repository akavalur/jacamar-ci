package datadir

import (
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type dirTest struct {
	buildEnv string
	cfg      configure.Options
	req      envparser.RequiredEnv
	user     *authuser.UserContext
	claims   gitlabjwt.Claims

	assertError       func(*testing.T, error)
	assertUserContext func(*testing.T, *authuser.UserContext)
}

var testRedEnv envparser.RequiredEnv
var testRedClaims gitlabjwt.Claims

func init() {
	testRedEnv = envparser.RequiredEnv{
		ConcurrentID: "0",
		JobID:        "123",
		RunnerShort:  "short",
	}
	testRedClaims = gitlabjwt.Claims{
		ProjectPath: "group/project",
		UserLogin:   "tester",
	}
}

func TestUserContext_identifyDirectories(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testContext := &authuser.UserContext{
		Username: "user",
		HomeDir:  "/home/user",
	}

	tests := map[string]dirTest{
		"undefined username in UserContext": {
			user: &authuser.UserContext{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"incomplete user context detected, aborting build directory identification",
				)
			},
		},
		"undefined homedir in UserContext": {
			user: &authuser.UserContext{
				Username: "user",
				HomeDir:  "",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"incomplete user context detected, aborting build directory identification",
				)
			},
		},
		"DataDir defined for user's home, no custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "$HOME",
				},
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.jacamar-ci", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/cache", u.CacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"DataDir defined for user's home, custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			buildEnv: "/custom/dir",
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/custom/dir/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
			},
		},
		"DataDir defined for target directory, no custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "/ci",
				},
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/ci/user", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/ci/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/ci/user/cache", u.CacheDir, "UserContext - cacheDir")
				assert.Equal(
					t, "/ci/user/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"DataDir defined for target directory, custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "/ci",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			buildEnv: "/custom/dir",
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/custom/dir/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir")
			},
		},
		"DataDir not defined in configuration": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{},
				Auth:    configure.Auth{},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"undefined 'data_dir' unsupported, please updated your configuration",
				)
			},
		},
		"Custom build directory enabled but not used": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "/ci",
					CustomBuildDir: true,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/ci/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
			},
		},
		"Shared cache directory enabled and dataDir correctly defined": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "/ci",
				},
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/ci/user", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/ci/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/ci/user/cache", u.CacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/ci/user/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"variable data directory found": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:         "$TEST/something",
					VariableDataDir: true,
				},
				Auth: configure.Auth{},
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "$TEST/something/user", u.BaseDir,
					"UserContext - baseDir",
				)
			},
			assertError: tst.AssertNoError,
		},
		"variable data directory found with root creation": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "$TEST/something",
				},
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unsupported configuration, root_dir_creation cannot be used with variable builds")
			},
		},
		"variable data directory found without enabling": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "$TEST/something",
				},
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unsupported configuration, variable_data_dir must be enabled for variable builds")
			},
		},
		"custom build directory with root creation": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "/ci",
					CustomBuildDir: true,
				},
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unsupported configuration, root_dir_creation cannot be used with custom builds")
			},
		},
		"DataDir overridden in RunAs, no custom builds": {
			user: &authuser.UserContext{
				Username:        "user",
				HomeDir:         "/home/user",
				DataDirOverride: "/runas",
			},
			cfg: configure.Options{
				General: configure.General{
					DataDir: "$HOME",
				},
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/runas/user", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/runas/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/runas/user/cache", u.CacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/runas/user/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.buildEnv != "" {
				_ = os.Setenv("CUSTOM_ENV_CUSTOM_CI_BUILDS_DIR", tt.buildEnv)
				defer os.Unsetenv("CUSTOM_ENV_CUSTOM_CI_BUILDS_DIR")
			}

			tt.user.JWT = tt.claims

			err := IdentifyDirectories(tt.user, tt.cfg, tt.req)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertUserContext != nil {
				tt.assertUserContext(t, tt.user)
			}
		})
	}
}
