package fullauth

import (
	"fmt"
	"os/user"
	"strconv"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/datadir"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
)

// processFlow manages all aspects of the user authorization flow, leveraging the supplied
// configuration as well as context from the custom executor. The process supporting this will
// ensure the entire authorization flow is strictly observed, as  such it is recommended to
// leverage stateful variables to store key aspects of the user context after a successful
// authorization. Any issues encountered during this process will results in an error returned.
// Though steps are taken to return a safe error externally defined scripts/syscall are made
// that may return more information than required. It is strongly advised that the error message
// presented to a CI user only inform them of the general failures and more details are logged
// by the privileged user.
func (f Factory) processFlow(u *authuser.UserContext) error {
	err := f.validateUser(u)
	if err != nil {
		return fmt.Errorf("invalid authorization target user: %w", err)
	}

	// Root can be a valid GitLab account and should not be allowed to run while auth is
	// enabled and a non standard downscope has been identified we enforce not using it
	// of the current service account.
	if err = checkUserContext(u, f.Opt.Auth); err != nil {
		return err
	}

	if err = datadir.IdentifyDirectories(u, f.Opt, f.Env.RequiredEnv); err != nil {
		return fmt.Errorf("unable to identify target directories: %w", err)
	}

	return nil
}

// processFromState updates a UserContext by leveraging the stateful variables previously established
// and provided to subsequent stages by the custom executor. No auth data is reconfirmed as part of this process.
// Errors can occur during user/group lookup.
func (f Factory) processFromState(u *authuser.UserContext) error {
	usr, err := user.Lookup(f.Env.StatefulEnv.Username)
	if err != nil {
		return err
	}

	if err = u.ProcessFromState(usr, f.Env.StatefulEnv); err != nil {
		return err
	}

	// Groups can only be established with authorization binary.
	groups, err := groupIDs(usr)
	if err != nil {
		return err
	}
	u.Groups = groups

	return nil
}

func groupIDs(usr *user.User) (groups []uint32, err error) {
	groupids, err := usr.GroupIds()
	if err != nil {
		return
	}

	for _, sGID := range groupids {
		var iGID int
		iGID, err = strconv.Atoi(sGID)
		if err != nil {
			return
		}
		groups = append(groups, uint32(iGID))
	}

	return groups, err
}

func (f Factory) validateUser(u *authuser.UserContext) error {
	// Establishing starting/known values. These can change over the course of authorization!
	u.Username = u.JWT.UserLogin
	u.GitlabAccount = u.JWT.UserLogin

	if f.Opt.Auth.ListsPreValidation {
		if err := preUserCheckList(u, f.Opt.Auth); err != nil {
			return err
		}
	}

	if f.Valid.RunAs != nil {
		if err := f.processRunAs(u); err != nil {
			return fmt.Errorf("RunAs validation: %w", err)
		}
	}

	// Check defined allowlist or blocklist (final uid before job execution).
	if err := preUserCheckList(u, f.Opt.Auth); err != nil {
		return err
	}

	return establishGroups(u)
}

func (f Factory) processRunAs(u *authuser.UserContext) error {
	override, err := f.Valid.RunAs.Execute(u.JWT, u.Username, f.SysLog)
	if err != nil {
		return err
	}

	f.SysLog.Info(fmt.Sprintf("RunAs (%s) parsed successfully", f.Opt.Auth.RunAs.ValidationScript))
	f.SysLog.Debug(fmt.Sprintf("RunAs potential override values: %+v", override))

	u.Username = override.Username
	u.GitlabAccount = override.GitLabAccount
	u.DataDirOverride = override.DataDir

	return nil
}

func preUserCheckList(u *authuser.UserContext, auth configure.Auth) error {
	target, err := user.Lookup(u.Username)
	if err != nil {
		return fmt.Errorf("failed user.Lookup, verify system expectations: %w", err)
	}

	return checkLists(u, target, auth)
}

// checkLists verifies all allowlist/blocklist logic is observed. It in turn ensures
// aspects that the UserContext has been updated (username, uid, gid, and homedir only).
func checkLists(u *authuser.UserContext, target *user.User, auth configure.Auth) error {
	groups, err := lookupGroups(target, auth)
	if err != nil {
		return err
	}

	if err = validateShell(auth.ShellAllowlist, target.Uid, target.Username); err != nil {
		return err
	}

	if err = validateABLists(auth, target, groups); err != nil {
		return err
	}

	if err = u.SetIntIDs(target.Uid, target.Gid); err != nil {
		return err
	}
	u.HomeDir = target.HomeDir

	return nil
}

// establishGroups within the validation works uses the authorized target user to create
// a list of groups they belong to and update the UserContext accordingly.
func establishGroups(u *authuser.UserContext) error {
	// Need to lookup the currently identified username.
	usr, err := user.Lookup(u.Username)
	if err != nil {
		return fmt.Errorf("failed user.Lookup, verify system expectations: %w", err)
	}

	groups, err := groupIDs(usr)
	if err != nil {
		return err
	}
	u.Groups = groups

	return nil
}

// lookupGroups checks the local system for the declared user and group memberships.
func lookupGroups(target *user.User, auth configure.Auth) (groups []string, err error) {
	if len(auth.GroupAllowlist) > 0 || len(auth.GroupBlocklist) > 0 {
		groupids, err := target.GroupIds()
		if err != nil {
			return []string{}, fmt.Errorf(
				"unable to identify groups (usr.GroupIds) that user %s is a member of",
				target.Username,
			)
		}
		for _, gid := range groupids {
			groupptr, err := user.LookupGroupId(gid)
			if err != nil {
				return []string{}, fmt.Errorf("unable to lookup group id %s: %w", gid, err)
			}
			group := *groupptr
			groups = append(groups, group.Name)
		}
	}

	return groups, nil
}

func checkUserContext(u *authuser.UserContext, auth configure.Auth) error {
	if auth.Downscope != "" {
		usr, err := curUsr()
		if err != nil {
			return fmt.Errorf("unable to identify service user: %w", err)
		}

		// We only need to enforce limitations on the user when a supported
		// downscoping mechanism is configured.
		if (u.Username == "root" || u.UID == 0 || u.GID == 0) ||
			(u.Username == usr.Username && auth.Downscope != "none") {
			return fmt.Errorf("cannot execute as %s while authorization is enforced", u.Username)
		}
	}

	return nil
}
