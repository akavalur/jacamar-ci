package fullauth

import (
	"fmt"
	"os/user"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/pwshell"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
)

// validateShell returns an error if the user's shell is not
// found in the admin defined allowlist. If the list is empty no error is returned.
func validateShell(shells []string, uid, username string) error {
	if len(shells) != 0 {
		userShell, err := pwshell.FetchShell(uid)
		if err != nil {
			return fmt.Errorf("unable to obtain user defined shell: %w", err)
		}

		for _, val := range shells {
			if val == userShell {
				return nil
			}
		}
		return fmt.Errorf("invalid shell (%s) detected user %s", userShell, username)
	}

	return nil
}

// validateABLists manage the allowlist/blocklist implementation. All lists are established in
// the job configuration:
// 	- user_allowlist: An authoritative list of users who can execute CI jobs.
// 	- user_blocklist: A list of usernames that are not allowed to run CI jobs. More authoritative
// 	  than group lists, but can be overridden by user allowlist.
// 	- groups_allowlist: A list of groups that are allowed to run CI jobs. Least authoritative
// 	- groups_blocklist: A list of groups that are not allowed to run CI jobs.
// If any defined list would prevent the target user from executing a job, an error will be
// returned.
func validateABLists(auth configure.Auth, target *user.User, userGroups []string) error {
	if contains(auth.UserAllowlist, target.Username) {
		return nil
	} else if contains(auth.UserBlocklist, target.Username) {
		return fmt.Errorf(
			"%s, is not in the user allowlist and is in the user blocklist",
			target.Username,
		)
	} else if checkGroupBlocklist(auth.GroupBlocklist, userGroups) {
		return fmt.Errorf(
			"%s, is not on the user allowlist and is in the group blocklist",
			target.Username,
		)
	} else if checkGroupAllowlist(auth.GroupAllowlist, userGroups) {
		return nil
	}

	return fmt.Errorf(
		"a group allowlist exists, but %s is not a member of any defined group",
		target.Username,
	)
}

// checkGroupAllowlist checks if the user is in any group that is a member.
func checkGroupAllowlist(groupAllowlist, userGroups []string) bool {
	if len(groupAllowlist) == 0 {
		return true
	}

	for _, userGroup := range userGroups {
		if contains(groupAllowlist, userGroup) {
			return true
		}
	}
	return false
}

// checkGroupBlocklist checks if the user is any group that is a member,
// return all shared groups.
func checkGroupBlocklist(groupBlocklist, userGroups []string) bool {
	if len(groupBlocklist) == 0 {
		return false
	}

	var sharedGroups []string
	for _, userGroup := range userGroups {
		if contains(groupBlocklist, userGroup) {
			sharedGroups = append(sharedGroups, userGroup)
		}
	}
	return len(sharedGroups) > 0
}

// check if an item exists in a list of items, return true if it does.
func contains(list []string, item string) bool {
	for _, x := range list {
		if x == item {
			return true
		}
	}
	return false
}
