package fullauth

import (
	"errors"
	"os/user"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func Test_groupIDs(t *testing.T) {
	badUser := &user.User{}
	usr, _ := user.Current()

	tests := map[string]struct {
		usr         *user.User
		assertError func(*testing.T, error)
	}{
		"invalid user provided": {
			usr: badUser,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, `user: list groups for : invalid gid ""`)
			},
		},
		"current user (correct)": {
			usr: usr,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			_, err := groupIDs(tt.usr)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_checkUserContext(t *testing.T) {
	tester := func() (*user.User, error) {
		return &user.User{
			Username: "tester",
		}, nil
	}

	tests := map[string]authTests{
		"no downscoping": {
			assertError: tst.AssertNoError,
		},
		"error encountered during user lookup": {
			auth: configure.Auth{
				Downscope: "setuid",
			},
			mockCurUsr: func() (*user.User, error) {
				return nil, errors.New("error message")
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to identify service user: error message")
			},
		},
		"same username identified": {
			auth: configure.Auth{
				Downscope: "setuid",
			},
			u: &authuser.UserContext{
				Username: "tester",
				UID:      1000,
				GID:      1000,
			},
			mockCurUsr: tester,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "cannot execute as tester while authorization is enforced")
			},
		},
		"root username identified": {
			auth: configure.Auth{
				Downscope: "setuid",
			},
			u: &authuser.UserContext{
				Username: "root",
			},
			mockCurUsr: curUsr,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "cannot execute as root while authorization is enforced")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockCurUsr != nil {
				curUsr = tt.mockCurUsr
			} else {
				curUsr = authuser.CurrentUser
			}

			err := checkUserContext(tt.u, tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestUserContext_checkLists(t *testing.T) {
	opt := buildMockFullOpt()

	t.Run("list error encountered", func(t *testing.T) {
		u := &authuser.UserContext{
			Username: currentUsr.Username,
		}

		err := checkLists(u, currentUsr, opt.Auth)
		assert.Error(t, err)
	})

	t.Run("list error encountered", func(t *testing.T) {
		if runtime.GOOS != "linux" {
			t.Skip()
		}

		u := &authuser.UserContext{
			Username: currentUsr.Username,
		}
		opt.Auth.ShellAllowlist = []string{"/example/shell"}

		err := checkLists(u, currentUsr, opt.Auth)
		assert.Error(t, err)
	})
}
