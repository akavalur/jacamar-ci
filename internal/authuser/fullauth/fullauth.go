package fullauth

import (
	"fmt"
	"os/user"

	"github.com/sirupsen/logrus"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
)

type targetCtx struct {
	usrCtx authuser.UserContext
}

func (t targetCtx) Username() string {
	return t.usrCtx.Username
}

func (t targetCtx) HomeDir() string {
	return t.usrCtx.HomeDir
}

func (t targetCtx) IntegerID() (uid int, gid int) {
	return t.usrCtx.UID, t.usrCtx.GID
}

func (t targetCtx) Groups() []uint32 {
	return t.usrCtx.Groups
}

func (t targetCtx) BaseDir() string {
	return t.usrCtx.BaseDir
}

func (t targetCtx) BuildsDir() string {
	return t.usrCtx.BuildsDir
}

func (t targetCtx) CacheDir() string {
	return t.usrCtx.CacheDir
}

func (t targetCtx) ScriptDir() string {
	return t.usrCtx.ScriptDir
}

func (t targetCtx) BuildState() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		Username:    t.usrCtx.Username,
		BaseDir:     t.usrCtx.BaseDir,
		BuildsDir:   t.usrCtx.BuildsDir,
		CacheDir:    t.usrCtx.CacheDir,
		ScriptDir:   t.usrCtx.ScriptDir,
		ProjectPath: t.usrCtx.JWT.ProjectPath,
	}
}

func (t targetCtx) PrepareNotification() string {
	return fmt.Sprintf(
		"Running as %s UID: %d GID: %d\n",
		t.usrCtx.Username,
		t.usrCtx.UID,
		t.usrCtx.GID,
	)
}

func (t targetCtx) GitLabAccount() string {
	return t.usrCtx.GitlabAccount
}

func (t targetCtx) JobJWT() gitlabjwt.Claims {
	return t.usrCtx.JWT
}

// Factory defines requirements and interface for the entire full authorization workflow. All values
// must be defined before attempting to use.
type Factory struct {
	SysLog *logrus.Entry
	Env    envparser.ExecutorEnv
	Opt    configure.Options
	Valid  authuser.Validators
}

// EstablishUser processes and identifies a local user for the GitLab CI job. The current environment and
// configuration are used to dictate how the user authorization process will be handled. Once the process
// is completed the user context is immutable and should only be accessed through the provided interface.
func (f Factory) EstablishUser() (authuser.Authorized, error) {
	u := &authuser.UserContext{}

	if err := u.VerifySetJWT(f.Valid.JobJWT); err != nil {
		return nil, fmt.Errorf("unable to parse supplied CI_JOB_JWT: %w", err)
	}
	f.SysLog.Info("JWT Verified")

	if f.Env.StatefulEnv.Username != "" {
		if err := f.processFromState(u); err != nil {
			f.SysLog.Error(fmt.Sprintf(
				"failed to process state for %s: %s",
				f.Env.StatefulEnv.Username,
				err.Error(),
			))
			return nil, err
		}
		f.SysLog.Info(fmt.Sprintf("user context %s processed from job state", u.Username))
	} else {
		if err := f.processFlow(u); err != nil {
			f.SysLog.Error(fmt.Sprintf("failed to authorize user for CI job: %s", err.Error()))
			return nil, err
		}
		f.SysLog.Info(fmt.Sprintf("User %s authorized for CI job execution.", u.Username))
	}

	if err := checkUserContext(u, f.Opt.Auth); err != nil {
		return nil, err
	}

	return targetCtx{
		usrCtx: *u,
	}, nil
}

var curUsr func() (*user.User, error)

func init() {
	curUsr = authuser.CurrentUser
}
