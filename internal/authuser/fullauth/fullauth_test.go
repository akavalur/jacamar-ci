package fullauth

import (
	"errors"
	"io/ioutil"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_gitlabjwt"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_validation"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

var (
	currentUsr *user.User
)

type authTests struct {
	auth    configure.Auth
	cfg     configure.Configurer
	env     envparser.ExecutorEnv
	factory Factory
	u       *authuser.UserContext
	valid   authuser.Validators

	mockEstValid func(configure.Configurer, envparser.ExecutorEnv) (authuser.Validators, error)
	mockCurUsr   func() (*user.User, error)

	assertError      func(*testing.T, error)
	assertContext    func(*testing.T, *authuser.UserContext)
	assertAuthorized func(*testing.T, authuser.Authorized)
}

func buildMockFullOpt() configure.Options {
	return configure.Options{
		Auth: configure.Auth{
			UserAllowlist:  []string{"u1", "u4", "u8"},
			UserBlocklist:  []string{"u3", "u4", "u8"},
			GroupAllowlist: []string{"g2", "g5", "g8"},
			GroupBlocklist: []string{"g7", "g8"},
		},
		General: configure.General{
			DataDir: "/ci",
		},
	}
}

func buildMockMinOpt() configure.Options {
	return configure.Options{
		Auth: configure.Auth{
			Downscope: "setuid",
		},
		General: configure.General{
			DataDir: "/ci",
		},
	}
}

func TestFactory_EstablishUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ll := logrus.New()
	ll.Out = ioutil.Discard
	sysLog := logrus.NewEntry(ll)

	jwtError := mock_gitlabjwt.NewMockValidator(ctrl)
	jwtError.EXPECT().Parse().Return(gitlabjwt.Claims{}, errors.New("error message")).AnyTimes()

	jwtSuccess := mock_gitlabjwt.NewMockValidator(ctrl)
	jwtSuccess.EXPECT().Parse().Return(gitlabjwt.Claims{
		JobID:     "123",
		UserLogin: currentUsr.Username,
	}, nil).AnyTimes()

	runasErr := mock_validation.NewMockRunAsValidator(ctrl)
	runasErr.EXPECT().Execute(gomock.Any(), gomock.Any(), gomock.Any()).Return(validation.RunAsOverride{}, errors.New("error message"))

	tests := map[string]authTests{
		"invalid stateful username": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "invalid-user",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "user: unknown user invalid-user")
			},
		},
		"invalid jwt": {
			factory: Factory{
				Valid: authuser.Validators{
					JobJWT: jwtError,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to parse supplied CI_JOB_JWT: unable to parse supplied CI_JOB_JWT: error message")
			},
		},
		"completed process from state, no downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: currentUsr.Username,
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtSuccess,
				},
			},
			assertError: tst.AssertNoError,
		},
		"context failed from state, setuid downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: currentUsr.Username,
					},
				},
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "cannot execute as "+currentUsr.Username)
			},
		},
		"processes failed from jwt, setuid downscope": {
			factory: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "cannot execute as "+currentUsr.Username)
			},
		},
		"processes from jwt, no downscope": {
			factory: Factory{
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtSuccess,
				},
			},
			assertError: tst.AssertNoError,
			assertAuthorized: func(t *testing.T, auth authuser.Authorized) {
				assert.Equal(t, currentUsr.Username, auth.Username())
			},
		},
		"processes from jwt, invalid directory configurations": {
			factory: Factory{
				Opt: configure.Options{
					General: configure.General{
						DataDir:        "/ci",
						CustomBuildDir: true,
					},
					Auth: configure.Auth{
						RootDirCreation: true,
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtSuccess,
				},
			},
			assertError: tst.AssertError,
		},
		"processes from jwt, user block list pre-validation": {
			factory: Factory{
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						ListsPreValidation: true,
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "is not in the user allowlist and is in the user blocklist")
			},
		},
		"processes from jwt, user block list": {
			factory: Factory{
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "is not in the user allowlist and is in the user blocklist")
			},
		},
		"processes from jwt, runas failure": {
			factory: Factory{
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					RunAs:  runasErr,
					JobJWT: jwtSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid authorization target user: RunAs validation: error message")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.factory.EstablishUser()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAuthorized != nil {
				tt.assertAuthorized(t, got)
			}
		})
	}
}

func Test_Authorized_Interface(t *testing.T) {
	working := targetCtx{
		usrCtx: authuser.UserContext{
			Username:      "username",
			HomeDir:       "/home/username",
			UID:           1000,
			GID:           2000,
			Groups:        []uint32{1, 2, 3},
			BaseDir:       "/base",
			BuildsDir:     "/builds",
			CacheDir:      "/cache",
			ScriptDir:     "/script",
			GitlabAccount: "gitlab-user",
			JWT: gitlabjwt.Claims{
				JobID: "123",
			},
		},
	}

	t.Run("verify basic interface functionality implemented", func(tt *testing.T) {
		assert.Equal(tt, "username", working.Username())
		assert.Equal(tt, "/home/username", working.HomeDir())
		uid, gid := working.IntegerID()
		assert.Equal(tt, 1000, uid)
		assert.Equal(tt, 2000, gid)
		assert.Equal(tt, []uint32{1, 2, 3}, working.Groups())
		assert.Equal(tt, "/base", working.BaseDir())
		assert.Equal(tt, "/builds", working.BuildsDir())
		assert.Equal(tt, "/cache", working.CacheDir())
		assert.Equal(tt, "/script", working.ScriptDir())
		assert.Equal(tt, "username", working.BuildState().Username)
		assert.Equal(tt, "Running as username UID: 1000 GID: 2000\n", working.PrepareNotification())
		assert.Equal(tt, "gitlab-user", working.GitLabAccount())
		assert.Equal(tt, gitlabjwt.Claims{
			JobID: "123",
		}, working.JobJWT())
	})
}

func init() {
	currentUsr, _ = user.Current()
}
