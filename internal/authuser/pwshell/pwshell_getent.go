// +build linux,getent

package pwshell

import (
	"os/exec"
	"strings"
)

// FetchShell for Linux with osusergo tag, obtains the user's system defined
// shell using getent command.
func FetchShell(uid string) (string, error) {
	path, err := exec.LookPath("getent")
	if err != nil {
		return "", err
	}

	cmd := exec.Command(path, "passwd", uid)
	out, err := cmd.Output()
	if err != nil {
		return "", err
	}
	s := strings.Split(strings.TrimSuffix(string(out), "\n"), ":")

	return s[6], nil
}
