// +build linux,!getent

package pwshell

/*
   #include <pwd.h>
   #include <stdio.h>
   #include <unistd.h>
*/
import "C"
import (
	"strconv"
)

// FetchShell for Linux with CGO enabled, obtains the user's system defined
// shell via using the C getpwuid(3) function.
func FetchShell(uid string) (string, error) {
	u, err := strconv.Atoi(uid)
	if err != nil {
		return "", err
	}
	s := C.getpwuid(C.__uid_t(u))
	shell := C.GoString(s.pw_shell)
	return shell, nil
}
