package authuser_test

import (
	"os/user"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

func TestUserContext_ProcessFromState(t *testing.T) {
	usr, err := authuser.CurrentUser()
	assert.NoError(t, err, "error identifying current user")

	type args struct {
		usr *user.User
		s   envparser.StatefulEnv
	}
	tests := map[string]struct {
		u                 *authuser.UserContext
		args              args
		assertError       func(*testing.T, error)
		assertUserContext func(*testing.T, *authuser.UserContext)
	}{
		"error encountered setting interface UID": {
			u: &authuser.UserContext{},
			args: args{
				usr: &user.User{
					Uid: "uid",
					Gid: "gid",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"error encountered setting interface GID": {
			u: &authuser.UserContext{},
			args: args{
				usr: &user.User{
					Uid: "1000",
					Gid: "gid",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"update context from established state": {
			u: &authuser.UserContext{},
			args: args{
				usr: usr,
				s: envparser.StatefulEnv{
					Username:  usr.Username,
					BaseDir:   "/base",
					BuildsDir: "/base/builds",
					CacheDir:  "/base/cache",
					ScriptDir: "/base/script",
				},
			},
			assertError: tst.AssertNoError,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.NotNil(t, u)
				assert.Equal(t, usr.Username, u.Username)
				assert.Equal(t, "/base", u.BaseDir)
				assert.Equal(t, usr.Gid, strconv.Itoa(u.GID))
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.u.ProcessFromState(tt.args.usr, tt.args.s)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertUserContext != nil {
				tt.assertUserContext(t, tt.u)
			}
		})
	}
}

func TestEstablishValidators(t *testing.T) {
	// Testing validators is a goal within Pavilion, we should verify basic conditions here.
	t.Run("error encountered creating RunAs validation", func(t *testing.T) {
		runAsErr := configure.Options{
			Auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationPlugin: "test",
				},
			},
		}

		_, err := authuser.EstablishValidators(runAsErr, envparser.ExecutorEnv{})
		assert.Error(t, err)

	})

	t.Run("standard validators, no runas", func(t *testing.T) {
		runAsTst := configure.Options{}
		stateEnv := envparser.ExecutorEnv{
			RequiredEnv: envparser.RequiredEnv{
				JobID:     "1",
				ServerURL: "https://gitlab.example.com/",
				CIJobJWT:  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
			},
		}
		got, err := authuser.EstablishValidators(runAsTst, stateEnv)
		assert.NoError(t, err)
		assert.Nil(t, got.RunAs, "RunAs validator")
		assert.NotNil(t, got.JobJWT, "JWT validator")
	})
}

func Test_CurrentUser(t *testing.T) {
	// At this time the CurrentUser function should simply defer to calling the
	// associated method in the user package. Enforce this until future changes may be made.
	sysUsr, sysErr := user.Current()
	usr, err := authuser.CurrentUser()

	assert.Equal(t, sysUsr, usr)
	assert.Equal(t, sysErr, err)
}
