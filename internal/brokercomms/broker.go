// Package brokercomms maintains functionality required to generate and register an identifier
// with a configured CI token broker instance. There is no validation steps for the CI job
// token or JWT. It is expected that these steps have taken place earlier in the process.
package brokercomms

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/go-playground/validator/v10"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/rules"
)

const (
	brokerRef = "/-/broker/job"
)

// Registerer interface supports registration and identification of broker service interactions.
type Registerer interface {
	// JobBrokerToken returns the unique job identifier that can be provided the CI user and used
	// freely with the broker service instead of a traditional job token.
	JobBrokerToken() string
	// PostJob will register the unique job identifier to the configured broker
	// service via a POST request. Must be manually invoked separate from interface creation.
	PostJob() error
}

// JobData represent the body of a /-/broker/job POST request.
type JobData struct {
	// JobJWT is the GitLab issued CI_JOB_JWT.
	JWT string `json:"ci_job_jwt" validate:"jwt"`
	// Token is the GitLab issued CI_JOB_TOKEN.
	Token string `json:"ci_job_token" validate:"gitlabToken"`
	// ServiceUser is the GitLab username who has been identified by the runner as
	// the target local service (RunAs) user. This account's access credentials
	// are used to validate incoming requests.
	ServiceUser string `json:"service_user" validate:"username"`
}

type jobResponse struct {
	BrokerToken string `json:"broker_token" validate:"sha256"`
}

// HTTPClient implements a client to post a HTTP api endpoint.
type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

var (
	client HTTPClient
)

// NewJob based upon the Jacamar configuration will establish a functional Registerer interface
// by which interactions with the broker can be subsequently established.
func NewJob(cfg configure.Broker, req envparser.RequiredEnv, auth authuser.Authorized) Registerer {
	return &ciJobJWT{
		rawURL: cfg.URL,
		cur: JobData{
			JWT:         req.CIJobJWT,
			Token:       req.JobToken,
			ServiceUser: auth.GitLabAccount(),
		},
	}
}

func (j JobData) postJob(rawURL string) (string, error) {
	reqBody, err := json.Marshal(&j)
	if err != nil {
		return "", err
	}

	registerURL, err := registrationURL(rawURL)
	if err != nil {
		return "", fmt.Errorf("failed to parse registration URL: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(
		ctx,
		"POST",
		registerURL.String(),
		bytes.NewBuffer(reqBody),
	)
	if err != nil {
		return "", err
	}
	defer req.Body.Close()

	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	jobResp := jobResponse{}
	if resp.StatusCode == http.StatusCreated {
		if err = json.NewDecoder(resp.Body).Decode(&jobResp); err != nil {
			return "", fmt.Errorf("failed to decode response: %w", err)
		}
	} else {
		return "", fmt.Errorf("failed to post job data to broker, %s", resp.Status)
	}
	_ = resp.Body.Close()

	validate := validator.New()
	_ = validate.RegisterValidation("sha256", rules.CheckSHA256)
	err = validate.Struct(jobResp)

	return jobResp.BrokerToken, err
}

func registrationURL(rawURL string) (*url.URL, error) {
	u, err := url.Parse(rawURL)
	if err != nil || u.Host == "" || u.Scheme == "" {
		// Undefined host/scheme will lead to issues later, catch now.
		return nil, errors.New("invalid Broker URL configured")
	}
	return u.Parse(brokerRef)
}

type ciJobJWT struct {
	brokerToken string
	rawURL      string
	cur         JobData
}

func (c *ciJobJWT) JobBrokerToken() string {
	return c.brokerToken
}

func (c *ciJobJWT) PostJob() error {
	token, err := c.cur.postJob(c.rawURL)
	c.brokerToken = token
	return err
}

func init() {
	client = &http.Client{Timeout: time.Second * 10}
}
