package logging

import (
	"io/ioutil"
	"runtime"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

func Test_Messengers(t *testing.T) {
	msg := NewMessenger()

	// We currently aren't testing the output, just verify there are
	// no unexpected panics or visually confirm during development.
	t.Run("Testing printing functions", func(t *testing.T) {
		msg.Stdout("hello stdout")
		msg.Stderr("hello stderr")
		msg.Stderr("hello %s", "world")
		msg.Warn("hello %s", "warning")
		msg.Notify("hello notification")
		msg.Error("hello %s\n", "error")
	})
}

func TestEstablishLogger(t *testing.T) {
	if runtime.GOOS != "linux" {
		t.Skip("tests only for Linux")
	}

	location := t.TempDir() + "/test.log"

	type args struct {
		stage string
		opts  configure.Options
		req   envparser.RequiredEnv
	}
	tests := map[string]struct {
		args         args
		assertLogrus func(*testing.T, *logrus.Entry)
		assertError  func(*testing.T, error)
	}{
		"default logging enabled but underlying syslog error": {
			args: args{
				stage: "cleanup",
				opts: configure.Options{
					Auth: configure.Auth{
						Logging: configure.Logging{
							Enabled: true,
							Level:   "Debug",
						},
					},
					General: configure.General{
						Name: "test",
					},
				},
				req: envparser.RequiredEnv{
					JobID:       "123",
					RunnerShort: "short",
				},
			},
			assertError: func(t *testing.T, err error) {
				// targeting golang image test environment, we need to update
				// if additional logging options are added (e.g., log to specific file).
				assert.EqualError(t, err, "Unix syslog delivery error")
			},
		},
		"logging disabled, empty entry returned": {
			args: args{
				opts: configure.Options{},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertLogrus: func(t *testing.T, l *logrus.Entry) {
				assert.Equal(t, ioutil.Discard, l.Logger.Out)
			},
		},
		"logging to file with different levels": {
			args: args{
				stage: "cleanup",
				opts: configure.Options{
					Auth: configure.Auth{
						Logging: configure.Logging{
							Enabled:  true,
							Level:    "warn",
							Location: location,
						},
					},
				},
				req: envparser.RequiredEnv{
					JobID:       "123",
					RunnerShort: "short",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertLogrus: func(t *testing.T, l *logrus.Entry) {
				l.Warn("ci test warning")
				l.Debug("ci test debug")
				f, err := ioutil.ReadFile(location)
				assert.NoError(t, err, "error locating logging file")
				assert.Contains(t, string(f), "ci test warning")
				assert.NotContains(t, string(f), "ci test debug")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := EstablishLogger(tt.args.stage, tt.args.opts, tt.args.req)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertLogrus != nil {
				tt.assertLogrus(t, got)
			}
		})
	}
}
