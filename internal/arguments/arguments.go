// Package arguments manages the command line subcommands and shared arguments
// for the Jacamar CI applications. This structure is to support the multi-application
// requirements along with some aspects of the go-args library currently being used.
package arguments

// ConfigCmd define the arguments for config_exec stage.
type ConfigCmd struct {
	Configuration string `arg:"required" help:"Target configuration for the runner, expected full path to TOML compliant file."`
}

// PrepareCmd defines the arguments for the prepare_exec stage.
type PrepareCmd struct{}

// RunCmd defines the arguments for the run_exec stage.
type RunCmd struct {
	Script string `arg:"positional" help:"The path to the script that GitLab Runner creates for the Custom executor to run."`
	Stage  string `arg:"positional" help:"Name of the stage."`
}

// CleanupCmd defines the arguments for the cleanup_exec stage.
type CleanupCmd struct {
	Configuration string `arg:"required" help:"Target configuration for the runner, expected full path to TOML compliant file."`
}

// TranslateCmd defines arguments for the utils package support for TOML translation.
type TranslateCmd struct {
	Source string `arg:"positional" help:"The GitLab configuration TOML you wish to translate (default: /etc/gitlab-runner/config.toml)." default:"/etc/gitlab-runner/config.toml"`
	Target string `arg:"positional" help:"Optional target file for the newly generated Jacamar configuration file, default to current working directory when not provided."`
}

// EnvCmd define arguments for userspace Jacamar to assist in environment variable resolution
// and identification. Should only be observed with --no-auth.
type EnvCmd struct {
	Input string `arg:"positional" help:"String containing variable(s) that is the target for expansion in a working (login) environment. Result will be returned to stdout."`
}

// ConcreteArgs organizes all supported subcommands.
type ConcreteArgs struct {
	Config    *ConfigCmd
	Prepare   *PrepareCmd
	Run       *RunCmd
	Cleanup   *CleanupCmd
	Translate *TranslateCmd
	EnvCmd    *EnvCmd
	// UnobfuscatedError removes the default obfuscation from all authorization level errors.
	UnobfuscatedError bool
	// NoAuth skip all authorization level functionality (controlled via the [auth] table in
	// configuration). This is only advised if either downscoping has already occurred or jobs
	// are meant to execute under the runner user.
	NoAuth bool
}
