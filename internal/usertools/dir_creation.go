package usertools

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"syscall"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
)

// CreateDirectories manages the CI directory creation process for a validated user
// and target directory structures.
func CreateDirectories(au authuser.Authorized) error {
	directories := []string{
		au.BaseDir(),
		au.BuildsDir(),
		au.CacheDir(),
		au.ScriptDir(),
	}

	for _, dir := range directories {
		// It is worth verifying the permissions for each and every directory
		// to account for potential
		if err := verifyBasePermissions(dir); err != nil {
			return err
		}
		if err := mkdir(dir); err != nil {
			return err
		}
	}

	return nil
}

var curUserFactory = authuser.CurrentUser

// verifyBasePermissions ensures that if an existing directory is found, permissions
// and ownership are validated.
func verifyBasePermissions(dir string) error {
	if dir == "" {
		return errors.New("no base directory defined, verify runner configuration")
	}

	info, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("failed to identify target directory creation status: %w", err)
	} else if os.IsNotExist(err) {
		return nil
	}

	usr, err := curUserFactory()
	if err != nil {
		return fmt.Errorf("error while attempting to identifying current user: %w", err)
	}

	uid, _ := strconv.Atoi(usr.Uid)
	if info.Sys().(*syscall.Stat_t).Uid != uint32(uid) {
		return fmt.Errorf(
			"invalid ownership detected on directory %s, this must be manually addressed",
			dir,
		)
	}

	if info.Mode().String() != "drwx------" {
		return fmt.Errorf(
			"invalid permissions detected on directory %s (%s), this must be manually addressed",
			dir,
			info.Mode().String(),
		)
	}

	return nil
}

// mkdir creates every directory provided with the package's defined dirPermissions.
func mkdir(dir string) error {
	syscall.Umask(FileUmask) // Enforce 0700 directory creation for user.

	path := filepath.Clean(dir)
	if err := os.MkdirAll(path, FilePermissions); err != nil {
		return err
	}

	return nil
}
