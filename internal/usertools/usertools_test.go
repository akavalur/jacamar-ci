package usertools

import (
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
)

func Test_ConfirmHelperConst(t *testing.T) {
	// At no point should we change this value!
	assert.Equal(t, 0700, FilePermissions, "invalid change to FilePermissions constant")
	assert.Equal(t, 077, FileUmask, "invalid change to FileUmask constant")
}

func mockMissing(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BaseDir().Return("/does/not/exist").MaxTimes(1)
	m.EXPECT().BuildsDir().Return("/does/not/exist/builds").MaxTimes(1)
	m.EXPECT().CacheDir().Return("/does/not/exist/cache").MaxTimes(1)
	m.EXPECT().ScriptDir().Return("/does/not/exist/script").MaxTimes(1)
	return m
}

func mockPermissions(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BaseDir().Return("/var/tmp").MaxTimes(1)
	m.EXPECT().BuildsDir().Return("/var/tmp/builds").MaxTimes(1)
	m.EXPECT().CacheDir().Return("/var/tmp/cache").MaxTimes(1)
	m.EXPECT().ScriptDir().Return("/var/tmp/script").MaxTimes(1)
	return m
}

func Test_PreCleanupVerification(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]dirTests{
		"base directory does not exist": {
			au: mockMissing(ctrl),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"permissions issues on base directory": {
			au: mockPermissions(ctrl),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := PreCleanupVerification(tt.au)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_FileGeneration(t *testing.T) {
	t.Run("create script", func(t *testing.T) {
		file := t.TempDir() + "/CreateScript"
		err := CreateScript(file, `hello world`)
		assert.NoError(t, err, "CreateScript()")

		f, err := os.Stat(file)
		assert.NoError(t, err, "Stat(file)")
		assert.Equal(t, "-rwx------", f.Mode().String())
	})

	t.Run("create file", func(t *testing.T) {
		file := t.TempDir() + "/CreateFile"
		err := CreateFile(file, `hello world`)
		assert.NoError(t, err, "CreateFile()")

		f, err := os.Stat(file)
		assert.NoError(t, err, "Stat(file)")
		assert.Equal(t, "-rw-------", f.Mode().String())
	})
}
