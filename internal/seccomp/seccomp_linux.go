// +build linux

// Package seccomp for managing Jacamar's bindings for libseccomp.
package seccomp

import (
	"syscall"

	libseccomp "github.com/seccomp/libseccomp-golang"

	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
)

var (
	allowAct libseccomp.ScmpAction
	blockAct libseccomp.ScmpAction
)

func init() {
	allowAct = libseccomp.ActAllow
	blockAct = libseccomp.ActErrno.SetReturnCode(int16(syscall.EPERM))
}

// Enable establishes and loads appropriate filters based upon a set of Rules.
// Errors generated during this should be observed and result in a system failure.
func (r Rules) Enable() error {
	if r.disabled {
		return nil
	}

	filter, err := r.sysFilter()
	if err != nil {
		return errorhandling.NewSeccompError(err)
	}

	if err = r.applyRules(filter); err != nil {
		return errorhandling.NewSeccompError(err)
	}

	if err = filter.Load(); err != nil {
		return errorhandling.NewSeccompError(err)
	}

	return nil
}

func (r Rules) applyRules(filter *libseccomp.ScmpFilter) (err error) {
	var syscallID libseccomp.ScmpSyscall
	rule := r.sysRule()

	for _, element := range r.list {
		syscallID, err = libseccomp.GetSyscallFromName(element)
		if err == nil {
			err = filter.AddRule(syscallID, rule)
		}
		if err != nil {
			return
		}
	}

	return
}

func (r Rules) sysFilter() (*libseccomp.ScmpFilter, error) {
	if r.blockAll {
		return libseccomp.NewFilter(blockAct)
	}
	return libseccomp.NewFilter(allowAct)
}

func (r Rules) sysRule() libseccomp.ScmpAction {
	if r.blockAll {
		return allowAct
	}
	return blockAct
}
