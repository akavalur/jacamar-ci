package seccomp

import (
	"testing"
)

func TestRules_Enable(t *testing.T) {
	tests := map[string]struct {
		rules   Rules
		wantErr bool
	}{
		"block default": {
			rules: Rules{
				blockAll: false,
			},
			wantErr: false,
		},
		"rules disabled": {
			rules: Rules{
				disabled: true,
			},
			wantErr: false,
		},
		"rules enabled none defined": {
			wantErr: false,
		},
		"incorrect block target declared": {
			rules: Rules{
				list: []string{
					"notasyscall",
				},
			},
			wantErr: true,
		},
		"getpgid blocked": {
			rules: Rules{
				list: []string{
					"getpgid",
				},
			},
			wantErr: false,
		},
		"incorrect allow target declared": {
			rules: Rules{
				blockAll: true,
				list: []string{
					"notasyscall",
				},
			},
			wantErr: true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if err := tt.rules.Enable(); (err != nil) != tt.wantErr {
				t.Errorf("Enable() error = %v, wantErr %v", err, tt.wantErr)
			}
			// Functionality beyond simply ensuring the rules are applied accomplished
			// with Pavilion testing.
		})
	}
}
