// +build darwin

// Package seccomp for managing Jacamar's bindings for libseccomp, raises error on Darwin.
// This is to help avoid errors while developing on a Mac.
package seccomp

import (
	"errors"
)

// Enable will fail on Darwin.
func (r Rules) Enable() error {
	return errors.New("seccomp not supported on MacOS")
}
