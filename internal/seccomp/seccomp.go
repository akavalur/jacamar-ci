package seccomp

import (
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
)

type Rules struct {
	disabled bool
	blockAll bool
	list     []string
}

// Establish create rules for establishment of seccomp filters based upon the administratively
// controlled Jacamar configuration.
func Establish(c arguments.ConcreteArgs, opt configure.Options) (r Rules) {
	if opt.Auth.Seccomp.Disabled {
		r.disabled = true
		return
	}

	r.blockAll = opt.Auth.Seccomp.BlockAll
	if !r.blockAll {
		setuid(&r, c, opt)
		r.list = removeDuplicate(append(r.list, opt.Auth.Seccomp.BlockCalls...))
		if len(opt.Auth.Seccomp.AllowCalls) > 0 {
			r.list = removeDoubles(append(r.list, opt.Auth.Seccomp.AllowCalls...))
		}
	} else {
		r.list = removeDuplicate(opt.Auth.Seccomp.AllowCalls)
	}

	return
}

func setuid(r *Rules, c arguments.ConcreteArgs, opt configure.Options) {
	if opt.Auth.Downscope == "setuid" && !cobaltSetuidException(c, opt) {
		r.list = append(r.list, "ioctl")
	}
}

// cobaltSetuidException implement a cobalt only exception for ioctl to be observed in a limited
// fashion only when the job script is known to be submitted to the scheduler.
func cobaltSetuidException(c arguments.ConcreteArgs, opt configure.Options) bool {
	tar := strings.TrimSpace(strings.ToLower(opt.General.Executor))
	if tar != "cobalt" && tar != "qsub" {
		return false
	}

	return c.Run != nil && (c.Run.Stage == "step_script" || c.Run.Stage == "build_script")
}

func removeDuplicate(source []string) (target []string) {
	unique := make(map[string]bool)

	for _, val := range source {
		if _, found := unique[val]; !found {
			unique[val] = true
			target = append(target, val)
		}
	}

	return target
}

func removeDoubles(source []string) (target []string) {
	num := make(map[string]int)

	for _, val := range source {
		num[val] += 1
	}

	for i, val := range num {
		if val == 1 {
			target = append(target, i)
		}
	}

	return
}
