package seccomp

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
)

var runC, cfgC arguments.ConcreteArgs

func TestEstablish(t *testing.T) {
	type args struct {
		c   arguments.ConcreteArgs
		opt configure.Options
	}
	tests := map[string]struct {
		args  args
		wantR Rules
	}{
		"standard rules enforced for setuid": {
			args: args{
				c: runC,
				opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
				},
			},
			wantR: Rules{
				disabled: false,
				list: []string{
					"ioctl",
				},
			},
		},
		"standard rules enforces for sudo": {
			args: args{
				c: cfgC,
				opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "sudo",
					},
				},
			},
			wantR: Rules{},
		},
		"global disabled identified": {
			args: args{
				c: cfgC,
				opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							Disabled: true,
						},
					},
				},
			},
			wantR: Rules{
				disabled: true,
			},
		},
		"duplicate blocked syscalls removed": {
			args: args{
				c: cfgC,
				opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							BlockCalls: []string{
								"ioctl",
								"sethostname",
								"sendfile",
							},
						},
					},
				},
			},
			wantR: Rules{
				list: []string{
					"ioctl",
					"sethostname",
					"sendfile",
				},
			},
		},
		"cobalt executor with setuid": {
			args: args{
				c: runC,
				opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
					General: configure.General{
						Executor: "cobalt",
					},
				},
			},
			wantR: Rules{},
		},
		"block all and allow list": {
			args: args{
				c: runC,
				opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							BlockAll: true,
							AllowCalls: []string{
								"sethostname",
								"sendfile",
							},
						},
					},
				},
			},
			wantR: Rules{
				blockAll: true,
				list: []string{
					"sethostname",
					"sendfile",
				},
			},
		},
		"allow system blocked": {
			args: args{
				c: runC,
				opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							BlockCalls: []string{
								"sethostname",
							},
							AllowCalls: []string{
								"ioctl",
							},
						},
					},
				},
			},
			wantR: Rules{
				list: []string{
					"sethostname",
				},
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			gotR := Establish(tt.args.c, tt.args.opt)

			assert.Equal(t, tt.wantR, gotR)
		})
	}
}

func Test_cobaltSetuidException(t *testing.T) {
	type args struct {
		c   arguments.ConcreteArgs
		opt configure.Options
	}
	tests := map[string]struct {
		args args
		want bool
	}{
		"shell executor": {
			args: args{
				c: runC,
				opt: configure.Options{
					General: configure.General{
						Executor: "shell",
					},
				},
			},
			want: false,
		},
		"cobalt executor step_script": {
			args: args{
				c: runC,
				opt: configure.Options{
					General: configure.General{
						Executor: "cobalt",
					},
				},
			},
			want: true,
		},
		"cobalt executor config stage": {
			args: args{
				c: cfgC,
				opt: configure.Options{
					General: configure.General{
						Executor: "cobalt",
					},
				},
			},
			want: false,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if got := cobaltSetuidException(tt.args.c, tt.args.opt); got != tt.want {
				t.Errorf("cobaltSetuidException() = %v, want %v", got, tt.want)
			}
		})
	}
}

func init() {
	runC = arguments.ConcreteArgs{
		Run: &arguments.RunCmd{
			Stage: "step_script",
		},
	}

	cfgC = arguments.ConcreteArgs{
		Config: &arguments.ConfigCmd{
			Configuration: "/some/file/config.toml",
		},
	}
}
