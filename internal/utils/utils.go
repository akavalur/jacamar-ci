// Package utils is only for supporting the jacamar-utils application. This seeks to support
// a most friendly, command line driven set of interactions for both administrator as well
// as users with the sometimes complicated requirements that come with registration,
// configuring, and maintaining Jacamar CI.
package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

var (
	msg                  logging.Messenger
	scanner              func(*int) error
	errExit, successExit func()
)

// Run manages the complete workflow for any subcommand relating to admin/user utilities not
// part of the traditional custom executor workflow.
func Run(c arguments.ConcreteArgs) {
	switch {
	case c.Translate != nil:
		if err := translateConfiguration(*c.Translate); err != nil {
			failure(err.Error())
		}
	case c.EnvCmd != nil:
		if err := envIdentifications(*c.EnvCmd, msg); err != nil {
			failure(err.Error())
		}
	default:
		failure("unsupported subcommand detected")
	}

	// successfully close application
	successExit()
}

func failure(s string) {
	msg.Error(s)
	errExit()
}

func createFile(b []byte, tar string) error {
	tar, _ = filepath.Abs(filepath.Clean(tar))
	err := ioutil.WriteFile(tar, b, 0600)
	return err
}

func init() {
	msg = logging.NewMessenger()
	scanner = func(in *int) error {
		_, err := fmt.Scanf("%d", in)
		return err
	}
	errExit = func() { os.Exit(1) }
	successExit = func() { os.Exit(0) }
}
