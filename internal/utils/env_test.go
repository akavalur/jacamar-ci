package utils

import (
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func Test_envIdentifications(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	usr, _ := user.Current()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Stdout(gomock.Eq(usr.HomeDir + "/.test"))
	m.EXPECT().Stdout(gomock.Eq("/.test"))

	tests := []struct {
		name          string
		arg           arguments.EnvCmd
		msg           logging.Messenger
		mockSomething func(*testing.T)
		assertError   func(*testing.T, error)
	}{
		{
			name:        "no arguments (currently encountered an error)",
			assertError: tst.AssertError,
		}, {
			name: "resolve simple input",
			arg: arguments.EnvCmd{
				Input: "$HOME/.test",
			},
			msg:         m,
			assertError: tst.AssertNoError,
		}, {
			// We don't check this here, rely on calling process to validate.
			name: "resolve empty",
			arg: arguments.EnvCmd{
				Input: "$NOT_A_VALID_JACAMAR_VAR/.test",
			},
			msg:         m,
			assertError: tst.AssertNoError,
		}, {
			name: "maximum length encountered",
			arg: arguments.EnvCmd{
				Input: "$HOME/.test",
			},
			msg: m,
			mockSomething: func(t *testing.T) {
				maxLength = 1
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "maximum length of expanded value (1) reached")
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mockSomething != nil {
				tt.mockSomething(t)
			}

			err := envIdentifications(tt.arg, tt.msg)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
