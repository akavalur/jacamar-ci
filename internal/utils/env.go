package utils

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

var (
	// prePostFix are used to help differentiate output from a related evaluation from that of
	// unexpected login shell stdout/stderr.
	prePostFix = "{{JACAMAR_ENV}}"
	// maxLength represents an arbitrary maximum length of potential value that could be returned.
	maxLength = 2000
)

// IdentifyEnvReturn extracts contents of an 'jacamar env ?' command from the provided
// stdout. No validation is preformed on the results and remains the responsibility of the caller.
func IdentifyEnvReturn(out string) (string, error) {
	if strings.Count(out, prePostFix) != 2 {
		return "", fmt.Errorf("output does not contain expected pre+postfix")
	}

	// Previously verified that the pre/postfix will be present.
	parts := strings.Split(out, prePostFix)

	return parts[1], nil
}

func envIdentifications(arg arguments.EnvCmd, msg logging.Messenger) (err error) {
	if arg.Input != "" {
		err = resolveInput(arg.Input, msg)
	} else {
		err = errors.New("input required at this time")
	}

	return
}

func resolveInput(target string, msg logging.Messenger) error {
	cmd := createCommand(target)

	out, err := cmd.CombinedOutput()
	if err != nil {
		return err
	}

	sOut := string(out)
	if len(sOut) > maxLength {
		return fmt.Errorf("maximum length of expanded value (%d) reached", maxLength)
	}

	sOut, err = IdentifyEnvReturn(sOut)
	if err != nil {
		return err
	}
	msg.Stdout(sOut)

	return nil
}

func createCommand(target string) *exec.Cmd {
	target = strings.Join([]string{prePostFix, target, prePostFix}, "")

	/* #nosec */
	// executed only within userspace in limited conditions.
	cmd := exec.Command(
		"/usr/bin/env",
		"-i",
		fmt.Sprintf("HOME=%s", os.Getenv("HOME")),
		"/bin/bash",
		"--login",
		"-c",
		fmt.Sprintf("eval echo -e \"\\%s\"", target),
	)

	return cmd
}
