package utils

import (
	"testing"

	"github.com/golang/mock/gomock"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type utilsTests struct {
	c       arguments.ConcreteArgs
	scanner func(*int) error

	assertError   func(*testing.T, error)
	assertResults func(*testing.T, arguments.ConcreteArgs)
}

func TestRun(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Error("unsupported subcommand detected").Times(1)
	m.EXPECT().Error("open /config/does/not/exist.toml: no such file or directory").Times(1)
	m.EXPECT().Error("input required at this time").Times(1)

	msg = m
	errExit = func() {}
	successExit = func() {}

	tests := map[string]utilsTests{
		"unsupported subcommand, default enforced": {
			c: arguments.ConcreteArgs{},
		},
		"error encountered during translation": {
			c: arguments.ConcreteArgs{
				Translate: &arguments.TranslateCmd{
					Source: "/config/does/not/exist.toml",
				},
			},
		},
		"error encountered during env": {
			c: arguments.ConcreteArgs{
				EnvCmd: &arguments.EnvCmd{},
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			Run(tt.c)
		})
	}
}
