package utils

import (
	"errors"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func Test_translateConfiguration(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Since we require testing of default behaviors, cleanup after test.
	defer removeTestConfigs()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Notify(gomock.Any()).AnyTimes()
	m.EXPECT().Stdout(gomock.Any()).AnyTimes()
	m.EXPECT().Warn(gomock.Eq("BatchRequiredSpecs is not supported by Jacamar")).Times(1)
	m.EXPECT().Warn(gomock.Eq("HideSensitiveEnv is not supported by Jacamar")).Times(1)
	m.EXPECT().Warn(gomock.Eq(
		"A data directory must be specified in order for Jacamar CI to function,please address manually ([genera].data_dir)",
	)).Times(1)
	m.EXPECT().Warn(gomock.Eq(
		"Unable to identify batch system, address manually ([genera].executor)",
	)).Times(1)
	m.EXPECT().Error(gomock.Eq(
		"invalid executor detected, must be either shell or batch executor",
	)).Times(1)

	msg = m
	errExit = func() {}

	tests := map[string]utilsTests{
		"non-existent source configuration file": {
			c: arguments.ConcreteArgs{
				Translate: &arguments.TranslateCmd{
					Source: "/config/does/not/exist.toml",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "open /config/does/not/exist.toml: no such file or directory")
			},
		},
		"user input error detected": {
			c: arguments.ConcreteArgs{
				Translate: &arguments.TranslateCmd{
					Source: "../../test/testdata/translate/complete_source.toml",
				},
			},
			scanner: func(in *int) error {
				return errors.New("error message")
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"unsupported executor source error encountered": {
			c: arguments.ConcreteArgs{
				Translate: &arguments.TranslateCmd{
					Source: "../../test/testdata/translate/multiple_runners.toml",
				},
			},
			scanner: func(in *int) error {
				*in = 1
				return nil
			},
		},
		"missing specific batch in source config": {
			c: arguments.ConcreteArgs{
				Translate: &arguments.TranslateCmd{
					Source: "../../test/testdata/translate/multiple_runners.toml",
				},
			},
			scanner: func(in *int) error {
				*in = 0
				return nil
			},
		},
		"complete source file translated": {
			c: arguments.ConcreteArgs{
				Translate: &arguments.TranslateCmd{
					Source: "../../test/testdata/translate/complete_source.toml",
					Target: t.TempDir() + "/test.toml",
				},
			},
			scanner: func(in *int) error {
				*in = 0
				return nil
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertResults: func(t *testing.T, c arguments.ConcreteArgs) {
				assert.FileExists(t, c.Translate.Target)
				cfg, err := configure.NewConfig(c.Translate.Target)
				assert.NoError(t, err, "unexpected error loading config")
				assert.Equal(
					t,
					configure.Options{
						General: configure.General{
							Name:           "Complete Source",
							Executor:       "slurm",
							DataDir:        "/ecp",
							RetainLogs:     true,
							CustomBuildDir: true,
						},
						Batch: configure.Batch{
							ArgumentsVariable: []string{"TEST_PARAMETERS"},
							NFSTimeout:        "30s",
							SchedulerBin:      "/usr/bin",
						},
						Auth: configure.Auth{
							RunAs: configure.RunAs{
								ValidationScript: "/etc/gitlab-runner/runas.bash",
								Federated:        true,
							},
							UserAllowlist:  []string{"usr1", "usr2"},
							UserBlocklist:  []string{"usr3"},
							GroupAllowlist: []string{"grp1"},
							GroupBlocklist: []string{"grp2", "grp3"},
							ShellAllowlist: []string{"/bin/bash"},
							Downscope:      "setuid",
							Logging: configure.Logging{
								Enabled: false,
							},
							Broker: configure.Broker{
								URL: "",
							},
						},
					},
					cfg.Options(),
				)

			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.scanner != nil {
				scanner = tt.scanner
			}

			err := translateConfiguration(*tt.c.Translate)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertResults != nil {
				tt.assertResults(t, tt.c)
			}
		})
	}
}

func removeTestConfigs() {
	path, err := os.Getwd()
	if err != nil {
		// Since this is optional cleanup if an error is encountered just skip.
		return
	}

	var files []string
	err = filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})

	for _, file := range files {
		if strings.Contains(file, "/jacamar-config-") && strings.HasSuffix(file, ".toml") {
			_ = os.Remove(file)
		}
	}
}
