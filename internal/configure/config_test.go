package configure

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var fullOptions, prepOptions Options

func TestNewConfig(t *testing.T) {
	t.Run("Complete/valid auth config file", func(t *testing.T) {
		cfg, err := NewConfig("../../test/testdata/valid_auth_config.toml")
		assert.Nil(t, err)
		assert.Equal(t, fullOptions, cfg.Options())
		assert.Equal(t, fullOptions.Auth, cfg.Auth())
		assert.Equal(t, fullOptions.General, cfg.General())
		assert.Equal(t, fullOptions.Batch, cfg.Batch())

		contents, err := cfg.PrepareState(true)
		assert.NoError(t, err, "PrepareState()")
		assert.NotNil(t, contents, "PrepareState()")

		os.Setenv(EnvVariable, contents)
	})

	t.Run("Complete/valid auth config contents", func(t *testing.T) {
		got, err := NewConfig(EnvVariable)
		assert.NoError(t, err)
		assert.Equal(t, prepOptions, got.Options())
	})

	t.Run("Missing configuration file", func(t *testing.T) {
		_, err := NewConfig("/not/a/valid/file.toml")
		assert.Error(t, err)
	})

	t.Run("Invalid configuration", func(t *testing.T) {
		os.Setenv(EnvVariable, "[]")
		_, err := NewConfig(EnvVariable)
		assert.Error(t, err)
	})
}

func Test_loadContents(t *testing.T) {
	type args struct {
		contents string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "No contents",
			args:    args{contents: ""},
			wantErr: true,
		}, {
			name: "Invalid TOML",
			args: args{
				contents: `[general]
name = "Invalid"
executor = 42
`,
			},
			wantErr: true,
		}, {
			name: "Valid configuration",
			args: args{
				contents: `[general]
name = "Valid"
executor = "batch"

[batch]
arguments_variable = ["SCHEDULER_PARAMETERS"]
`,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := loadContents(tt.args.contents)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}
}

func Test_encode(t *testing.T) {
	opt := Options{
		General: General{
			Name:     "Valid",
			Executor: "batch",
		},
		Batch: Batch{
			ArgumentsVariable: []string{"PARAMETER"},
		},
	}
	t.Run("Encode valid options", func(t *testing.T) {
		_, err := encode(opt)
		assert.Nil(t, err)
	})
}

func init() {
	fullOptions = Options{
		General: General{
			Name:        "Complete config",
			Executor:    "batch",
			RetainLogs:  true,
			DataDir:     "/ecp",
			KillTimeout: "30s",
		},
		Auth: Auth{
			Downscope:          "setuid",
			MaxEnvChars:        10000,
			UserAllowlist:      []string{"u1"},
			UserBlocklist:      []string{"g1", "g2"},
			GroupAllowlist:     []string{"u2"},
			GroupBlocklist:     []string{"g3"},
			ShellAllowlist:     []string{"/bin/bash"},
			ListsPreValidation: true,
			RootDirCreation:    true,
			RunAs: RunAs{
				ValidationScript: "valid.bash",
				RunAsVariable:    "RUNAS",
			},
			Logging: Logging{
				Enabled:  true,
				Level:    "debug",
				Location: "syslog",
			},
		},
		Batch: Batch{
			ArgumentsVariable: []string{"FOO", "BAR"},
			CommandDelay:      "30s",
			NFSTimeout:        "1m",
		},
	}

	prepOptions = Options{
		General: fullOptions.General,
		Batch:   fullOptions.Batch,
		Auth:    Auth{},
	}
}
