package configure

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/BurntSushi/toml"
)

// Config maintains all identified options applicable to the running job.
type Config struct {
	opt Options
}

// NewConfig accepts a target configuration, either a complete file path
// or the configure.EnvVariable. The appropriate contents of the associated
// target will be loaded and made available through the returned interface.
func NewConfig(tar string) (cfg Config, err error) {
	if tar == EnvVariable {
		encoded := os.Getenv(EnvVariable)
		var decoded []byte
		decoded, err = base64.StdEncoding.DecodeString(encoded)
		if err != nil {
			return Config{}, fmt.Errorf(
				"error executing DecodeString() on %s contents, %w", EnvVariable, err,
			)
		}
		cfg.opt, err = loadContents(string(decoded))
	} else {
		cfg.opt, err = loadFile(tar)
	}

	return
}

// PrepareState creates an encoded TOML (string) for usage as a environment
// variable to be provided to another process. By declaring scope any
// [Auth] related configuration will be removed.
func (c Config) PrepareState(scope bool) (string, error) {
	if scope {
		c.opt = Options{
			General: c.opt.General,
			Batch:   c.opt.Batch,
		}
	}
	return encode(c.opt)
}

// Options returns the contents of a decoded configuration.
func (c Config) Options() Options {
	return c.opt
}

// General return the structure of a decoded configuration.
func (c Config) General() General {
	return c.opt.General
}

// Auth return the structure of a decoded configuration.
func (c Config) Auth() Auth {
	return c.opt.Auth
}

// Batch return the structure of a decoded configuration.
func (c Config) Batch() Batch {
	return c.opt.Batch
}

func defaults() Options {
	return Options{
		Auth: Auth{
			MaxEnvChars: defMaxEnvChars,
			Logging: Logging{
				Level:    defLogLevel,
				Location: defLogLocation,
			},
		},
		Batch: Batch{
			NFSTimeout: defNFSTimeout,
		},
		General: General{
			KillTimeout: defKillTimeout,
		},
	}
}

// LoadFile loads the specified TOML file and decodes the contents
// into the Options struct.
func loadFile(file string) (Options, error) {
	r, err := ioutil.ReadFile(filepath.Clean(file))
	if err != nil {
		return Options{}, fmt.Errorf("error executing ReadFile(), %w", err)
	}

	return loadContents(string(r))
}

// loadContents decodes the already read contents of a TOML file into the Options struct.
func loadContents(contents string) (Options, error) {
	if contents == "" {
		return Options{}, fmt.Errorf(
			"missing configuration, expected in runner environment (%s)", EnvVariable,
		)
	}

	cfg := defaults()
	r := strings.NewReader(contents)
	if _, err := toml.DecodeReader(r, &cfg); err != nil {
		return Options{}, fmt.Errorf("error executing DecodeReader(), %w", err)
	}

	return cfg, nil
}

// encode returns the TOML of the options as a string in Base64.
func encode(opt Options) (string, error) {
	buf := new(bytes.Buffer)
	if err := toml.NewEncoder(buf).Encode(opt); err != nil {
		return "", fmt.Errorf("error executing NewEncoder(), %w", err)
	}

	return base64.StdEncoding.EncodeToString(buf.Bytes()), nil
}
