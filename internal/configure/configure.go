// Package configure maintains all related structures for the Jacamar CI configuration
// through the supplied TOML file. All interactions with the config should be accomplished
// using the supported Configurer interface.
package configure

const (
	// EnvVariable is expected in the job's environment for the stateful configuration.
	EnvVariable    = "JACAMAR_CI_CONFIG_STR"
	defMaxEnvChars = 10000
	defNFSTimeout  = "30s"
	defKillTimeout = "30s"
	defLogLevel    = "debug"
	defLogLocation = "syslog"
)

// Configurer implements an interface for loading and interacting with the the executor's
// configuration and subsequent Options structure.
type Configurer interface {
	PrepareState(bool) (string, error)
	Options() Options
	General() General
	Auth() Auth
	Batch() Batch
}

// RunAs represents configuration of the RunAs (e.g. service user) to affect
// or validate the user context in the auth flow.
type RunAs struct {
	// ValidationScript the path to a script where the RunAs user will be checked
	// against known job context.
	ValidationScript string `toml:"validation_script"`
	// ValidationPlugin the path to a Go plugin where the RunAs user will be checked
	// against known job context.
	ValidationPlugin string `toml:"validation_plugin"`
	// SHA256 sum of plugin/script, if provided will be checked.
	SHA256 string `toml:"sha256"`
	// RunAsVariable defines the name of the variable the RunAs user will be passed in as
	// from the .gitlab-ci.yml file.
	RunAsVariable string `toml:"user_variable"`
	// Federated enables the any validation script/plugin to be provided additional
	// federated context.
	Federated bool
}

// Logging represents configuration of how the jacamar-auth application will log relevant information.
type Logging struct {
	// Enabled allows for system logging of key results from the jacamar-auth application.
	Enabled bool `toml:"enabled"`
	// Level denotes the logging level of messages saved (default: debug).
	Level string `toml:"log_level"`
	// Location identifies where logs will be saved, this can be a distinct file or syslog (default).
	// In the case of syslog, we will connect to the appropriate log daemon, targeting the
	// syslog server if related values are not specified.
	Location string `toml:"location"`
	// Network specified (e.g., tcp) used for remote log daemon connections.
	Network string `toml:"network"`
	// Address specified (e.g., localhost:1234) used for remote log daemon connections.
	Address string `toml:"address"`
}

// Broker organizes configurations relating to the token broker service.
type Broker struct {
	// URL (e.g. broker.example.gov) for the token broker service. Required to use the broker.
	URL string `toml:"url"`
}

// Seccomp organizes all configurations relating to the manage of seccomp(2) rules.
type Seccomp struct {
	// Disabled globally turns off all rules.
	Disabled bool `toml:"disabled"`
	// BlockAll globally blocks all system calls from being used, this means reliance on
	// a manually defined list of AllowCalls.
	BlockAll bool `toml:"block_all"`
	// BlockCalls list of system calls that will be prevented and result in error
	// when encountered.
	BlockCalls []string `toml:"block_calls"`
	// AllowCalls list of system call that will be allowed, this take precedence over
	// any manually or system defined blocked calls.
	AllowCalls []string `toml:"allow_calls"`
}

// Auth represents authorization process configuration for establishing a local
// target account that is associated with the GitLab user who trigger the job.
type Auth struct {
	RunAs RunAs `toml:"runas"`

	// UserAllowlist is an authoritative list of users who can execute CI jobs.
	UserAllowlist []string `toml:"user_allowlist"`
	// UserBlocklist is a list of usernames that are not allowed to run CI jobs. More
	// authoritative than group lists, but can be overridden by UserAllowlist.
	UserBlocklist []string `toml:"user_blocklist"`
	// GroupAllowlist is a list of groups that are allowed to run CI jobs. Least authoritative.
	GroupAllowlist []string `toml:"groups_allowlist"`
	// GroupBlocklist is a list of groups that are not allowed to run CI jobs.
	GroupBlocklist []string `toml:"groups_blocklist"`
	// ShellAllowlist is an authoritative list of acceptable shells that are defined in the
	// user database.
	ShellAllowlist []string `toml:"shell_allowlist"`

	// Downscope target mechanism for execution of all CI scripts and generated
	// commands through the auth mechanisms (e.g. setuid).
	Downscope string `toml:"downscope"`
	// JacamarPath full path to the Jacamar application, used in constructing the
	// command for job execution.
	JacamarPath string `toml:"jacamar_path"`
	// DownscopeEnv is an array of "key=value" pairs that will be used when constructing the
	// environment for the downscoped command. Care must be given as values provided here take
	// the highest priority and will override any identified by Jacamar.
	DownscopeEnv []string `toml:"downscope_env"`
	// DownscopeCmdDir is a development/debug only command allow direct manipulation of the
	// working directory for any downscope command generate (see: https://golang.org/pkg/os/exec/#Cmd).
	DownscopeCmdDir string `toml:"downscope_cmd_dir"`

	Broker Broker `toml:"broker"`

	// MaxEnvChars is the maximum number of characters one can have in the environment
	// (default: 10000)
	MaxEnvChars int `toml:"max_env_chars"`
	// ListsPreValidation indicates if the allow/block list rules should be observed
	// prior to the execution of the RunAS validate script.
	ListsPreValidation bool `toml:"lists_pre_validation"`
	// RootDirCreation indicates if the trusted root level directory (e.g. 0700 permissions)
	// will be created and chown called.
	RootDirCreation bool `toml:"root_dir_creation"`

	Logging Logging `toml:"logging"`
	Seccomp Seccomp `toml:"seccomp"`
}

// Batch represents configuration related to HPC schedulers.
type Batch struct {
	// ArgumentsVariable potential CI variables for user provided arguments for job submission
	// (default (always enforced): SCHEDULER_PARAMETERS).
	ArgumentsVariable []string `toml:"arguments_variable"`
	// CommandDelay is the sleep time for all command line interactions with the underlying
	// scheduler, this is a duration string (default: 30s).
	CommandDelay string `toml:"command_delay"`
	// NFSTimeout is the largest possible delay expected from NFS for files, this is a
	// duration string (e.g. 1m).
	NFSTimeout string `toml:"nfs_timeout"`
	// SchedulerBin a directory path used as prefix for all jop submission commands
	// (e.g. /usr/scheduler/bin).
	SchedulerBin string `toml:"scheduler_bin"`
}

// General represents configuration options that can apply throughout the job (from authorization
// to execution).
type General struct {
	// Name option description, associated with config in system logs.
	Name string `toml:"name"`
	// Executor type, eg. shell, cobalt, slurm, etc.
	Executor string `toml:"executor"`
	// DataDir a directory where builds and cache are stored. If set to $HOME, each will
	// be stored in the given users home directory, otherwise each will will have their
	// own user-specific workspace below this directory.
	DataDir string `toml:"data_dir"`
	// RetainLogs indicated all logs generated by the underlying system should be kept
	// (default: false, removed upon job completion).
	RetainLogs bool `toml:"retain_logs"`
	// CustomBuildDir allow users to specify their CI_BUILDS_DIR for CI jobs, the runner will
	// ensure unique paths and fail jobs if permissions are invalid.
	CustomBuildDir bool `toml:"custom_build_dir"`
	// KillTimeout represents the maximum timeout the Jacamar-Auth application will wait
	// before sending a SIGKILL to the underlying Jacamar process.
	KillTimeout string `toml:"kill_timeout"`
	// Shell path/application to be used for all command execution by Jacamar and Jacamar-Auth
	// only. Please note that only Bash is supported at this time.
	Shell string `toml:"shell_path"`
	// VariableDataDir indicate that variables defined by the admin within the DataDir are
	// acceptable and will be resolved by the user in a downscoped process.
	VariableDataDir bool `toml:"variable_data_dir"`
}

// Options represent all defined aspects of a potential job that are
// directly obtained through the specified configuration or command line interaction.
type Options struct {
	General General `toml:"general"`
	Batch   Batch   `toml:"batch"`
	Auth    Auth    `toml:"auth"`
}
