package executors

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type executorsTests struct {
	auth        *mock_authuser.MockAuthorized
	cfg         *mock_configure.MockConfigurer
	runner      *mock_runmechanisms.MockRunner
	env         envparser.ExecutorEnv
	scriptPath  string
	execOptions string
	targetPath  string
	stage       string
	buildsDir   string
	targetEnv   map[string]string

	exec mock_executors.MockExecutor

	assertError  func(t *testing.T, err error)
	assertMap    func(t *testing.T, m map[string]string)
	assertString func(t *testing.T, s string)
}

func validAuth(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BuildState().Return(envparser.StatefulEnv{
		Username:  "user",
		BaseDir:   "/base",
		BuildsDir: "/builds",
		CacheDir:  "/cache",
		ScriptDir: "/script",
	}).AnyTimes()
	m.EXPECT().BuildsDir().Return("/builds").AnyTimes()
	m.EXPECT().CacheDir().Return("/cache").AnyTimes()
	m.EXPECT().PrepareNotification().Return("Running as ...\n").AnyTimes()
	return m
}

func Test_printPrepareMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := mock_configure.NewMockConfigurer(ctrl)
	c.EXPECT().General().Return(configure.General{Executor: "test"})

	tests := map[string]executorsTests{
		"building prepare message": {
			auth: validAuth(ctrl),
			cfg:  c,
			assertString: func(t *testing.T, s string) {
				assert.True(
					t,
					strings.HasPrefix(
						s,
						"Targeting test execution\nRunning as ...\nLocal time:",
					),
					"check prefix for message structure",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			a := &AbstractExecutor{
				Auth: tt.auth,
				Cfg:  tt.cfg,
			}
			got := a.printPrepareMessage()

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func sha256File(file string) (string, error) {
	f, err := os.Open(file)
	if err != nil {
		return "", err
	}
	defer f.Close()

	sha := sha256.New()
	if _, err := io.Copy(sha, f); err != nil {
		return "", err
	}
	return hex.EncodeToString(sha.Sum(nil)), nil
}

func Test_relocateJobScript(t *testing.T) {
	dir, _ := ioutil.TempDir(t.TempDir(), "relocateJobScript")

	tests := map[string]executorsTests{
		"prepare_script provided via command line, no changes required": {
			scriptPath: "../../test/testdata/jobscript.bash",
			targetPath: dir + "/prepare_nochange.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
				},
			},
			targetEnv: map[string]string{
				envparser.UserEnvPrefix + "JACAMAR_NO_BASH_PROFILE": "true",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				sum, err := sha256File(s)
				assert.NoError(t, err, "failed to obtain sha256 sum")
				assert.Equal(
					t,
					"7d83836141882826a5417fc3d06da0907d763538b10aa0869eec43c7cff9d39c",
					sum,
				)
			},
		},
		"prepare_script provided via command line, login shell": {
			scriptPath: "../../test/testdata/jobscript.bash",
			targetPath: dir + "/prepare_login.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				sum, err := sha256File(s)
				assert.NoError(t, err, "failed to obtain sha256 sum")
				assert.Equal(
					t,
					"7d83836141882826a5417fc3d06da0907d763538b10aa0869eec43c7cff9d39c",
					sum,
				)
			},
		},
		"invalid script contents provided env": {
			scriptPath: "env-script",
			targetPath: dir + "/prepare_profile.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
				},
			},
			targetEnv: map[string]string{
				envparser.ScriptContentsPrefix + "0": "BASE%%64",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to RetrieveScriptEnv: error decoding script contents, illegal base64 data at input byte 4",
				)
			},
		},
		"invalid source file path provided": {
			scriptPath: "/file/doesnt/exist.bash",
			targetPath: dir + "/prepare_error.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to augment CI JobScript: open /file/doesnt/exist.bash: no such file or directory",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			err := relocateJobScript(tt.scriptPath, tt.targetPath, tt.stage, tt.env)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, tt.targetPath)
			}
		})
	}
}
