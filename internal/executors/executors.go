// Package executors manages global aspects of the CI job before final responsibility
// for execution is handed over to the specified interface.
package executors

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
)

// Executor manages the runnable operations associated with the GitLab Runner's custom-executor.
// Realization of this interfaces requires execution of the implemented stages, further documented
// online (https://docs.gitlab.com/runner/executors/custom.html). Each stage will be called
// at least once, with the Run occurring multiple time for each generated script.
type Executor interface {
	// Prepare implements the runner's associated stage (prepare_exec). This should focus on
	// preparing  the system(s) for running scripts that will be provided in subsequent stages.
	// Some executors will not preform any actions and instead rely on the global preparations.
	Prepare() error
	// Run implements the runner's associated stage (run_exec). It is invoked multiple times, once
	// for  each sub-stage. The sub-stage name and target script for execution can be provided in
	// the executor.JobConfig structure.
	Run() error
	// Cleanup implements the runner's associated stage (cleanup_exec). It will be the final stage
	// executed even if one of the previous stages failed. Some executors will not preform any
	// actions and instead rely on the global cleanup.
	Cleanup() error
}

// AbstractExecutor organizes all aspects of job execution regardless of the target executor
// interface that may ultimately be used and is dependent on Jacamar's configuration. The
// existence of every possible interface is done to ensure that executors, regardless of type,
// are provided with a complete set of tools to realize any requirement.
type AbstractExecutor struct {
	Auth   authuser.Authorized
	Cfg    configure.Configurer
	Env    envparser.ExecutorEnv
	Msg    logging.Messenger
	Runner runmechanisms.Runner
	// ExecOptions contents of the Jacamar configuration, for use outside of authorization.
	ExecOptions string
	// ScriptPath to the generated script during run_exec.
	ScriptPath string
	// Stage name during run_exec currently being executed.
	Stage string
	// SysLog primarily focused on enabling logging during authorization processes.
	SysLog *logrus.Entry
}

// Prepare realizes generic aspects of the prepare_exec custom executor stage prior
// to calling the associated Executor interface.
func (a *AbstractExecutor) Prepare(e Executor) error {
	if err := usertools.CreateDirectories(a.Auth); err != nil {
		return err
	}
	return e.Prepare()
}

// Run realizes generic aspects of the run_exec custom executor stage prior to calling the
// provided Executor.
func (a *AbstractExecutor) Run(e Executor) error {
	tar := fmt.Sprintf("%s/%s.bash", a.Auth.ScriptDir(), a.Stage)
	err := relocateJobScript(a.ScriptPath, tar, a.Stage, a.Env)
	if err != nil {
		return fmt.Errorf("unable to relocateJobScript (%s): %w", a.ScriptPath, err)
	}
	a.ScriptPath = tar

	// Remove job script to avoid storing tokens locally.
	defer os.Remove(a.ScriptPath)

	if a.Stage == "prepare_script" {
		// We use our own more detailed message, thus can ignore the GitLab generated one.
		fmt.Println(a.printPrepareMessage())
	} else if a.Stage == "get_sources" {
		err = augmenter.CreateGitAskpass(a.Env, a.Msg)
		if err != nil {
			return fmt.Errorf("unable to CreateGitAskpass: %w", err)
		}
	}

	return e.Run()
}

// Cleanup realizes generic aspects of the cleanup_exec custom executor stage prior to
// calling the provided Executor.
func (a *AbstractExecutor) Cleanup(e Executor) error {
	defer a.cleanupScriptDir()

	// Remove GitAskPass file if it had been created.
	augmenter.CleanupGitAskpass(a.Env, a.Msg)

	return e.Cleanup()
}

func (a *AbstractExecutor) printPrepareMessage() string {
	var sb strings.Builder

	sb.WriteString("Targeting " + a.Cfg.General().Executor + " execution\n")
	sb.WriteString(a.Auth.PrepareNotification())
	stamp := time.Now().Format("2006-01-02 15:04:05")
	sb.WriteString("Local time: " + stamp + "\n")

	return sb.String()
}

// cleanupScriptDir ensure post job logic for the scheduler created script directory and all files
// found within are properly executed. User defined COPY_SCHEDULER_LOGS is used to determine a new
// target for the directory.
func (a *AbstractExecutor) cleanupScriptDir() {
	scriptDir := a.Auth.ScriptDir()
	if !a.Cfg.General().RetainLogs {
		defer os.RemoveAll(scriptDir)
	}

	// Copying the log file will be managed by the user's shell, all variables are realized.
	copyDest := os.Getenv(envparser.UserEnvPrefix + "COPY_SCHEDULER_LOGS")
	if copyDest != "" {
		if err := os.Chmod(scriptDir, usertools.FilePermissions); err != nil {
			a.Msg.Warn("unable to ensure permissions script directory: %s", err.Error())
		}

		copyDest = filepath.Clean(strings.TrimSpace(copyDest))

		// We rely on executing the command via this method as it allows users
		// to embed environment variables which will then be properly resolved.
		out, err := a.Runner.ReturnOutput(
			fmt.Sprintf("mkdir -p %s && cp -aR %s %s/", copyDest, scriptDir, copyDest),
		)
		if err != nil {
			a.Msg.Warn("error encountered while attempting to copy log directory: %s", out)
		}
	}
}

// relocateJobScript ensures the proper relocations of a job script from the source path
// provided via command line arguments to the target. The current run_exec stage along with
// the established builds_dir must be provided.
func relocateJobScript(srcPath, tarPath, stage string, env envparser.ExecutorEnv) (err error) {
	var contents string

	if srcPath == "env-script" {
		contents, err = envparser.RetrieveScriptEnv()
		if err != nil {
			return fmt.Errorf("unable to RetrieveScriptEnv: %w", err)
		}
	} else {
		rules := augmenter.Rules{} // Default rules only
		contents, err = rules.JobScript(
			srcPath,
			stage,
			env,
		)
		if err != nil {
			return fmt.Errorf("unable to augment CI JobScript: %w", err)
		}
	}

	return finalizeJobScript(contents, tarPath)
}

// finalizeJobScript prepares a runner generated script for usage with any executor, ensuring the
// script will result in the appropriate shell settings. Script is then written to target file.
func finalizeJobScript(contents, target string) error {
	return ioutil.WriteFile(target, []byte(contents), usertools.FilePermissions)
}
