package slurm

import (
	"errors"
	"fmt"
	"os/exec"
	"path/filepath"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/batch"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
)

type executor struct {
	absExec *executors.AbstractExecutor

	// batch submission only variables
	mng     batch.Manager
	jobName string
	outFile string
}

func (e *executor) Prepare() error {
	_, err := exec.LookPath(
		batch.PrefixSchedulerPath("sbatch", e.absExec.Cfg.Batch().SchedulerBin),
	)
	if err != nil {
		return errors.New("unable to locate Slurm (sbatch) in the CI environment")
	}

	return nil
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}

	// Job script to be submitted via sbatch, needs updated.
	if err := augmenter.LoginShellScript(e.absExec.ScriptPath); err != nil {
		return fmt.Errorf("failed to updated Slurm job script: %w", err)
	}

	return e.runSlurm()
}

func (e *executor) Cleanup() error {
	// No slurm specific cleanup required.
	return nil
}

func (e *executor) runSlurm() error {
	// Pre-create output file to avoid issues with jobs stuck in queue.
	batch.CreateFiles([]string{e.outFile}, e.absExec.Msg)

	var wg sync.WaitGroup
	wg.Add(2)

	cmdErr := make(chan error, 1)
	defer close(cmdErr)
	go func() {
		defer wg.Done()
		e.submitJob(cmdErr)
	}()

	jobDone := make(chan struct{}, 1)
	defer close(jobDone)
	go func() {
		defer wg.Done()
		e.monitorJob(jobDone)
	}()

	err := <-cmdErr
	jobDone <- struct{}{}

	wg.Wait()

	return err
}

func (e *executor) submitJob(cmdErr chan error) {
	sbatchStdin := fmt.Sprintf(
		"%s --wait --job-name=%s --output=%s %s",
		e.mng.BatchCmd(),
		e.jobName,
		e.outFile,
		e.mng.UserArgs(),
	)

	err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath, sbatchStdin)
	cmdErr <- err
}

func (e *executor) monitorJob(jobDone chan struct{}) {
	stopArg := fmt.Sprintf("--name=%s", e.jobName)

	var wg sync.WaitGroup
	wg.Add(1) // outfile

	stopTail := make(chan struct{}, 1)
	defer close(stopTail)
	go func() {
		defer wg.Done()
		// We generate output file in previous step from local host, add arbitrary short timeout.
		err := e.mng.TailFiles([]string{e.outFile}, stopTail, 10*time.Second, e.absExec.Msg)
		if err != nil {
			e.absExec.Msg.Warn("Unable to monitor output file (%s): %s", e.outFile, err.Error())
			e.absExec.Msg.Stdout("Jacamar will attempt to cancel job (%s)", e.jobName)
			command.NoOutputCmd("scancel", stopArg)
		}
	}()

	e.mng.MonitorTermination(e.absExec.Runner.RequestContext(), jobDone, stopArg)
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	stopTail <- struct{}{}
	wg.Wait()
}

// NewExecutor generates a valid Slurm executor that fulfills the executors.Executor interface.
func NewExecutor(ae *executors.AbstractExecutor) *executor {
	e := &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd: "sbatch",
			StopCmd:  "scancel",
			IllegalArgs: []string{
				"-o",
				"--output",
				"-e",
				"--error",
				"-J",
				"--job-name",
				"-W",
				"--wait",
				"--wait-all-nodes",
			},
		}

		e.jobName = fmt.Sprintf(
			"ci-%s_%d",
			ae.Env.RequiredEnv.JobID,
			time.Now().Unix(),
		)
		e.mng = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		e.outFile = filepath.Clean(fmt.Sprintf(
			"%s/slurm-ci-%s.out",
			ae.Auth.ScriptDir(),
			ae.Env.RequiredEnv.JobID,
		))
	}

	return e
}
