package slurm

import (
	"errors"
	"path/filepath"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/batch"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

// We should mainly rely on Pavilion + Facility/Docker to realize more comprehensive
// testing against Slurm. Use these tests to ensure we properly handle errors, goroutines,
// and channel communications where possible.

func Test_executor_Prepare(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	miss := mock_configure.NewMockConfigurer(ctrl)
	miss.EXPECT().Batch().Return(configure.Batch{})

	path, _ := filepath.Abs("../../../test/scripts/schedulers")
	found := mock_configure.NewMockConfigurer(ctrl)
	found.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin: path,
	})

	tests := map[string]struct {
		cfg         configure.Configurer
		assertError func(*testing.T, error)
	}{
		"sbatch identified": {
			cfg: found,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"sbatch not found": {
			cfg: miss,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to locate Slurm (sbatch) in the CI environment")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: &executors.AbstractExecutor{
					Cfg:   tt.cfg,
					Stage: "prepare_exec",
				},
			}
			err := e.Prepare()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().JobScriptOutput("/working/after_script.bash").Return(nil)
	run.EXPECT().JobScriptOutput("/failing/get_sources.bash").Return(errors.New("error message"))

	type fields struct {
		absExec *executors.AbstractExecutor
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"working after_script launched locally": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Runner:     run,
					ScriptPath: "/working/after_script.bash",
					Stage:      "after_script",
				},
			},
			assertError: tst.AssertNoError,
		},
		"failing get_sources launched locally": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Runner:     run,
					ScriptPath: "/failing/get_sources.bash",
					Stage:      "get_sources",
				},
			},
			assertError: tst.AssertError,
		},
		"non-existent script path defined": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					ScriptPath: "/file/does/not/exist.bash",
					Stage:      "step_script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to updated Slurm job script: open /file/does/not/exist.bash: no such file or directory")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
			}

			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_Cleanup(t *testing.T) {
	err := (&executor{}).Cleanup()
	assert.Nil(t, err, "Cleanup successfully skipped")
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tempDir := t.TempDir()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout: "5s",
	})

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("No %s variable detected, please check your CI job if this is unexpected.", "SCHEDULER_PARAMETERS")

	auth := mock_authuser.NewMockAuthorized(ctrl)
	auth.EXPECT().ScriptDir().Return(tempDir)

	tests := map[string]struct {
		ae             *executors.AbstractExecutor
		assertExecutor func(*testing.T, *executor)
	}{
		// We don't need to test for many failure states as we can
		// rely that the data provided has already been parsed.
		"basic Slurm executor created for non step_script stage": {
			ae: &executors.AbstractExecutor{
				Stage: "after_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, e.absExec.Stage, "after_script")
			},
		},
		"build Slurm executor for step_script": {
			ae: &executors.AbstractExecutor{
				Auth: auth,
				Cfg:  cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.NotNil(t, e.mng)
				assert.Contains(t, e.jobName, "ci-101_")
				assert.Equal(t, tempDir+"/slurm-ci-101.out", e.outFile)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := NewExecutor(tt.ae)

			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}

func Test_executor_submitJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().JobScriptOutput(
		"pass.bash",
		"sbatch --wait --job-name=pass --output=/tmp/pass.log --test",
	).Return(nil)
	run.EXPECT().JobScriptOutput(
		"fail.bash",
		gomock.Any(),
	).Return(errors.New("sbatch error"))

	mng := mock_batch.NewMockManager(ctrl)
	mng.EXPECT().BatchCmd().Return("sbatch").Times(2)
	mng.EXPECT().UserArgs().Return("--test").Times(2)

	type fields struct {
		absExec *executors.AbstractExecutor
		mng     batch.Manager
		jobName string
		outFile string
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"sbatch ran without error": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Runner:     run,
					ScriptPath: "pass.bash",
				},
				mng:     mng,
				jobName: "pass",
				outFile: "/tmp/pass.log",
			},
			assertError: tst.AssertNoError,
		},
		"sbatch encountered error": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Runner:     run,
					ScriptPath: "fail.bash",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "sbatch error")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
				mng:     tt.fields.mng,
				jobName: tt.fields.jobName,
				outFile: tt.fields.outFile,
			}

			cmdErr := make(chan error, 1)
			defer close(cmdErr)

			e.submitJob(cmdErr)

			if tt.assertError != nil {
				tt.assertError(t, <-cmdErr)
			}
		})
	}
}

func Test_executor_monitorJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout: "5s",
	})

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().RequestContext().Return(nil).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("Unable to monitor output file (%s): %s", gomock.Any(), gomock.Any())
	msg.EXPECT().Stdout("Jacamar will attempt to cancel job (%s)", gomock.Any())

	taiError := mock_batch.NewMockManager(ctrl)
	taiError.EXPECT().TailFiles(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("tail error"))
	taiError.EXPECT().MonitorTermination(nil, nil, "--name=").Return()
	taiError.EXPECT().NFSTimeout(gomock.Eq("5s"), gomock.Any())

	t.Run("job completed, channel closed", func(t *testing.T) {
		e := &executor{
			absExec: &executors.AbstractExecutor{
				Cfg:    cfg,
				Msg:    msg,
				Runner: run,
			},
			mng: taiError,
		}

		e.monitorJob(nil)
	})
}
