package cobalt

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

// qsubJobID parses the output from a successful qsub command in order
// to identify the jobID. Verifies jobID by confirming integer conversion.
func qsubJobID(out string) (string, error) {
	out = strings.TrimSpace(out)
	lines := strings.Split(out, "\n")
	id := lines[len(lines)-1]
	if _, err := strconv.Atoi(id); err != nil {
		return "", err
	}
	return id, nil
}

// exitStatus checks the (jobID).cobaltlog file for a
// string associated with a successful job.
func exitStatus(log string) (es string, err error) {
	file, err := os.Open(filepath.Clean(log))
	if err != nil {
		return "", fmt.Errorf("unable to open CobaltLog: %w", err)
	}

	/* #nosec */
	// file is not written too
	defer file.Close()

	sc := bufio.NewScanner(file)
	for sc.Scan() {
		line := sc.Text()
		if strings.Contains(line, "task completed normally with an exit code of 0") {
			return "0", nil
		}
	}
	if err := sc.Err(); err != nil {
		return "", fmt.Errorf("scanner error: %w", err)
	}

	return "1", errors.New("unable to find successful exit code in CobaltLog")
}

func summarizeErrors(log, id string, msg logging.Messenger) {
	if envparser.TrueEnvVar(summaryVar) {
		log, _ = filepath.Abs(log)
		/* #nosec */
		// variable file path required
		data, err := ioutil.ReadFile(log)
		if err != nil {
			msg.Warn(strings.Join([]string{"Unable to read", log, "for error summary", err.Error()}, " "))
		}

		if len(data) != 0 {
			msg.Notify(strings.Join([]string{"Cobalt job (", id, ") error summary:"}, " "))
			msg.Stderr(string(data))
		}
	}
}
