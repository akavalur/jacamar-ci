package cobalt

import (
	"errors"
	"fmt"
	"os/exec"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/batch"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
)

const (
	summaryVar = envparser.UserEnvPrefix + "COBALT_SUMMARIZE_ERRORS"
)

type executor struct {
	absExec   *executors.AbstractExecutor
	mng       batch.Manager
	outLog    string
	errLog    string
	logFile   string
	jobID     string
	sleepTime time.Duration // Sleep between scheduler integrations.
}

func (e *executor) Prepare() error {
	_, err := exec.LookPath(
		batch.PrefixSchedulerPath("qsub", e.absExec.Cfg.Batch().SchedulerBin),
	)
	if err != nil {
		return errors.New("unable to locate Cobalt (qsub) in the CI environment")
	}

	return nil
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}

	// Modify command and create login script for submission to qsub,
	// to account for limited environment.
	e.absExec.Runner.ModifyCmd(command.IdentifyShell(e.absExec.Cfg.General()))
	if err := augmenter.LoginShellScript(e.absExec.ScriptPath); err != nil {
		return fmt.Errorf("failed to updated Cobalt job script: %w", err)
	}

	return e.runCobalt()
}

func (e *executor) Cleanup() error {
	// No cobalt specific cleanup required.
	return nil
}

func (e *executor) runCobalt() error {
	if err := e.submitJob(); err != nil {
		return fmt.Errorf(
			"error while attempting to submit job to Cobalt: %w", err,
		)
	}

	// Wait for configured NFS timeout window.
	defer e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	if err := e.monitorJob(); err != nil {
		return fmt.Errorf(
			"error while attempting to monitor job (%s): %w", e.jobID, err,
		)
	}
	return nil
}

func (e *executor) submitJob() error {
	qsubStdin := fmt.Sprintf("%s %s", e.mng.BatchCmd(), e.mng.UserArgs())
	out, err := e.absExec.Runner.JobScriptReturn(e.absExec.ScriptPath, qsubStdin)
	if err != nil {
		return fmt.Errorf("failed to request allocation (%s): %s", qsubStdin, out)
	}

	jobID, err := qsubJobID(out)
	if err != nil {
		return fmt.Errorf("failed to identify jobid from qsub stdout: %s", out)
	}
	e.jobID = jobID
	fmt.Printf("Submitted batch job %s\n", e.jobID)

	// generate expected output/error files
	e.outLog = fmt.Sprintf("%s/%s.output", e.absExec.Auth.ScriptDir(), e.jobID)
	e.errLog = fmt.Sprintf("%s/%s.error", e.absExec.Auth.ScriptDir(), e.jobID)
	batch.CreateFiles([]string{e.outLog, e.errLog}, e.absExec.Msg)

	e.logFile = fmt.Sprintf("%s/%s.cobaltlog", e.absExec.Auth.ScriptDir(), e.jobID)

	return nil
}

// monitorJob examines the job's output logs and status throughout duration.
func (e *executor) monitorJob() error {
	var wg sync.WaitGroup
	wg.Add(2) // output files

	jobDone := make(chan struct{})
	go func() {
		defer wg.Done()
		err := e.mng.TailFiles(
			[]string{e.outLog, e.errLog}, jobDone, 45*time.Second, e.absExec.Msg,
		)
		if err != nil {
			e.absExec.Msg.Warn(
				"Unable to monitor files, job output distrusted: %s", err.Error(),
			)
		}
	}()

	go func() {
		defer wg.Done()
		e.mng.MonitorTermination(e.absExec.Runner.RequestContext(), jobDone, e.jobID)
	}()

	qstatCmd := fmt.Sprintf("%s %s", e.mng.StateCmd(), e.jobID)
	for {
		_, err := e.absExec.Runner.ReturnOutput(qstatCmd)
		if err != nil {
			break
		}
	}

	close(jobDone)
	wg.Wait()

	summarizeErrors(e.errLog, e.jobID, e.absExec.Msg)
	es, err := exitStatus(e.logFile)
	if err != nil {
		return fmt.Errorf("unable to identify job's final exit status (): %w", err)
	}
	fmt.Println(es)

	return nil
}

// NewExecutor generates a valid Slurm executor that fulfills the executors.Executor interface.
func NewExecutor(ae *executors.AbstractExecutor) (e *executor) {
	e = &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd:    "qsub",
			StateCmd:    "qstat",
			StopCmd:     "qdel",
			IllegalArgs: []string{"-o", "--output", "-e", "--error", "--debuglog"},
		}
		e.mng = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)

		e.sleepTime = batch.DefaultSleep
	}

	return
}
