package cobalt

import (
	"path/filepath"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

type cobaltTests struct {
	files []string
	log   string

	mng *mock_batch.MockManager // batch
	run *mock_runmechanisms.MockRunner
	msg *mock_logging.MockMessenger

	prepare func(t *testing.T, s string)

	assertError func(t *testing.T, err error)
	assertSlice func(t *testing.T, s []string)
}

func Test_executor_Prepare(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	miss := mock_configure.NewMockConfigurer(ctrl)
	miss.EXPECT().Batch().Return(configure.Batch{})

	path, _ := filepath.Abs("../../../test/scripts/schedulers")
	found := mock_configure.NewMockConfigurer(ctrl)
	found.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin: path,
	})

	tests := map[string]struct {
		cfg         configure.Configurer
		assertError func(*testing.T, error)
	}{
		"qsub identified": {
			cfg: found,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"qsub not found": {
			cfg: miss,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to locate Cobalt (qsub) in the CI environment")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: &executors.AbstractExecutor{
					Cfg:   tt.cfg,
					Stage: "prepare_exec",
				},
			}
			err := e.Prepare()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_Cleanup(t *testing.T) {
	e := NewExecutor(&executors.AbstractExecutor{})
	err := e.Cleanup()
	assert.Nil(t, err, "Cleanup successfully skipped")
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{}).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("No %s variable detected, please check your CI job if this is unexpected.", "SCHEDULER_PARAMETERS")

	tests := map[string]struct {
		ae             *executors.AbstractExecutor
		assertExecutor func(*testing.T, *executor)
	}{
		"non-step_script, minimal executor scope": {
			ae: &executors.AbstractExecutor{
				Stage: "get_sources",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Equal(t, *e.absExec, executors.AbstractExecutor{
					Stage: "get_sources",
				})
			},
		},
		"step_script batch executor prepared": {
			ae: &executors.AbstractExecutor{
				Cfg:   cfg,
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e.mng)
				assert.Equal(t, "qsub", e.mng.BatchCmd())
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := NewExecutor(tt.ae)

			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	bashRun := mock_runmechanisms.NewMockRunner(ctrl)
	bashRun.EXPECT().JobScriptOutput("/dir/after_script.bash").Return(nil)

	modifyCmd := mock_runmechanisms.NewMockRunner(ctrl)
	modifyCmd.EXPECT().ModifyCmd(command.IdentifyShell(configure.General{
		Shell: "/bin/bash",
	}))

	binCfg := mock_configure.NewMockConfigurer(ctrl)
	binCfg.EXPECT().General().Return(configure.General{
		Shell: "/bin/bash",
	})

	type fields struct {
		absExec *executors.AbstractExecutor
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"non-step_script, minimal executor scope": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Runner:     bashRun,
					Stage:      "after_script",
					ScriptPath: "/dir/after_script.bash",
				},
			},
			assertError: tst.AssertNoError,
		},
		"command modified with ": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Cfg:        binCfg,
					Runner:     modifyCmd,
					Stage:      "step_script",
					ScriptPath: "/dir/job_script.bash",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to updated Cobalt job script: open /dir/job_script.bash: no such file or directory")
			},
		},
		// Testing the entirety of Cobalt requires extensive mocking, we'll
		// explore better ways to verify interactions.
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
			}

			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
