package cobalt

import (
	"io/ioutil"
	"log"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func Test_qsubJobID(t *testing.T) {
	type args struct {
		out string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"JobID only", args{"1234"}, "1234", false},
		{"Invalid JobID", args{"abc"}, "", true},
		{"Build queue", args{"Job routed to queue \"build\".\n5678"}, "5678", false},
		{"Empty string", args{""}, "", true},
		{"Return", args{"90\n"}, "90", false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := qsubJobID(tt.args.out)
			if (err != nil) != tt.wantErr {
				t.Errorf("qsubJobID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("qsubJobID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_exitStatus(t *testing.T) {
	tests := map[string]cobaltTests{
		"no log specified": {
			log: "",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"invalid file specified": {
			log: "/not/a/file",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"terminated job in CobaltLog": {
			log: "/tmp/cobaltlog",
			assertError: func(t *testing.T, err error) {
				assert.Equal(t, "unable to find successful exit code in CobaltLog", err.Error())
			},
			prepare: func(t *testing.T, s string) {
				f, _ := os.OpenFile(s, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
				f.WriteString(`Jobid: 456
qsub -A account test.bash
Thu Jan 1 12:00:00 2020 +0000 (UTC) submitted with cwd set to: /some/dir
jobid 456 submitted from terminal /dev/pts/0
Thu Jan 1 12:00:00 2020 +0000 (UTC) Info: maximum execution time exceeded; initiating job termination`)
				f.Close()
			},
		},
		"successful job in CobaltLog": {
			log: "/tmp/cobaltlog",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			prepare: func(t *testing.T, s string) {
				f, _ := os.OpenFile(s, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
				f.WriteString(`Jobid: 123
qsub -A account test.bash
Thu Jan 1 12:00:00 2020 +0000 (UTC) submitted with cwd set to: /some/dir
jobid 123 submitted from terminal /dev/pts/0
Thu Jan 1 12:00:00 2020 +0000 (UTC) Info: task completed normally with an exit code of 0; initiating job cleanup and removal`)
				f.Close()
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.log != "" && tt.log != "/not/a/file" {
				file, err := ioutil.TempFile("/tmp", "cobaltlog")
				assert.NoError(t, err, "failed to create test CobaltLog file")
				tt.log = file.Name()
				//defer os.Remove(tt.log)
			}

			if tt.prepare != nil {
				tt.prepare(t, tt.log)
			}

			_, err := exitStatus(tt.log)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_summarizeErrors(t *testing.T) {
	emptyLog, err := ioutil.TempFile(os.TempDir(), "*.error")
	if err != nil {
		log.Fatal("failed to create temp file", err)
	}
	defer os.Remove(emptyLog.Name())

	errorLog, err := ioutil.TempFile(os.TempDir(), "*.error")
	if err != nil {
		log.Fatal("failed to create temp file", err)
	}
	defer os.Remove(errorLog.Name())
	errorLog.Write([]byte("cobalt error...\nanother error..."))

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn("Unable to read /no/file/found/error.log for error summary open /no/file/found/error.log: no such file or directory").Times(1)
	m.EXPECT().Notify("Cobalt job ( 123 ) error summary:").Times(1)
	m.EXPECT().Stderr("cobalt error...\nanother error...").Times(1)

	tests := []struct {
		name string
		log  string
		id   string
		env  string
		msg  logging.Messenger
	}{
		{
			name: "no error log file found, env true",
			log:  "/no/file/found/error.log",
			env:  "true",
			msg:  m,
		}, {
			name: "empty error log, env true",
			log:  emptyLog.Name(),
			env:  "1",
			msg:  m,
		}, {
			name: "defined error log, env false",
			log:  errorLog.Name(),
			msg:  m,
		}, {
			name: "defined error log, env true",
			id:   "123",
			log:  errorLog.Name(),
			env:  "true",
			msg:  m,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.env != "" {
				os.Setenv(summaryVar, tt.env)
				defer os.Unsetenv(summaryVar)
			}

			summarizeErrors(tt.log, tt.id, tt.msg)
		})
	}
}
