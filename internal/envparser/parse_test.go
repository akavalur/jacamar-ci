package envparser

import (
	"testing"

	"github.com/stretchr/testify/assert"

	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type envTests struct {
	targetEnv   map[string]string
	stateReq    bool
	contents    string
	maxEnvChars int

	assertExecutorEnv func(*testing.T, ExecutorEnv)
	assertRequiredEnv func(*testing.T, *RequiredEnv)
	assertExitStatus  func(*testing.T, int, int)
	assertError       func(*testing.T, error)
	assertString      func(*testing.T, string)
	assertSlice       func(*testing.T, []string)
	assertBoolean     func(*testing.T, bool)
}

func TestExitCodes(t *testing.T) {
	tests := map[string]envTests{
		"No exit status variables defined in environment": {
			assertExitStatus: func(t *testing.T, build, sys int) {
				assert.Equal(t, 1, build, "Default build failure exit code expected")
				assert.Equal(t, 2, sys, "Default system failure exit code expected")
			},
		},
		"Missing build exit code failure": {
			targetEnv: map[string]string{"SYSTEM_FAILURE_EXIT_CODE": "22"},
			assertExitStatus: func(t *testing.T, build, sys int) {
				assert.Equal(t, 1, build, "Default build failure exit code expected")
				assert.Equal(t, 22, sys)
			},
		},
		"Working environment, both build & system codes defined": {
			targetEnv: map[string]string{
				"BUILD_FAILURE_EXIT_CODE":  "11",
				"SYSTEM_FAILURE_EXIT_CODE": "22",
			},
			assertExitStatus: func(t *testing.T, build, sys int) {
				assert.Equal(t, 11, build)
				assert.Equal(t, 22, sys)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			sys, build := ExitCodes()

			if tt.assertExitStatus != nil {
				tt.assertExitStatus(t, build, sys)
			}
		})
	}
}

func TestFetcher(t *testing.T) {
	tests := map[string]envTests{
		"Complete test environment provided (stateful + required variables)": {
			stateReq: true,
			targetEnv: map[string]string{
				"TRUSTED_CI_CONCURRENT_PROJECT_ID":      "0",
				"TRUSTED_CI_RUNNER_SHORT_TOKEN":         "abc",
				"TRUSTED_CI_JOB_ID":                     "123",
				"TRUSTED_CI_JOB_TOKEN":                  "JoBt0k3n",
				"TRUSTED_CI_BUILDS_DIR":                 "/gitlab/builds",
				"TRUSTED_CI_CACHE_DIR":                  "/gitlab/cache",
				"TRUSTED_CI_SERVER_URL":                 "gitlab.url",
				UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				UserEnvPrefix + "CI_JOB_JWT":            "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
				StatefulEnvPrefix + "AUTH_USERNAME":     "tester",
				StatefulEnvPrefix + "BASE_DIR":          "/ci",
				StatefulEnvPrefix + "BUILDS_DIR":        "/ci/builds",
				StatefulEnvPrefix + "CACHE_DIR":         "/ci/cache",
				StatefulEnvPrefix + "SCRIPT_DIR":        "/ci/script",
				StatefulEnvPrefix + "PROJECT_PATH":      "group/project",
				"BUILD_FAILURE_EXIT_CODE":               "1",
				"TEST_PATH":                             "/usr/bin:/usr/sbin",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExecutorEnv: func(t *testing.T, env ExecutorEnv) {
				expected := ExecutorEnv{
					RequiredEnv: RequiredEnv{
						ConcurrentID: "0",
						JobID:        "123",
						JobToken:     "JoBt0k3n",
						RunnerShort:  "abc",
						ServerURL:    "gitlab.url",
						CIJobJWT:     "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
					},
					StatefulEnv: StatefulEnv{
						Username:    "tester",
						BaseDir:     "/ci",
						BuildsDir:   "/ci/builds",
						CacheDir:    "/ci/cache",
						ScriptDir:   "/ci/script",
						ProjectPath: "group/project",
					},
				}
				assert.Equal(t, expected, env)
			},
		},
		"Missing variables in CI environment": {
			stateReq:  false,
			targetEnv: map[string]string{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"missing required variable TRUSTED_CI_CONCURRENT_PROJECT_ID, "+errSuffix,
				)
			},
			assertExecutorEnv: func(t *testing.T, env ExecutorEnv) {
				assert.Equal(t, ExecutorEnv{}, env)
			},
		},
		"Missing stateful variables in CI environment": {
			stateReq: true,
			targetEnv: map[string]string{
				"TRUSTED_CI_CONCURRENT_PROJECT_ID":      "1",
				"TRUSTED_CI_JOB_ID":                     "123",
				"TRUSTED_CI_JOB_TOKEN":                  "JoBt0k3n",
				"TRUSTED_CI_RUNNER_SHORT_TOKEN":         "abc",
				"TRUSTED_CI_SERVER_URL":                 "gitlab.url",
				"TRUSTED_CI_BUILDS_DIR":                 "/ci/builds",
				"TRUSTED_CI_CACHE_DIR":                  "/ci/cache",
				UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				UserEnvPrefix + "CI_JOB_JWT":            "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
				"TEST_PATH":                             "/usr/bin:/usr/sbin",
				"BUILD_FAILURE_EXIT_CODE":               "1",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"missing stateful variable JACAMAR_CI_BASE_DIR, "+errSuffix,
				)
			},
		},
		"invalid CI_JOB_JWT specified": {
			stateReq: true,
			targetEnv: map[string]string{
				"TRUSTED_CI_CONCURRENT_PROJECT_ID":      "1",
				"TRUSTED_CI_JOB_ID":                     "123",
				"TRUSTED_CI_JOB_TOKEN":                  "JoBt0k3n",
				"TRUSTED_CI_RUNNER_SHORT_TOKEN":         "abc",
				"TRUSTED_CI_SERVER_URL":                 "gitlab.url",
				"TRUSTED_CI_BUILDS_DIR":                 "/ci/builds",
				"TRUSTED_CI_CACHE_DIR":                  "/ci/cache",
				UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				UserEnvPrefix + "CI_JOB_JWT":            "eyJ:)y.eyJoZWx.sm0r7FO",
				"TEST_PATH":                             "/usr/bin:/usr/sbin",
				"BUILD_FAILURE_EXIT_CODE":               "1",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"non-standard CI_JOB_JWT detected in environment, do not change this variable",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			env, err := Fetcher(tt.stateReq)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutorEnv != nil {
				tt.assertExecutorEnv(t, env)
			}
		})
	}
}

func TestRequiredEnv_keyVarMapping(t *testing.T) {
	tests := map[string]envTests{
		"Missing variables in the provided environment": {
			targetEnv: map[string]string{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"missing required variable TRUSTED_CI_CONCURRENT_PROJECT_ID, "+errSuffix)
			},
		},
		"All required variables provided": {
			targetEnv: map[string]string{
				"TRUSTED_CI_CONCURRENT_PROJECT_ID":     "1",
				"TRUSTED_CI_JOB_ID":                    "123",
				"TRUSTED_CI_JOB_TOKEN":                 "JoBt0k3n",
				"TRUSTED_CI_RUNNER_SHORT_TOKEN":        "abc",
				"TRUSTED_CI_BUILDS_DIR":                "/gitlab/builds",
				"TRUSTED_CI_CACHE_DIR":                 "/gitlab/cache",
				UserEnvPrefix + "CI_PROJECT_PATH_SLUG": "slug/test",
				UserEnvPrefix + "GITLAB_USER_LOGIN":    "test",
				UserEnvPrefix + "CI_JOB_JWT":           "",
				"TRUSTED_CI_SERVER_URL":                "gitlab.url",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRequiredEnv: func(t *testing.T, r *RequiredEnv) {
				expected := RequiredEnv{
					ConcurrentID: "1",
					JobID:        "123",
					JobToken:     "JoBt0k3n",
					RunnerShort:  "abc",
					ServerURL:    "gitlab.url",
					CIJobJWT:     "",
				}
				assert.Equal(t, expected, *r)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			r := &RequiredEnv{}
			err := r.keyVarMapping(tt.targetEnv)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRequiredEnv != nil {
				tt.assertRequiredEnv(t, r)
			}
		})
	}
}

func TestEstablishScriptEnv(t *testing.T) {
	tests := map[string]envTests{
		"empty contents provided": {
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 0)
			},
		},
		"zero maxEnvChars provided (should not occur due to defaults)": {
			contents: "script",
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 0)
			},
		},
		"single element return expected": {
			contents:    "script",
			maxEnvChars: 100,
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 1)
				assert.Equal(t, []string{
					ScriptContentsPrefix + "0=c2NyaXB0",
				}, s)
			},
		},
		"multi-element return expected": {
			contents:    "ABCD",
			maxEnvChars: 1,
			assertSlice: func(t *testing.T, s []string) {
				assert.Equal(t, []string{
					ScriptContentsPrefix + "0=Q", ScriptContentsPrefix + "1=U",
					ScriptContentsPrefix + "2=J", ScriptContentsPrefix + "3=D",
					ScriptContentsPrefix + "4=R", ScriptContentsPrefix + "5=A",
					ScriptContentsPrefix + "6==", ScriptContentsPrefix + "7==",
				}, s)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := EstablishScriptEnv(tt.contents, tt.maxEnvChars)

			if tt.assertSlice != nil {
				tt.assertSlice(t, got)
			}
		})
	}
}

func TestRetrieveScriptEnv(t *testing.T) {
	tests := map[string]envTests{
		"no environment variables established, error": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to retrieve script from environment")
			},
		},
		"single ScriptContentsPrefix defined": {
			targetEnv: map[string]string{
				ScriptContentsPrefix + "0": "c2NyaXB0",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "script", s)
			},
		},
		"multiple ScriptContentsPrefix defined": {
			targetEnv: map[string]string{
				ScriptContentsPrefix + "0": "QU",
				ScriptContentsPrefix + "1": "JD",
				ScriptContentsPrefix + "2": "RA",
				ScriptContentsPrefix + "3": "==",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "ABCD", s)
			},
		},
		"bad encoding found": {
			targetEnv: map[string]string{
				ScriptContentsPrefix + "0": "c2Nya%%0",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"error decoding script contents, illegal base64 data at input byte 5",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got, err := RetrieveScriptEnv()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}
