package envparser

import (
	"errors"
	"os"
	"regexp"
	"strings"
)

// TrustJobToken returns the CI Job Token as provided by the runner.
func TrustJobToken() string {
	return os.Getenv("TRUSTED_CI_JOB_TOKEN")
}

// GitTrace if Git's tracing/debug is expected.
func GitTrace() bool {
	return TrueEnvVar(UserEnvPrefix + "GIT_TRACE")
}

// TrueEnvVar check if variable 'true' or '1'.
func TrueEnvVar(key string) bool {
	val, present := os.LookupEnv(key)
	if present {
		val = strings.ToLower(strings.TrimSpace(val))
		if val == "true" || val == "1" {
			return true
		}
	}

	return false
}

// verifyJWTFormat checks the encoded JWT against regular expression and
// returns if conditions meet.
func verifyJWTFormat() (string, error) {
	encoded, found := os.LookupEnv(UserEnvPrefix + "CI_JOB_JWT")
	// JWT is not required in all cases, when required and not present
	// it'll fail later with a more helpful error message.
	if found {
		re := regexp.MustCompile(`^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]*$`)
		if !re.MatchString(encoded) {
			return "", errors.New(
				"non-standard CI_JOB_JWT detected in environment, do not change this variable",
			)
		}
	}
	return encoded, nil
}
