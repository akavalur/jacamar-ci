package envparser

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func TestTrustJobToken(t *testing.T) {
	t.Run("verify CI_JOB_TOKEN retrieved", func(t *testing.T) {
		os.Setenv("TRUSTED_CI_JOB_TOKEN", "T0k3n")
		defer os.Unsetenv("TRUSTED_CI_JOB_TOKEN")

		got := TrustJobToken()
		assert.Equal(t, "T0k3n", got)
	})
}

func TestTrueEnvVar(t *testing.T) {
	tests := map[string]envTests{
		"Undefined TEST_TRUE_ENV_VAR env": {
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
		"Defined true TEST_TRUE_ENV_VAR env": {
			targetEnv: map[string]string{
				UserEnvPrefix + "TEST_TRUE_ENV_VAR": "true",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
		"Defined 1 TEST_TRUE_ENV_VAR env": {
			targetEnv: map[string]string{
				UserEnvPrefix + "TEST_TRUE_ENV_VAR": "1",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
		"Defined false TEST_TRUE_ENV_VAR env": {
			targetEnv: map[string]string{
				UserEnvPrefix + "TEST_TRUE_ENV_VAR": "false",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got := TrueEnvVar(UserEnvPrefix + "TEST_TRUE_ENV_VAR")

			if tt.assertBoolean != nil {
				tt.assertBoolean(t, got)
			}
		})
	}
}

func TestGitTrace(t *testing.T) {
	tests := map[string]envTests{
		"Undefined GIT_TRACE env": {
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
		"Defined true GIT_TRACE env": {
			targetEnv: map[string]string{
				UserEnvPrefix + "GIT_TRACE": "true",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got := GitTrace()

			if tt.assertBoolean != nil {
				tt.assertBoolean(t, got)
			}
		})
	}
}
