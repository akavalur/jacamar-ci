// Package envparser manages the analyzing and retrieving of the current application's environment
// in order to obtain context proved by the GitLab Runner's custom executor. The ideal scenario
// involves fetching the ExecutorEnv structure as early as possible during the job's preparation
// phase. Any missing required variables can lead to unexpected errors that should be avoided by
// strictly observing any error raised as part of this package.
package envparser

import (
	"encoding/base64"
	"errors"
	"fmt"
	"math"
	"os"
	"reflect"
	"strconv"
	"strings"
)

const (
	// UserEnvPrefix is the expected runner defined prefix for all environment variables
	// provided to the custom executor driver.
	UserEnvPrefix = "CUSTOM_ENV_"
	// StatefulEnvPrefix is the required application defined prefix for stateful (e.g. job)
	// environment variables.
	StatefulEnvPrefix = "JACAMAR_CI_"
	// UserVarTagName defines the structure tag used to identify the associated environment
	// variable set by the runner.
	UserVarTagName = "key"
	// RequiredKey defines the structure tag indicating if an environment variable is
	// required. This should be observed, jobs without required variables must be failed.
	RequiredKey = "required"
	// SysCodePrefix is the environment variable key for the preferred system failure exit status.
	SysCodePrefix = "SYSTEM_FAILURE_EXIT_CODE"
	// BuildCodePrefix is the environment variable key for the preferred build failure exit status.
	BuildCodePrefix = "BUILD_FAILURE_EXIT_CODE"
	// ScriptContentsPrefix is the prefix for all environment variables set with encoded contents.
	ScriptContentsPrefix = "JACAMAR_SCRIPT_CONTENTS_"
	errSuffix            = "error encountered, verify GitLab-Runner meets Jacamar requirements"
)

// RequiredEnv identifies and separate required variables from the CustomEnv
// provided by the executor for easy retrieval.
type RequiredEnv struct {
	ConcurrentID string `key:"TRUSTED_CI_CONCURRENT_PROJECT_ID"`
	JobID        string `key:"TRUSTED_CI_JOB_ID"`
	JobToken     string `key:"TRUSTED_CI_JOB_TOKEN"`
	RunnerShort  string `key:"TRUSTED_CI_RUNNER_SHORT_TOKEN"`
	ServerURL    string `key:"TRUSTED_CI_SERVER_URL"`
	CIJobJWT     string // CUSTOM_ENV_CI_JOB_JWT
}

// StatefulEnv maintains variables provided back to the runner during the configuration
// that are then made available to subsequent stages.
type StatefulEnv struct {
	BaseDir     string `key:"JACAMAR_CI_BASE_DIR" required:"true"`      // ValidUser.BaseDir
	BuildsDir   string `key:"JACAMAR_CI_BUILDS_DIR" required:"true"`    // ValidUser.BuildsDir
	CacheDir    string `key:"JACAMAR_CI_CACHE_DIR" required:"true"`     // ValidUser.CacheDir
	ScriptDir   string `key:"JACAMAR_CI_SCRIPT_DIR" required:"true"`    // ValidUser.ScriptDir
	SharedGroup string `key:"JACAMAR_CI_SHARED_GROUP" required:"false"` // ValidUser.SharedGroup
	Username    string `key:"JACAMAR_CI_AUTH_USERNAME" required:"true"` // ValidUser.UserName
	BrokerToken string `key:"JACAMAR_CI_BROKER_TOKEN" required:"false"` // Brokercomms
	ProjectPath string `key:"JACAMAR_CI_PROJECT_PATH" required:"false"` // jwt.project_path
}

// ExecutorEnv maintains a view of all environment variables that are necessary for
// the completion of a CI job. Please note that all variables provided to the driver
// from the GitLab runner should remain available throughout the duration of the
// process and can still be obtained using traditional means (e.g. os.Getenv()).
type ExecutorEnv struct {
	RequiredEnv
	StatefulEnv
}

// Fetcher retrieves expected environment variables (required and stateful) and
// returns them via the ExecutorEnv struct. If any missing variables are detected
// an error message is returned. Both the build and system failures codes are NOT
// fetched as part of this process.
func Fetcher(stateReq bool) (ExecutorEnv, error) {
	je := ExecutorEnv{
		RequiredEnv: RequiredEnv{},
		StatefulEnv: StatefulEnv{},
	}

	env := envMap()
	if err := (&je.RequiredEnv).keyVarMapping(env); err != nil {
		return je, err
	}

	encoded, err := verifyJWTFormat()
	if err != nil {
		return je, err
	}
	je.RequiredEnv.CIJobJWT = encoded

	if stateReq {
		if err := (&je.StatefulEnv).keyVarMapping(env); err != nil {
			return je, err
		}
	}

	return je, nil
}

// ExitCodes returns both the SYSTEM_FAILURE_EXIT_CODE (default: 2) and BUILD_FAILURE_EXIT_CODE
// (default: 1 ) if defined by the custom executor. Uses default values if none found.
func ExitCodes() (sysExit, buildExit int) {
	var buildErr, sysErr error
	ce := os.Environ()
	for _, e := range ce {
		if strings.HasPrefix(e, SysCodePrefix) {
			se := strings.Join(strings.Split(e, "=")[1:], "=")
			sysExit, sysErr = strconv.Atoi(se)
		} else if strings.HasPrefix(e, BuildCodePrefix) {
			be := strings.Join(strings.Split(e, "=")[1:], "=")
			buildExit, buildErr = strconv.Atoi(be)
		}
	}

	if buildExit == 0 || buildErr != nil {
		buildExit = 1
	}
	if sysExit == 0 || sysErr != nil {
		sysExit = 2
	}

	return
}

// EstablishScriptEnv splits the contents of a CI job script according to the maximum environment
// character length and set's every environment variable accordingly.
func EstablishScriptEnv(contents string, maxEnvChars int) []string {
	if contents == "" || maxEnvChars == 0 {
		return []string{}
	}

	encoded := base64.StdEncoding.EncodeToString([]byte(contents))
	l := len(encoded)
	n := 0

	env := make([]string, int(math.Ceil(float64(l)/float64(maxEnvChars))))

	for i := 0; i < l; i += maxEnvChars {
		if (l - i) <= maxEnvChars {
			env[n] = fmt.Sprintf(
				"%s%d=%s",
				ScriptContentsPrefix,
				n,
				encoded[i:],
			)
		} else {
			env[n] = fmt.Sprintf(
				"%s%d=%s",
				ScriptContentsPrefix,
				n,
				encoded[i:i+maxEnvChars],
			)
		}
		n++
	}

	return env
}

// RetrieveScriptEnv returns the full contents of a job script transferred via environment
// variables. An empty string will result in an error.
func RetrieveScriptEnv() (string, error) {
	var sb strings.Builder
	n := 0

	for {
		val, exists := os.LookupEnv(ScriptContentsPrefix + strconv.Itoa(n))
		if !exists {
			break
		}

		// Avoid passing large variables to new processes when not required.
		_ = os.Unsetenv(ScriptContentsPrefix + strconv.Itoa(n))

		sb.WriteString(val)
		n++
	}

	if sb.Len() == 0 {
		return "", errors.New("failed to retrieve script from environment")
	}

	decoded, err := base64.StdEncoding.DecodeString(sb.String())
	if err != nil {
		return "", fmt.Errorf("error decoding script contents, %w", err)
	}

	return string(decoded), nil
}

// SupportedPrefix ensures that the environment variable key provided has a supported prefix.
// This should be used to avoid sharing or closely analyzing environment variables not associated
// with the custom executor model. IMPORTANT: this does not guarantee security.
func SupportedPrefix(s string) bool {
	if strings.HasPrefix(s, UserEnvPrefix) ||
		strings.HasPrefix(s, StatefulEnvPrefix) ||
		strings.HasPrefix(s, ScriptContentsPrefix) ||
		strings.HasPrefix(s, "TRUSTED_CI_") ||
		strings.HasPrefix(s, SysCodePrefix) ||
		strings.HasPrefix(s, BuildCodePrefix) {
		return true
	}

	return false
}

func envMap() map[string]string {
	env := os.Environ()
	m := make(map[string]string)

	for _, e := range env {
		if SupportedPrefix(e) {
			k := strings.Split(e, "=")[0]
			v := strings.Join(strings.Split(e, "=")[1:], "=")
			m[k] = v
		}
	}

	return m
}

func (r *RequiredEnv) keyVarMapping(env map[string]string) error {
	t := reflect.TypeOf(*r)
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		tag := field.Tag.Get(UserVarTagName)
		val := env[tag]
		if tag == "" {
			continue
		} else if val == "" {
			return fmt.Errorf("missing required variable %s, %s", tag, errSuffix)
		}
		r.setRequiredEnv(field.Name, val)
	}

	return nil
}

func (r *RequiredEnv) setRequiredEnv(field, value string) {
	v := reflect.ValueOf(r).Elem().FieldByName(field)
	if v.IsValid() {
		v.SetString(value)
	}
}

func (s *StatefulEnv) keyVarMapping(env map[string]string) error {
	t := reflect.TypeOf(*s)
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		tag := field.Tag.Get(UserVarTagName)
		req := field.Tag.Get(RequiredKey)
		val := env[tag]
		if val == "" {
			if req == "true" {
				return fmt.Errorf("missing stateful variable %s, %s", tag, errSuffix)
			}
			continue
		}
		s.setRequiredEnv(field.Name, val)
	}

	return nil
}

func (s *StatefulEnv) setRequiredEnv(field, value string) {
	v := reflect.ValueOf(s).Elem().FieldByName(field)
	if v.IsValid() {
		v.SetString(value)
	}
}
