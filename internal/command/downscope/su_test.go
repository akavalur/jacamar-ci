package downscope

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestFactory_CreateSu_Sudo(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		Shell:       "/bin/bash",
		KillTimeout: "5s",
	}).AnyTimes()
	cfg.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()

	f := Factory{
		AbsCmdr:  command.NewAbsCmdr(ctx, cfg.General(), m),
		Cfg:      cfg,
		AuthUser: mockAuthWorkingID(ctrl),
		Concrete: arguments.ConcreteArgs{
			Prepare: &arguments.PrepareCmd{},
		},
	}

	t.Run("sudo su shell created", func(t *testing.T) {
		s := f.CreateSudoShell(envparser.StatefulEnv{})
		assert.NotNil(t, s.cmd)
		assert.Equal(t, s.cmd.Args, []string{
			"sudo", "-E",
			"su", "user", "-m", "-s", "/bin/bash", "--pty",
			"-c", "/usr/bin/jacamar --no-auth prepare",
		})
		assert.Equal(t, s.cmd.Dir, "/home/user")
	})

	t.Run("su shell created", func(t *testing.T) {
		s := f.CreateSuShell(envparser.StatefulEnv{})
		assert.NotNil(t, s.cmd)
		assert.Equal(t, []string{
			"su", "user", "-m", "-s", "/bin/bash", "--pty",
			"-c", "/usr/bin/jacamar --no-auth prepare",
		}, s.cmd.Args)
		assert.Equal(t, s.cmd.Dir, "/home/user")
	})
}
