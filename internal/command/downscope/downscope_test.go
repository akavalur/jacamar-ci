package downscope

import (
	"context"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type downTests struct {
	script    string
	state     envparser.StatefulEnv
	targetEnv map[string]string

	s       *shell
	cfg     configure.Configurer
	absCmdr *command.AbstractCommander
	auth    *mock_authuser.MockAuthorized

	assertError  func(*testing.T, error)
	assertShell  func(*testing.T, *shell)
	assertString func(*testing.T, string)
}

func mockGenConfig(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()
	m.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()
	return m
}

func mockAuthCurrent(ctrl *gomock.Controller) (*mock_authuser.MockAuthorized, *user.User) {
	cur, _ := user.Current()
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().HomeDir().Return(cur.HomeDir).AnyTimes()
	return m, cur
}

func mockAuthWorkingID(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().Username().Return("user").AnyTimes()
	m.EXPECT().HomeDir().Return("/home/user").AnyTimes()
	m.EXPECT().IntegerID().Return(1001, 2002).AnyTimes()
	m.EXPECT().Groups().Return([]uint32{1001, 42}).AnyTimes()
	return m
}

func Test_shell_PipeOutput(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(ctrl)

	bash := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m),
		cmd: nil,
	}
	bash.build("/bin/bash")

	pty := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m),
		cmd: nil,
		pty: true,
	}
	pty.build("/bin/bash", "-c")

	tests := map[string]downTests{
		"exit 0 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 0`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"exit 1 command, successfully executed": {
			s:      bash,
			script: `echo "world" && exit 1`,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"pseudo terminal in use": {
			s:      pty,
			script: `date`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.s.PipeOutput(tt.script)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_shell_ReturnOutput(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(ctrl)

	bash := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m),
		cmd: nil,
	}
	bash.build("/bin/bash")

	pty := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m),
		cmd: nil,
		pty: true,
	}
	pty.build("/bin/bash", "-c")

	tests := map[string]downTests{
		"exit 0 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 0`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
		"exit 1 command, successfully executed": {
			s:      bash,
			script: `echo "world" && exit 1`,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "world")
			},
		},
		"pseudo terminal in use": {
			s:      pty,
			script: `echo "TEST"`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "TEST")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.s.ReturnOutput(tt.script)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func TestFactory_CreateStdShell(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(ctrl)
	cfg.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()

	bash := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m),
		cmd: nil,
	}
	bash.build("/bin/bash")

	tests := map[string]downTests{
		"valid standard bash command generated": {
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m),
			auth:    mockAuthWorkingID(ctrl),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.Equal(t, "/usr/bin/jacamar", s.cmd.Path)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				AbsCmdr:  tt.absCmdr,
				AuthUser: tt.auth,
				Cfg:      tt.cfg,
			}
			s := f.CreateStdShell()

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
		})
	}
}

func Test_MiscInterface(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(ctrl)

	s := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m),
		cmd: nil,
	}
	s.build("/usr/bin/env", "-i", "--login")

	t.Run("updated command directory", func(t *testing.T) {
		s.CommandDir("/new")
		assert.Equal(t, s.cmd.Dir, "/new")
	})
	t.Run("append env", func(t *testing.T) {
		s.AppendEnv([]string{"HELLO=WORLD"})
		assert.Contains(t, s.cmd.Env, "HELLO=WORLD")
	})
	t.Run("check sigterm received", func(t *testing.T) {
		got := s.SigtermReceived()
		assert.False(t, got)
	})
	t.Run("request context", func(t *testing.T) {
		got := s.RequestContext()
		assert.Nil(t, got.Err())
	})
	t.Run("request copied command", func(t *testing.T) {
		got := s.CopiedCmd()
		assert.NotNil(t, got)
		assert.Equal(t, "/usr/bin/env", got.Path)
	})
	t.Run("command not modified", func(t *testing.T) {
		got := s.CopiedCmd()
		s.ModifyCmd("date")
		assert.NotNil(t, s.cmd)
		assert.Equal(t, got, s.cmd)
	})
}
