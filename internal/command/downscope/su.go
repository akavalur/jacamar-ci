package downscope

import (
	"fmt"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

// CreateSudoShell  generates a shell targeting the authorized user with downscoping via
// sudo su.  All future commands will be executed within the users pseudo-terminal and the
// environment for them is dictated by Jacamar's. It is expected that this is used only
// as part of downscoping.
func (f Factory) CreateSudoShell(state envparser.StatefulEnv) *shell {
	// Build Sudo Su command with specific behaviors based upon manuals:
	// sudo(8): https://man7.org/linux/man-pages/man8/sudo.8.html
	// -E (preserve-env): Keep custom executor established environment
	// su(1): https://man7.org/linux/man-pages/man1/su.1.html
	// {username}: Target for downscope based upon authorization
	// -m (preserve-environment): Keep custom executor established environment
	// -s (shell): Target shell based upon configuration or default
	// --pty: Create a pseudo-terminal for the session
	// -c: Pass a command into the sessions, specific command set during execution
	args := append([]string{"-E", "su"}, suArguments(f.Cfg.General(), f.AuthUser.Username())...)
	return f.buildSu(state, "sudo", args)
}

// CreateSuShell  generates a shell targeting the authorized user with downscoping via
// su. This shares the same functionality as 'CreateSudoShell' only with the use
// of the sudo application.
func (f Factory) CreateSuShell(state envparser.StatefulEnv) *shell {
	return f.buildSu(state, "su", suArguments(f.Cfg.General(), f.AuthUser.Username()))
}

func (f Factory) buildSu(
	state envparser.StatefulEnv,
	name string,
	arg []string,
) *shell {
	s := &shell{
		abs: f.AbsCmdr,
		cmd: nil,
		pty: true,
	}

	jName, jArgs := command.JacamarCmd(f.Cfg.Auth(), f.Concrete)
	jCmd := fmt.Sprintf("%s %s", jName, strings.Join(jArgs, " "))
	arg = append(arg, jCmd)

	s.build(name, arg...)
	s.cmd.Env = safeEnv(f.AuthUser, f.Cfg.Auth(), state.BrokerToken)
	s.CommandDir(f.identifyWorkDir())

	return s
}

func suArguments(gen configure.General, username string) []string {
	return []string{
		username,
		"-m",
		"-s",
		command.IdentifyShell(gen),
		"--pty",
		"-c",
	}
}
