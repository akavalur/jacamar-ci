package downscope

import (
	"fmt"
	"os"
	"strings"
	"syscall"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

// CreateSetuidShell generates a setuid enabled shell targeting the authorized user.
// Upon successful creation a command ($ bash) will be defined for future
// runs, via stdin. Runs are always presented with a controlled command environment. Only
// custom executor defined variables (e.g. CUSTOM_ENV_) will be replicated. It is
// expected that this is used as part of a Jacamar-Auth application when downscoping is specified.
func (f Factory) CreateSetuidShell(state envparser.StatefulEnv) *shell {
	s := &shell{
		abs: f.AbsCmdr,
		cmd: nil,
	}

	name, args := command.JacamarCmd(f.Cfg.Auth(), f.Concrete)
	s.build(name, args...)

	s.targetValidUser(f.AuthUser)
	s.cmd.Env = safeEnv(f.AuthUser, f.Cfg.Auth(), state.BrokerToken)
	s.CommandDir(f.identifyWorkDir())

	return s
}

func (s *shell) targetValidUser(au authuser.Authorized) {
	uid, gid := au.IntegerID()

	s.cmd.SysProcAttr.Credential = &syscall.Credential{
		Uid:         uint32(uid),
		Gid:         uint32(gid),
		Groups:      au.Groups(),
		NoSetGroups: false,
	}
}

func safeEnv(au authuser.Authorized, cfg configure.Auth, brokerToken string) []string {
	var safe []string
	for _, e := range os.Environ() {
		if envparser.SupportedPrefix(e) {
			if strings.HasPrefix(e, envparser.UserEnvPrefix+"CI_JOB_JWT") {
				// Jacamar does not required CI_JOB_JWT so remove by default,
				continue
			}

			if cfg.Broker.URL != "" {
				e = strings.ReplaceAll(e, envparser.TrustJobToken(), brokerToken)
			}

			safe = append(safe, e)
		}
	}

	name := au.Username()
	sys, build := envparser.ExitCodes()

	safe = append(safe, []string{
		fmt.Sprintf("%s=%d", envparser.SysCodePrefix, sys),
		fmt.Sprintf("%s=%d", envparser.BuildCodePrefix, build),
		fmt.Sprintf("USER=%s", name),
		fmt.Sprintf("LOGNAME=%s", name),
		fmt.Sprintf("HOME=%s", au.HomeDir()),
		"PATH=/usr/bin:/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin",
	}...)

	return append(safe, cfg.DownscopeEnv...)
}
