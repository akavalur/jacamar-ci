package downscope

import (
	"context"
	"os"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func TestFactory_CreateSetuidShell(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	_ = os.Setenv("CREATESETUIDSHELL", "TEST")
	defer os.Unsetenv("CREATESETUIDSHELL")

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		Shell:       "/bin/bash",
		KillTimeout: "5s",
	}).AnyTimes()
	cfg.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()

	cmdDir := mock_configure.NewMockConfigurer(ctrl)
	cmdDir.EXPECT().General().Return(configure.General{
		Shell:       "/bin/bash",
		KillTimeout: "5s",
	}).AnyTimes()
	cmdDir.EXPECT().Auth().Return(configure.Auth{
		DownscopeCmdDir: "/var/tmp",
	}).AnyTimes()

	tests := map[string]downTests{
		"valid setuid bash command generated": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.Equal(t, "/usr/bin/jacamar", s.cmd.Path)
				assert.Equal(t, uint32(1001), s.cmd.SysProcAttr.Credential.Uid)
				assert.Equal(t, s.cmd.Dir, "/home/user")
			},
		},
		"SigtermReceived functions correctly against defaults": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.False(t, s.SigtermReceived(), "default SigtermReceived must be false")
			},
		},
		"underlying command AppendEnv properly function": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s)
				s.AppendEnv([]string{"FOO=BAR"})
				assert.Contains(t, s.cmd.Env, "FOO=BAR")
			},
		},
		"RequestContext returns context pointer": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.RequestContext())
			},
		},
		"setuid shell create with safe environment": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.NotContains(t, s.cmd.Env, "CREATESETUIDSHELL=TEST")
			},
		},
		"setuid shell created with configured command directory": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cmdDir.General(), m),
			cfg:     cmdDir,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.Equal(t, s.cmd.Dir, "/var/tmp")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				AbsCmdr:  tt.absCmdr,
				Cfg:      tt.cfg,
				AuthUser: tt.auth,
			}
			s := f.CreateSetuidShell(tt.state)

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
		})
	}
}

func TestMechanism_targetValidUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	workingShell := &shell{}
	workingShell.build("bash")

	tests := map[string]downTests{
		"valid command generated": {
			auth: mockAuthWorkingID(ctrl),
			s:    workingShell,
			assertShell: func(t *testing.T, s *shell) {
				cmd := s.cmd
				assert.NotNil(t, cmd)
				assert.True(t, strings.HasSuffix(cmd.Path, "bash"))
				assert.Equal(t, uint32(1001), cmd.SysProcAttr.Credential.Uid, "target UID")
				assert.Equal(t, uint32(2002), cmd.SysProcAttr.Credential.Gid, "target GID")
				assert.Equal(t, []uint32{1001, 42}, cmd.SysProcAttr.Credential.Groups)
				assert.Equal(t, false, cmd.SysProcAttr.Credential.NoSetGroups)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			tt.s.targetValidUser(tt.auth)

			if tt.assertShell != nil {
				tt.assertShell(t, tt.s)
			}
		})
	}
}

func Test_safeEnv(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().Username().Return("user").AnyTimes()
	m.EXPECT().HomeDir().Return("/home/user").AnyTimes()

	tests := map[string]struct {
		cfg       configure.Auth
		encoded   string
		auth      *mock_authuser.MockAuthorized
		targetEnv map[string]string
		assertEnv func(*testing.T, []string)
	}{
		"standard \"safe\" environment": {
			targetEnv: map[string]string{
				envparser.UserEnvPrefix + "A":          "foo",
				envparser.StatefulEnvPrefix + "B":      "bar",
				envparser.UserEnvPrefix + "CI_JOB_JWT": "T0k3n",
			},
			auth: m,
			assertEnv: func(t *testing.T, e []string) {
				assert.Contains(t, e, "HOME=/home/user")
				assert.Contains(t, e, envparser.UserEnvPrefix+"A"+"=foo")
				assert.Contains(t, e, envparser.StatefulEnvPrefix+"B"+"=bar")
				assert.Contains(t, e, "PATH=/usr/bin:/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin")
				assert.NotContains(t, e, "Tok3n")
			},
		},
		"broker enabled, required safe environment removes tokens": {
			targetEnv: map[string]string{
				envparser.UserEnvPrefix + "A":          "T0k3n",
				envparser.UserEnvPrefix + "CI_JOB_JWT": "T0k3n",
				"TRUSTED_CI_JOB_TOKEN":                 "T0k3n",
			},
			encoded: "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317",
			cfg: configure.Auth{
				Broker: configure.Broker{
					URL: "https://broker.example.com",
				},
			},
			auth: m,
			assertEnv: func(t *testing.T, e []string) {
				assert.NotContains(t, e, "Tok3n")
				assert.Contains(t, e, envparser.UserEnvPrefix+"A"+"=e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317")
			},
		},
		"included downscope_env configuration": {
			targetEnv: map[string]string{},
			auth:      m,
			cfg: configure.Auth{
				DownscopeEnv: []string{
					"HELLO=WORLD",
					"TESTING=123",
				},
			},
			assertEnv: func(t *testing.T, e []string) {
				assert.Contains(t, e, "HOME=/home/user")
				assert.Contains(t, e, "HELLO=WORLD")
				assert.Contains(t, e, "TESTING=123")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got := safeEnv(tt.auth, tt.cfg, tt.encoded)
			tt.assertEnv(t, got)
		})
	}
}
