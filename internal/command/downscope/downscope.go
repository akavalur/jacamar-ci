package downscope

import (
	"bytes"
	"context"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
)

type shell struct {
	abs *command.AbstractCommander
	cmd *exec.Cmd
	pty bool
}

func (s *shell) PipeOutput(stdin string) error {
	cmd := &exec.Cmd{}
	s.abs.CloneCmd(s.cmd, cmd)

	var stdoutBuf, stderrBuf bytes.Buffer
	cmd.Stdout = io.MultiWriter(os.Stdout, &stdoutBuf)
	cmd.Stderr = io.MultiWriter(os.Stderr, &stderrBuf)
	if stdin != "" {
		s.prepStdin(stdin, cmd)
	}

	return s.abs.RunCmd(cmd)
}

func (s *shell) ReturnOutput(stdin string) (string, error) {
	cmd := &exec.Cmd{}
	s.abs.CloneCmd(s.cmd, cmd)

	var b bytes.Buffer
	cmd.Stdout = &b
	cmd.Stderr = &b
	if stdin != "" {
		s.prepStdin(stdin, cmd)
	}

	err := s.abs.RunCmd(cmd)

	var sb strings.Builder
	sb.Write(b.Bytes())

	return sb.String(), err
}

func (s *shell) CommandDir(dir string) {
	s.cmd.Dir = dir
}

func (s *shell) RequestContext() context.Context {
	return s.abs.SignalContext()
}

func (s *shell) SigtermReceived() bool {
	return s.abs.SigtermReceived()
}

func (s *shell) AppendEnv(e []string) {
	s.cmd.Env = append(s.cmd.Env, e...)
}

func (s *shell) CopiedCmd() *exec.Cmd {
	cmd := &exec.Cmd{}
	s.abs.CloneCmd(s.cmd, cmd)
	return cmd
}

func (s *shell) ModifyCmd(name string, arg ...string) {
	log.Println("ModifyCmd not supported for Downscoping")
}

func (s *shell) build(name string, arg ...string) {
	/* #nosec */
	// launching subprocess with variables required
	cmd := exec.Command(name, arg...)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	s.cmd = cmd
}

func (s *shell) prepStdin(stdin string, cmd *exec.Cmd) {
	var sb strings.Builder
	sb.WriteString(stdin)

	if s.pty {
		cmd.Args = append(cmd.Args, sb.String())
	} else {
		cmd.Stdin = bytes.NewBufferString(sb.String())
	}
}

func (f Factory) identifyWorkDir() string {
	tar := f.Cfg.Auth().DownscopeCmdDir
	if tar != "" {
		return filepath.Clean(tar)
	}

	return f.AuthUser.HomeDir()
}

// Factory is used to defined requirements and interface to all related methods.
type Factory struct {
	AbsCmdr  *command.AbstractCommander
	Cfg      configure.Configurer
	AuthUser authuser.Authorized
	Concrete arguments.ConcreteArgs
}

// CreateStdShell generates a shell that is targeted at authorization level command execution.
// Upon successful creation a command ($ bash) will be defined for future runs, via
// stdin. No setuid (or similar) mechanism are enabled and the current environment is
// replicated in the command. It is expected that this is used as part of a Jacamar-Auth
// application with no downscoping specified.
func (f Factory) CreateStdShell() *shell {
	s := &shell{
		abs: f.AbsCmdr,
		cmd: nil,
	}

	name, args := command.JacamarCmd(f.Cfg.Auth(), f.Concrete)
	s.build(name, args...)
	s.cmd.Env = os.Environ()
	s.CommandDir(f.identifyWorkDir())

	return s
}
