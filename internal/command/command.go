package command

import (
	"bytes"
	"context"
	"fmt"
	"os/exec"
	"path/filepath"
	"reflect"
	"strings"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

const (
	defaultShell = "/usr/bin/bash"
	jacamarApp   = "jacamar"
	killDuration = 30 // backup duration
)

// Commander interface provides the lowest level mechanisms that are supported by
// Jacamar for influencing how commands/stdin is executed on the underlying or target
// system. Idly it should be interacted with via a Runner interface built on top of.
type Commander interface {
	// PipeOutput executes the provided stdin. All output from the command is piped
	// directly to stdout/stderr. Bypasses the runner command and instead rely solely
	// on the its underlying base/environment layer to avoid execution on such resources.
	PipeOutput(stdin string) error
	// ReturnOutput executes the provided stdin. All output from the command is returned
	// as a string. Bypasses the runner command and instead rely solely on the its
	// underlying base/environment layer to avoid execution on such resources.
	ReturnOutput(stdin string) (string, error)
	// CommandDir changes the directory where command will be executed.
	CommandDir(dir string)
	// SigtermReceived returns a boolean based upon if a SIGTERM has been captured.
	SigtermReceived() bool
	// RequestContext returns the commander context, used to identify if a SIGTERM has been captured
	// and Jacamar must being a graceful of shutdown process.
	RequestContext() context.Context
	// AppendEnv appends the provided slice to the command's environment.
	AppendEnv([]string)
	// CopiedCmd returns a copy of the underlying raw command.
	CopiedCmd() *exec.Cmd
	// ModifyCmd updates the underlying execute the named program with the given arguments.
	ModifyCmd(name string, arg ...string)
}

// AbstractCommander organizes configurations for all Runner interface. Each can
// implement support for configuration in their own way with no strict enforcement
// on how they are observed.
type AbstractCommander struct {
	KillTimeout  time.Duration
	NotifyTerm   bool // Send SIGTERM to sub-processes
	TermCaptured bool // Identify is SIGTERM has been capture and shutdown in process

	sigCtx    context.Context
	cancelCtx context.CancelFunc
}

func (a *AbstractCommander) SigtermReceived() bool {
	return a.TermCaptured
}

func (a *AbstractCommander) SignalContext() context.Context {
	return a.sigCtx
}

// EnableNotifyTerm safely enables the use of SIGKILL.
func (a *AbstractCommander) EnableNotifyTerm() {
	a.NotifyTerm = true
}

// CloneCmd clones all values from the source exec.Cmd struct to the
// provided destination.
func (a *AbstractCommander) CloneCmd(src, dest *exec.Cmd) {
	if src == nil || dest == nil {
		// Lets avoid a panic.
		return
	}

	val := reflect.ValueOf(src)
	if val.Kind() == reflect.Ptr {
		ex := val.Elem()
		y := reflect.New(ex.Type())
		ey := y.Elem()
		ey.Set(ex)
		reflect.ValueOf(dest).Elem().Set(y.Elem())
	}
}

// NoOutputCmd executes the provided command/args who's output, both for
// success as well as error, will be ignored.
func NoOutputCmd(command string, arg ...string) {
	/* #nosec */
	// Arguments identified and ran as same user.
	cmd := exec.Command(command, arg...)
	err := cmd.Start()
	if err != nil {
		_ = cmd.Wait()
	}
}

// RunCmd runs the provided exec.Cmd and integrates monitoring for SIGTERM into waiting for
// any spawned processes to complete.
func (a *AbstractCommander) RunCmd(cmd *exec.Cmd) (err error) {
	err = cmd.Start()
	if err != nil {
		return err
	}

	cmdErr := make(chan error, 1)
	defer close(cmdErr)

	go a.MonitorTermination(cmd, cmdErr)

	err = cmd.Wait()
	cmdErr <- err

	return
}

func IdentifyShell(gen configure.General) string {
	if gen.Shell != "" {
		return gen.Shell
	}

	path, err := exec.LookPath("bash")
	if err != nil || path == "" {
		// This should be exceedingly rare as Bash is a requirement for deployment
		// but revert to default in case encountered.
		path = defaultShell
	}
	return path
}

// JacamarCmd builds a valid stdin to invoke Jacamar via a defined run mechanism.
func JacamarCmd(auth configure.Auth, c arguments.ConcreteArgs) (string, []string) {
	var cmd bytes.Buffer

	if auth.JacamarPath != "" {
		cmd.WriteString(filepath.Clean(strings.TrimSpace(auth.JacamarPath)))
		if !strings.HasSuffix(cmd.String(), "jacamar") {
			if !strings.HasSuffix(auth.JacamarPath, "/") {
				cmd.WriteString("/")
			}
			cmd.WriteString(jacamarApp)
		}
	} else {
		path, err := exec.LookPath(jacamarApp)
		if err != nil {
			// When nothing is found default to /usr/bin as JacamarPath
			cmd.WriteString("/usr/bin/")
			cmd.WriteString(jacamarApp)
		}
		cmd.WriteString(path)
	}

	var args []string
	switch {
	case c.Prepare != nil:
		args = []string{"--no-auth", "prepare"}
	case c.Run != nil:
		args = []string{"--no-auth", "run", "env-script", c.Run.Stage}
	case c.Cleanup != nil:
		args = []string{"--no-auth", "cleanup", "--configuration", c.Cleanup.Configuration}
	case c.EnvCmd != nil:
		args = []string{"--no-auth", "env", fmt.Sprintf("'%s'", c.EnvCmd.Input)}
	}

	return cmd.String(), args
}

// NewAbsCmdr generate a valid AbstractCommander structure with application defaults
// establish signal (SIGTERM) monitoring.
func NewAbsCmdr(
	ctx context.Context,
	gen configure.General,
	msg logging.Messenger,
) *AbstractCommander {
	t, err := time.ParseDuration(gen.KillTimeout)
	if err != nil {
		// backup if invalid time duration provided
		t = killDuration * time.Second
		if gen.KillTimeout != "" {
			// we don't need to warn if choose to not set a time
			msg.Warn("Invalid time duration in configuration (KillTimeout), defaults to 30s.")
		}
	}

	sigCtx, cancelCtx := context.WithCancel(ctx)

	a := &AbstractCommander{
		NotifyTerm:  false,
		KillTimeout: t,
		sigCtx:      sigCtx,
		cancelCtx:   cancelCtx,
	}

	go a.MonitorSignal()

	return a
}
