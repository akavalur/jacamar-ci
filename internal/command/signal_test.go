package command

import (
	"context"
	"os"
	"os/exec"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type signalTests struct {
	a   *AbstractCommander
	cmd *exec.Cmd

	mockInteraction func(*exec.Cmd, chan error, *AbstractCommander)

	assertCommand func(*testing.T, *exec.Cmd)
	assertError   func(*testing.T, error)
}

func TestAbstractCommander_MonitorSignal(t *testing.T) {
	t.Run("signal identified, TermCaptured (use notifyProcess)", func(t *testing.T) {
		ctx := context.Background()
		sigCtx, cancelCtx := context.WithCancel(ctx)

		a := &AbstractCommander{
			sigCtx:    sigCtx,
			cancelCtx: cancelCtx,
		}
		go func() {
			time.Sleep(3 * time.Second)
			proc := &os.Process{
				Pid: os.Getpid(),
			}
			notifyProcess(proc)
		}()

		a.MonitorSignal()
		assert.True(t, a.TermCaptured)
	})
}

func TestAbstractCommander_MonitorTermination(t *testing.T) {
	tests := map[string]signalTests{
		"nil command argument provided": {
			a:   &AbstractCommander{},
			cmd: nil,
		},
		"nil process argument provided": {
			a: &AbstractCommander{},
			cmd: &exec.Cmd{
				Process: nil,
			},
			mockInteraction: func(cmd *exec.Cmd, cmdErr chan error, a *AbstractCommander) {
				// need to close to stop monitoring
				a.cancelCtx()
			},
		},
		"completed (done) without signal received": {
			a:   &AbstractCommander{},
			cmd: exec.Command("/bin/date"),
			mockInteraction: func(cmd *exec.Cmd, cmdErr chan error, a *AbstractCommander) {
				err := cmd.Run()
				assert.NoError(t, err, "error running test command")
				err = cmd.Wait()
				cmdErr <- err
			},
		},
		"terminate command, short notification window": {
			a: &AbstractCommander{
				NotifyTerm:  true,
				KillTimeout: 1 * time.Second,
			},
			cmd: exec.Command("/bin/sleep", "30"),
			mockInteraction: func(cmd *exec.Cmd, cmdErr chan error, a *AbstractCommander) {
				err := cmd.Run()
				assert.NoError(t, err, "error running test command")
				a.cancelCtx()
				_ = cmd.Wait()
			},
			assertCommand: func(t *testing.T, cmd *exec.Cmd) {
				assert.True(t, cmd.ProcessState.Exited())
			},
		},
		"SIGTERM captures, avoid running command": {
			a: &AbstractCommander{
				TermCaptured: true,
			},
			cmd: nil,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			cmdErr := make(chan error)
			defer close(cmdErr)

			ctx := context.Background()
			sigCtx, cancelCtx := context.WithCancel(ctx)
			tt.a.sigCtx = sigCtx
			tt.a.cancelCtx = cancelCtx

			if tt.mockInteraction != nil {
				go tt.mockInteraction(tt.cmd, cmdErr, tt.a)
			}

			tt.a.MonitorTermination(tt.cmd, cmdErr)

			if tt.assertCommand != nil {
				tt.assertCommand(t, tt.cmd)
			}
		})
	}
}
