package command

import (
	"context"
	"os"
	"os/exec"
	"reflect"
	"syscall"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestNewAbsCmdr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(gomock.Eq(
		"Invalid time duration in configuration (KillTimeout), defaults to 30s.",
	))

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		KillTimeout: "bad-time",
	})

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	t.Run("enforce new abstract commander default behavior", func(t *testing.T) {
		a := NewAbsCmdr(ctx, cfg.General(), m)

		assert.False(t, a.NotifyTerm, "NotifyTerm expected default is false")
		assert.False(t, a.TermCaptured, "TermCaptured expected default is false")

		a.EnableNotifyTerm()
		assert.True(t, a.NotifyTerm, "NotifyTerm expected to be changed")

		cancel()
	})
}

func TestCloneCmd(t *testing.T) {
	cmd := exec.Command("bash", "test")
	cmd.Env = append(cmd.Env, "foo=bar")
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmd.SysProcAttr.Credential = &syscall.Credential{
		Uid: 1,
		Gid: 1,
	}

	newCmd := &exec.Cmd{}

	tests := []struct {
		name string
		src  *exec.Cmd
		dest *exec.Cmd
	}{
		{
			name: "nil src pointer provided",
			src:  nil,
			dest: newCmd,
		}, {
			name: "copying standard command",
			src:  cmd,
			dest: newCmd,
		}, {
			name: "nil dest pointer provided",
			src:  cmd,
			dest: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AbstractCommander{}
			a.CloneCmd(tt.src, tt.dest)

			if tt.dest != nil && tt.src != nil {
				assert.True(t, reflect.DeepEqual(tt.src, tt.dest))
			}
		})
	}
}

func TestAbstractCommander_RunCmd(t *testing.T) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	tests := map[string]signalTests{
		"run successful command": {
			a: &AbstractCommander{
				sigCtx:    ctx,
				cancelCtx: cancel,
			},
			cmd: exec.Command("date"),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"run unsuccessful command": {
			a: &AbstractCommander{
				sigCtx:    ctx,
				cancelCtx: cancel,
			},
			cmd: exec.Command("exit", "1"),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.a.RunCmd(tt.cmd)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}

	cancel() // cleanup
}

func Test_IdentifyShell(t *testing.T) {
	t.Run("verify default Bash shell", func(t *testing.T) {
		// Remove PATH to ensure no bash found
		_ = os.Unsetenv("PATH")
		got := IdentifyShell(configure.General{})
		assert.Equal(t, defaultShell, got)
	})
}

func TestJacamarCmd(t *testing.T) {
	tmpPath := t.TempDir()
	orgPath := os.Getenv("PATH")
	defer os.Setenv("PATH", orgPath)

	tests := map[string]struct {
		auth     configure.Auth
		c        arguments.ConcreteArgs
		mockPath func()
		name     string
		args     []string
	}{
		"no configuration, run": {
			mockPath: func() {
				_ = os.Setenv("PATH", orgPath)
			},
			auth: configure.Auth{},
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Script: "/file.bash",
					Stage:  "step_script",
				},
			},
			name: "/usr/bin/jacamar",
			args: []string{"--no-auth", "run", "env-script", "step_script"},
		},
		"jacamar path defined, cleanup": {
			auth: configure.Auth{
				JacamarPath: "/bin",
			},
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "/dir/config.toml",
				},
			},
			name: "/bin/jacamar",
			args: []string{"--no-auth", "cleanup", "--configuration", "/dir/config.toml"},
		},
		"jacamar found on path, prepare": {
			mockPath: func() {
				_ = os.Mkdir(tmpPath+"/bin", 0700)
				file, _ := os.Create(tmpPath + "/bin/jacamar")
				_ = os.Chmod(file.Name(), 0700)
				_ = os.Setenv("PATH", orgPath+":"+tmpPath+"/bin")
			},
			auth: configure.Auth{},
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			name: tmpPath + "/bin/jacamar",
			args: []string{"--no-auth", "prepare"},
		},
		"jacamar path defined, env": {
			auth: configure.Auth{
				JacamarPath: "/bin",
			},
			c: arguments.ConcreteArgs{
				EnvCmd: &arguments.EnvCmd{
					Input: "/test/$SOMETHING/.ci",
				},
			},
			name: "/bin/jacamar",
			args: []string{"--no-auth", "env", "'/test/$SOMETHING/.ci'"},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockPath != nil {
				tt.mockPath()
			}

			got, got1 := JacamarCmd(tt.auth, tt.c)

			if got != tt.name {
				t.Errorf("JacamarCmd() name = %v, want %v", got, tt.name)
			}
			if !reflect.DeepEqual(got1, tt.args) {
				t.Errorf("JacamarCmd() args = %v, want %v", got1, tt.args)
			}
		})
	}
}
