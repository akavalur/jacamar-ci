package command

import (
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"time"
)

// MonitorSignal continuously monitors for SIGTERM until either it is
// encountered of the channel closed.
func (a *AbstractCommander) MonitorSignal() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGTERM)

	<-sig

	a.TermCaptured = true
	a.cancelCtx()
}

// MonitorTermination associates a command with the process of awaiting an identified
// termination signal. If encountered the commanded with be properly terminated.
func (a *AbstractCommander) MonitorTermination(cmd *exec.Cmd, cmdErr chan error) {
	if cmd == nil {
		return
	}

	select {
	case <-cmdErr:
		// command stopped/failed so monitoring no longer required
		return
	case <-a.sigCtx.Done():
		a.manageTermination(cmd.Process)
	}
}

func (a *AbstractCommander) manageTermination(proc *os.Process) {
	if proc == nil {
		return
	}

	if a.NotifyTerm {
		notifyProcess(proc)
		time.Sleep(a.KillTimeout) // Wait before killing process.
	}
	killProcess(proc)
}

func notifyProcess(proc *os.Process) {
	if proc != nil {
		if err := proc.Signal(syscall.SIGTERM); err != nil {
			return
		}
	}
}

func killProcess(proc *os.Process) {
	if proc != nil {
		if proc.Pid > 0 {
			_ = syscall.Kill(-proc.Pid, syscall.SIGKILL)
		} else {
			_ = proc.Kill()
		}
	}
}
