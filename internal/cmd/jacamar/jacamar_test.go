package jacamar

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type jacamarTests struct {
	c         arguments.ConcreteArgs
	opt       configure.Auth
	targetEnv map[string]string

	cfg *mock_configure.MockConfigurer
	msg *mock_logging.MockMessenger

	assertError    func(*testing.T, error)
	assertAbstract func(*testing.T, *executors.AbstractExecutor)
}

var (
	workingCfgEnv map[string]string
)

func init() {
	// override initialized exit behaviors
	sysExit = func() {}
	buildExit = func() {}

	workingCfgEnv = map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":     "1",
		"TRUSTED_CI_JOB_ID":                    "123",
		"TRUSTED_CI_JOB_TOKEN":                 "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":        "abc",
		"TRUSTED_CI_BUILDS_DIR":                "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":                 "/gitlab/cache",
		envparser.UserEnvPrefix + "CI_JOB_JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":                "gitlab.url",
	}
}

func Test_establishAbstract(t *testing.T) {
	ctx := context.Background()
	cancelCtx, cancelFun := context.WithCancel(ctx)
	defer cancelFun()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Stderr(gomock.Any(), gomock.Any()).AnyTimes()

	tests := map[string]jacamarTests{
		"config_exec - failed to establish job context": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			msg: m,
		},
		"config_exec - failed to authorize user": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/auth_test_config.toml",
				},
			},
			msg:       m,
			targetEnv: workingCfgEnv,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got := establishAbstract(cancelCtx, tt.c, tt.msg)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, got)
			}
		})
	}
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// reinforce the expectations configurations equate to the runner requirements.
	none := mock_configure.NewMockConfigurer(ctrl)
	none.EXPECT().General().Return(
		configure.General{
			Executor: "",
		},
	).Times(1)

	slurm := mock_configure.NewMockConfigurer(ctrl)
	slurm.EXPECT().General().Return(
		configure.General{
			Executor: "slurm",
		},
	).Times(1)

	lsf := mock_configure.NewMockConfigurer(ctrl)
	lsf.EXPECT().General().Return(
		configure.General{
			Executor: "lsf",
		},
	).Times(1)

	cobalt := mock_configure.NewMockConfigurer(ctrl)
	cobalt.EXPECT().General().Return(
		configure.General{
			Executor: "cobalt",
		},
	).Times(1)

	shell := mock_configure.NewMockConfigurer(ctrl)
	shell.EXPECT().General().Return(
		configure.General{
			Executor: "shell",
		},
	).Times(1)

	tests := map[string]jacamarTests{
		"no executor defined": {
			cfg: none,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unrecognized executor type defined in configuration",
				)
			},
		},
		"slurm executor defined": {
			cfg: slurm,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"lsf executor defined": {
			cfg: lsf,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"cobalt executor defined": {
			cfg: cobalt,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"shell executor defined": {
			cfg: shell,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ae := &executors.AbstractExecutor{
				Cfg: tt.cfg,
			}

			_, err := NewExecutor(ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
