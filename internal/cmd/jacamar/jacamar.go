package jacamar

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/cobalt"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/lsf"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/shell"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/slurm"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
)

var (
	// Function to facilitate associated process exit.
	sysExit, buildExit func()
)

// Start initializes the jacamar-ci application process, requires valid command arguments and
// custom executor provided environmental variables to be present.
func Start(c arguments.ConcreteArgs) {
	ctx := context.Background()
	msg := logging.NewMessenger()

	defer exitPanic(msg)

	if c.Cleanup != nil {
		sysExit = func() { os.Exit(0) }
	}

	ae := establishAbstract(ctx, c, msg)

	if c.Config != nil {
		preparations.ConfigExec(ae, c, sysExit)
		return
	}

	exec, err := NewExecutor(ae)
	if err != nil {
		preparations.StdError(c, err.Error(), msg, sysExit)
	}

	switch {
	case c.Prepare != nil:
		prepareExec(ae, exec, c)
	case c.Run != nil:
		ae.Runner.CommandDir(ae.Auth.ScriptDir())
		runExec(ae, exec, c)
	case c.Cleanup != nil:
		cleanupExec(ae, exec, c)
	default:
		preparations.StdError(c, "Invalid subcommand - See $ jacamar --help", ae.Msg, sysExit)
	}
}

func establishAbstract(
	ctx context.Context,
	c arguments.ConcreteArgs,
	msg logging.Messenger,
) (ae *executors.AbstractExecutor) {
	ae, err := preparations.JobContext(c, sysExit)
	if err != nil {
		preparations.StdError(c, err.Error(), msg, sysExit)
		return nil
	}

	ae.Msg = msg

	err = preparations.NewAuthorizedUser(ae, c)
	if err != nil {
		preparations.StdError(c, err.Error(), msg, sysExit)
		return nil
	}

	cmdr, err := preparations.NewCommander(ae, ctx, c)
	if err != nil {
		preparations.StdError(c, err.Error(), msg, sysExit)
		return nil
	}
	ae.Runner = basic.NewMechanism(cmdr, ae.Auth)

	return ae
}

func prepareExec(
	ae *executors.AbstractExecutor,
	exec executors.Executor,
	c arguments.ConcreteArgs,
) {
	if err := ae.Prepare(exec); err != nil {
		preparations.StdError(c, err.Error(), ae.Msg, sysExit)
	}
}

func runExec(
	ae *executors.AbstractExecutor,
	exec executors.Executor,
	c arguments.ConcreteArgs,
) {
	if err := ae.Run(exec); err != nil {
		preparations.StdError(c, err.Error(), ae.Msg, buildExit)
	}
}

func cleanupExec(
	ae *executors.AbstractExecutor,
	exec executors.Executor,
	c arguments.ConcreteArgs,
) {
	if err := usertools.PreCleanupVerification(ae.Auth); err != nil {
		preparations.StdError(c, err.Error(), ae.Msg, sysExit)
	}

	if err := ae.Cleanup(exec); err != nil {
		// Cleanup only fails when the executor's specific cleanup phase fail, any other
		// job failure that is inadvertently caught by this stage has already been correctly
		// reported.
		ae.Msg.Stderr("Job cleanup error: %s", err.Error())
		sysExit()
	}
}

// NewExecutor generates and returns a valid interface for job level CI execution.
func NewExecutor(ae *executors.AbstractExecutor) (executors.Executor, error) {
	tar := (ae.Cfg.General()).Executor
	switch strings.TrimSpace(strings.ToLower(tar)) {
	case "shell":
		return shell.NewExecutor(ae), nil
	case "cobalt", "qsub":
		return cobalt.NewExecutor(ae), nil
	case "lsf", "bsub":
		return lsf.NewExecutor(ae), nil
	case "slurm", "sbatch":
		return slurm.NewExecutor(ae), nil
	default:
		return nil, errors.New("unrecognized executor type defined in configuration")
	}
}

func exitPanic(msg logging.Messenger) {
	if r := recover(); r != nil {
		msg.Error(fmt.Sprintf("Unexpected error encountered: %v", r))
		sysExit()
	}
}

func init() {
	// Initialize exit status in order to facilitate potential failures at any stage.
	sys, build := envparser.ExitCodes()
	sysExit = func() { os.Exit(sys) }
	buildExit = func() { os.Exit(build) }
}
