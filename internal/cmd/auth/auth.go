// Package auth realizes the core of the jacamar-auth application by ensuring the realization
// of the process of authorizing the already authenticated  user provided by the GitLab server.
package auth

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/brokercomms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
	"gitlab.com/ecp-ci/jacamar-ci/internal/seccomp"
)

var (
	// Function to facilitate associated process exit.
	sysExit, buildExit func()
)

// Start initializes the jacamar-auth application process, requires valid command arguments and
// custom executor provided environmental variables.
func Start(c arguments.ConcreteArgs) {
	ctx := context.Background()
	msg := logging.NewMessenger()

	defer exitPanic(c, msg)

	if c.Cleanup != nil {
		// Only fail cleanup during explicit cleanup errors, not
		// due to a repeat of an error from config.
		sysExit = func() { os.Exit(0) }
	}

	ae, err := establishAbstract(ctx, c, msg)
	if err != nil {
		// All errors encountered here will be considered system errors, even if they are
		// caused by a user influenced setting.
		errorhandling.MessageError(c, msg, err)
		sysExit()
		return
	}

	if err = (seccomp.Establish(c, ae.Cfg.Options())).Enable(); err != nil {
		preparations.StdError(c, err.Error(), ae.Msg, sysExit)
	}

	switch {
	case c.Config != nil:
		preparations.ConfigExec(ae, c, sysExit)
	case c.Prepare != nil:
		prepareExec(ae, c)
	case c.Run != nil:
		runExec(ae, c)
	case c.Cleanup != nil:
		cleanupExec(ae, c)
	default:
		preparations.StdError(c, "Invalid subcommand - See $ jacamar-auth --help", ae.Msg, sysExit)
	}
}

func establishAbstract(
	ctx context.Context,
	c arguments.ConcreteArgs,
	msg logging.Messenger,
) (ae *executors.AbstractExecutor, err error) {
	ae, err = preparations.JobContext(c, sysExit)
	if err != nil {
		return nil, errorhandling.NewJobCtxError(err)
	}

	ae.Msg = msg

	err = preparations.NewAuthorizedUser(ae, c)
	if err != nil {
		return nil, errorhandling.NewAuthError(err)
	}

	if ae.Cfg.Auth().Broker.URL != "" {
		err = brokerService(ae, c)
		if err != nil {
			return nil, errorhandling.NewBrokerError(err)
		}
	}

	cmdr, err := preparations.NewCommander(ae, ctx, c)
	if err != nil {
		return nil, errorhandling.NewRunMechanismError(err)
	}
	ae.Runner = basic.NewMechanism(cmdr, ae.Auth)

	return ae, nil
}

// brokerService updates an AbstractExecutor to establish and potentially override context relating
// to sensitive tokens in order to realize an optional broker service.
func brokerService(ae *executors.AbstractExecutor, c arguments.ConcreteArgs) error {
	if c.Config != nil {
		reg := brokercomms.NewJob(ae.Cfg.Auth().Broker, ae.Env.RequiredEnv, ae.Auth)

		if err := reg.PostJob(); err != nil {
			err = fmt.Errorf("failed to establish NewJob for broker service: %w", err)
			ae.SysLog.Error(err)
			return err
		}

		ae.Env.RequiredEnv.JobToken = reg.JobBrokerToken()
		ae.Env.StatefulEnv.BrokerToken = reg.JobBrokerToken()
		ae.SysLog.Debug("Successfully registered job with broker service")
	} else {
		ae.Env.RequiredEnv.JobToken = ae.Env.StatefulEnv.BrokerToken
	}

	return nil
}

func prepareExec(ae *executors.AbstractExecutor, c arguments.ConcreteArgs) {
	if (ae.Cfg.Auth()).RootDirCreation {
		if err := rootDirManagement(ae.Auth); err != nil {
			err = errorhandling.NewAuthError(fmt.Errorf("error managaing base directory: %w", err))
			errorhandling.MessageError(c, ae.Msg, err)
			sysExit()
		}
	}

	if err := ae.Runner.PipeOutput(""); err != nil {
		errMsg := fmt.Sprintf("Error executing prepare_exec: %s", err.Error())
		ae.SysLog.Debug(errMsg)
		preparations.StdError(c, errMsg, ae.Msg, buildExit)
		sysExit()
	}
}

func runExec(ae *executors.AbstractExecutor, c arguments.ConcreteArgs) {
	err := ae.Runner.TransferScript(c.Run.Script, c.Run.Stage, ae.Env, ae.Cfg.Auth())
	if err != nil {
		errMsg := fmt.Sprintf("Failed to transfer job script (%s): %s", c.Run.Stage, err.Error())
		ae.SysLog.Warning(errMsg)
		preparations.StdError(c, errMsg, ae.Msg, sysExit)
	}

	if err = ae.Runner.PipeOutput(""); err != nil {
		errMsg := fmt.Sprintf("Error executing run_exec: %s", err.Error())
		ae.SysLog.Debug(errMsg)
		preparations.StdError(c, errMsg, ae.Msg, buildExit)
		buildExit()
	}
}

func cleanupExec(ae *executors.AbstractExecutor, c arguments.ConcreteArgs) {
	if err := ae.Runner.PipeOutput(""); err != nil {
		errMsg := fmt.Sprintf("Error executing cleanup_exec: %s", err.Error())
		ae.SysLog.Debug(errMsg)
		preparations.StdError(c, errMsg, ae.Msg, buildExit)
		sysExit()
	}
}

// rootDirManagement creates/manages the root directory for an authorization user during
// the privileged authorization process.
func rootDirManagement(auth authuser.Authorized) error {
	dir := auth.BaseDir()
	info, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if os.IsNotExist(err) {
		/* #nosec */
		// directory 700 permissions required
		if err = os.Mkdir(dir, 0700); err != nil {
			return err
		}
	} else if info.Mode().String() != "drwx------" {
		/* #nosec */
		// directory 700 permissions required
		if err = os.Chmod(dir, 0700); err != nil {
			return err
		}
	}

	// enforce permissions
	uid, gid := auth.IntegerID()

	return os.Chown(dir, uid, gid)
}

func exitPanic(c arguments.ConcreteArgs, msg logging.Messenger) {
	if r := recover(); r != nil {
		err := fmt.Errorf("panic encountered: %v", r)
		errorhandling.MessageError(c, msg, errorhandling.NewPanicError(err))
		sysExit()
	}
}

func init() {
	// Initialize exit status in order to facilitate potential failures at any stage.
	sys, build := envparser.ExitCodes()
	sysExit = func() { os.Exit(sys) }
	buildExit = func() { os.Exit(build) }
}
