package auth

import (
	"context"
	"os"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type authTests struct {
	ae        *executors.AbstractExecutor
	c         arguments.ConcreteArgs
	opt       configure.Auth
	targetEnv map[string]string

	auth *mock_authuser.MockAuthorized
	msg  *mock_logging.MockMessenger

	assertError    func(*testing.T, error)
	assertAbstract func(*testing.T, *executors.AbstractExecutor)
	assertDir      func(*testing.T, string)
}

var (
	workingCfgEnv map[string]string
)

func init() {
	// override initialized exit behaviors
	sysExit = func() {}
	buildExit = func() {}

	workingCfgEnv = map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":     "1",
		"TRUSTED_CI_JOB_ID":                    "123",
		"TRUSTED_CI_JOB_TOKEN":                 "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":        "abc",
		"TRUSTED_CI_BUILDS_DIR":                "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":                 "/gitlab/cache",
		envparser.UserEnvPrefix + "CI_JOB_JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":                "gitlab.url",
	}
}

func Test_establishAbstract(t *testing.T) {
	var tarErr errorhandling.ObfuscatedErr

	ctx := context.Background()
	cancelCtx, cancelFun := context.WithCancel(ctx)
	defer cancelFun()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	tests := map[string]authTests{
		"config_exec - failed to establish job context": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			msg: m,
			assertError: func(t *testing.T, err error) {
				assert.True(t, errorhandling.IsObfuscatedErr(err, &tarErr), "expect obfuscated error type")
			},
		},
		"config_exec - failed to authorize user": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/auth_test_config.toml",
				},
			},
			msg:       m,
			targetEnv: workingCfgEnv,
			assertError: func(t *testing.T, err error) {
				assert.True(t, errorhandling.IsObfuscatedErr(err, &tarErr), "expect obfuscated error type")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got, err := establishAbstract(cancelCtx, tt.c, tt.msg)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, got)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_rootDirManagement(t *testing.T) {
	cur, _ := user.Current()
	if cur.Uid != "0" {
		t.Skip("rootDirManagement tests must be run as root")
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// create new directory
	newDir := t.TempDir()
	create := mock_authuser.NewMockAuthorized(ctrl)
	create.EXPECT().BaseDir().Return(newDir).AnyTimes()
	create.EXPECT().IntegerID().Return(0, 0)

	// chmod on existing directory
	chmodDir := t.TempDir()
	_ = os.Chmod(chmodDir, 0770)
	existing := mock_authuser.NewMockAuthorized(ctrl)
	existing.EXPECT().BaseDir().Return(chmodDir).AnyTimes()
	existing.EXPECT().IntegerID().Return(0, 0)

	tests := map[string]authTests{
		"create new directory": {
			auth: create,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDir: func(t *testing.T, dir string) {
				info, err := os.Stat(dir)
				assert.NoError(t, err, "unable to stat directory")
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
		"chmod on existing directory": {
			auth: existing,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDir: func(t *testing.T, dir string) {
				info, err := os.Stat(dir)
				assert.NoError(t, err, "unable to stat directory")
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := rootDirManagement(tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertDir != nil {
				tt.assertDir(t, tt.auth.BaseDir())
			}
		})
	}
}
