package preparations

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/go-playground/validator/v10"
	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/version"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/rules"
)

// proj is the struct for excepted json STDOUT in config_exec.
type proj struct {
	Builds   string            `json:"builds_dir"`
	Cache    string            `json:"cache_dir"`
	Share    bool              `json:"builds_dir_is_shared"`
	Hostname string            `json:"hostname"`
	State    map[string]string `json:"job_env"`
	Driver   map[string]string `json:"driver"`
}

// ConfigExec realizes the config_exec custom executor stage. It will run without leveraging
// any authorized user, instead valid JSON payload is created for the upstream runner to ingest.
// No local system changes should made as part of this stage.
func ConfigExec(ae *executors.AbstractExecutor, c arguments.ConcreteArgs, exit func()) {
	js, err := configJSON(ae)
	if err != nil {
		errMsg := fmt.Sprintf("unable to construct job configuration payload: %s", err.Error())
		StdError(c, errMsg, ae.Msg, exit)
	}

	if ae.Cfg.General().VariableDataDir && strings.Contains(ae.Auth.BaseDir(), "$") {
		// Admin defined variable data_dir need resolved.
		js, err = expandBaseDir(ae, js)
		if err != nil {
			errMsg := fmt.Sprintf("error encountered expanding data_dir: %s", err.Error())
			StdError(c, errMsg, ae.Msg, exit)
		}
	}

	ae.Msg.Stdout(js) // Print configuration.
	ae.SysLog.Debug("Configuration payload established.")
}

// configJson encodes the included proj struct as json and returns it as a string for stdout.
// The format matches the target STDOUT outlined in the gitlab custom executor.
func configJSON(ae *executors.AbstractExecutor) (string, error) {
	host, err := os.Hostname()
	if err != nil {
		return "", err
	}

	state, err := buildState(ae)
	if err != nil {
		return "", err
	}

	// Configuration (managed outside of StatefulEnv structure)
	state[configure.EnvVariable] = ae.ExecOptions

	p := &proj{
		Builds:   ae.Runner.BuildsDir(),
		Cache:    ae.Runner.CacheDir(),
		Share:    false,
		Hostname: host,
		State:    state,
		Driver: map[string]string{
			"name":    "Jacamar CI",
			"version": version.Version(),
		},
	}

	data, err := json.Marshal(p)
	if err != nil {
		return "", err
	}

	return string(data), nil
}

// buildState leverages the ValidUser structure to generates a mapping of values to
// StatefulEnv keys that will be registered as part of the custom executors stateful
// variable management.
func buildState(ae *executors.AbstractExecutor) (map[string]string, error) {
	state := ae.Auth.BuildState()
	state.BrokerToken = ae.Env.StatefulEnv.BrokerToken

	env := make(map[string]string)

	t := reflect.TypeOf(&state).Elem()
	v := reflect.ValueOf(&state).Elem()
	for i := 0; i < t.NumField(); i++ {
		fieldT := t.Field(i)
		fieldV := v.Field(i)

		tag := fieldT.Tag.Get(envparser.UserVarTagName)
		req := fieldT.Tag.Get(envparser.RequiredKey)

		if req == "true" && fieldV.String() == "" {
			return env, fmt.Errorf("unable to identify required stateful variable %s", tag)
		} else if req == "false" && fieldV.String() == "" {
			continue
		}

		env[tag] = fieldV.String()
	}

	return env, nil
}

var envCmdr func(*executors.AbstractExecutor, string) (command.Commander, error)

// expandBaseDir resolves last minute modifications to the encoded javascript prior
// to reporting. Errors encountered in this operations must be observed as a failed job.
func expandBaseDir(ae *executors.AbstractExecutor, js string) (string, error) {
	dataDir := filepath.Clean(strings.TrimSpace(ae.Cfg.General().DataDir))

	cmdr, err := envCmdr(ae, dataDir)
	if err != nil {
		return "", err
	}

	out, err := cmdr.ReturnOutput("")
	if err != nil {
		return "", fmt.Errorf("encountered during command execution: %w", err)
	}

	out = filepath.Clean(strings.TrimSpace(out))
	if err = checkDirectory(out, dataDir); err != nil {
		return "", err
	}

	js = strings.ReplaceAll(js, dataDir, out)

	return js, nil
}

func checkDirectory(expandedDir, dataDir string) (err error) {
	v := validator.New()
	params := struct {
		Dir string `validate:"directory"`
	}{
		Dir: expandedDir,
	}
	_ = v.RegisterValidation("directory", rules.CheckDirectory)
	if err = v.Struct(&params); err != nil {
		return err
	}

	// Basic check again egregious error in which no variable(s)
	// had been properly expanded by the user environment.
	unExpanded := os.Expand(dataDir, func(s string) string {
		return ""
	})
	if unExpanded == expandedDir {
		err = errors.New("unexpanded variable detected")
	}

	return err
}
