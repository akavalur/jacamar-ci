package preparations

import (
	"context"
	"io/ioutil"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type prepTests struct {
	ae        *executors.AbstractExecutor
	c         arguments.ConcreteArgs
	message   string
	reqBool   bool
	targetEnv map[string]string

	msg logging.Messenger

	mockEnvCmdr func(*testing.T)

	assertAbstract  func(*testing.T, *executors.AbstractExecutor)
	assertCommander func(*testing.T, command.Commander)
	assertError     func(*testing.T, error)
	assertMap       func(*testing.T, map[string]string)
}

func TestStdError(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Stderr("Error encountered during job: %s", "cleanup").Times(1)
	m.EXPECT().Stderr("Error encountered during job: %s", "config").Times(1)
	m.EXPECT().Warn("Error encountered during job: %s", "prepare").Times(1)
	m.EXPECT().Warn("Error encountered during job: %s", "run").Times(1)

	tests := map[string]prepTests{
		"error encountered during cleanup": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			message: "cleanup",
			msg:     m,
		},
		"error encountered during run": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			message: "run",
			msg:     m,
		},
		"error encountered during prepare": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			message: "prepare",
			msg:     m,
		},
		"error encountered during config": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			message: "config",
			msg:     m,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			StdError(tt.c, tt.message, tt.msg, func() {})
		})
	}
}

func TestJobContext(t *testing.T) {
	workingContext := map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":     "1",
		"TRUSTED_CI_JOB_ID":                    "123",
		"TRUSTED_CI_JOB_TOKEN":                 "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":        "abc",
		"TRUSTED_CI_BUILDS_DIR":                "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":                 "/gitlab/cache",
		envparser.UserEnvPrefix + "CI_JOB_JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":                "gitlab.url",
	}

	tests := map[string]prepTests{
		"config_exec - invalid required environments": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/valid_exec_config.toml",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"config_exec - invalid configuration file": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			targetEnv: workingContext,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to establish Configurer: error executing ReadFile(), open /missing/file/config.toml: no such file or directory",
				)
			},
		},
		"config_exec - valid configuration and environment": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/valid_exec_config.toml",
				},
			},
			targetEnv: workingContext,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			_, err := JobContext(tt.c, func() {})
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewAuthorizedUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	curUser, _ := authuser.CurrentUser()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Options().Return(configure.Options{}).AnyTimes()

	invalidRunAs := mock_configure.NewMockConfigurer(ctrl)
	invalidRunAs.EXPECT().Options().Return(configure.Options{
		Auth: configure.Auth{
			RunAs: configure.RunAs{
				ValidationScript: "/some/file.bash",
				RunAsVariable:    "RUNAS",
			},
		},
	})

	_ = os.Setenv("CUSTOM_ENV_RUNAS", "$(bad)")
	defer os.Unsetenv("CUSTOM_ENV_RUNAS")

	ll := logrus.New()
	ll.Out = ioutil.Discard
	sysLog := logrus.NewEntry(ll)

	tests := map[string]prepTests{
		"process authorized user from available state": {
			ae: &executors.AbstractExecutor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: curUser.Username,
					},
				},
				SysLog: sysLog,
			},
			c: arguments.ConcreteArgs{
				NoAuth: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.NotNil(t, ae)
				assert.Equal(t, curUser.Username, ae.Auth.Username())
			},
		},
		"failed to process authorized user, invalid state": {
			ae: &executors.AbstractExecutor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "1-invalid-user",
					},
				},
				SysLog: sysLog,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"failed to establish validator": {
			ae: &executors.AbstractExecutor{
				Cfg: invalidRunAs,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "failed to establish RunAs validator: invalid user provided account (defined by RUNAS variable)")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			// Only state is analyzed at this time, static 'false'
			err := NewAuthorizedUser(tt.ae, tt.c)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAbstract != nil {
				tt.assertAbstract(t, tt.ae)
			}
		})
	}
}

func Test_stateReq(t *testing.T) {
	// Maintaining the state of an authorized user as well as job specific configurations
	// is vita. We need enforce the mechanisms used to determine if state is required.

	tests := map[string]prepTests{
		"config_exec stage": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			reqBool: false,
		},
		"prepare_exec stage": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			reqBool: true,
		},
		"run_exec stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			reqBool: true,
		},
		"cleanup_exec stage, configuration defined": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "some.toml",
				},
			},
			reqBool: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := stateReq(tt.c)

			assert.Equal(t, tt.reqBool, got, "expected state?")
		})
	}
}

func Test_stageConfig(t *testing.T) {
	tests := map[string]prepTests{
		"expect config_exec configuration": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.Equal(t, ae.Stage, "config_exec")
			},
		},
		"expected prepare_exec configuration": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.Equal(t, ae.Stage, "prepare_exec")
			},
		},
		"expect run_exec configuration": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Stage:  "stage_name",
					Script: "script.bash",
				},
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.Equal(t, ae.Stage, "stage_name")
				assert.Equal(t, ae.ScriptPath, "script.bash")
			},
		},
		"expected cleanup_exec configuration": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.Equal(t, ae.Stage, "cleanup_exec")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ae := &executors.AbstractExecutor{}
			stageConfig(ae, tt.c)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, ae)
			}
		})
	}
}

func Test_NewCommander(t *testing.T) {
	ctx := context.Background()
	cancelCtx, cancelFun := context.WithCancel(ctx)
	defer cancelFun()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Mock messaging
	msg := mock_logging.NewMockMessenger(ctrl)

	//  Mock configurations
	missing := mock_configure.NewMockConfigurer(ctrl)
	missing.EXPECT().Auth().Return(
		configure.Auth{},
	).AnyTimes()
	missing.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	none := mock_configure.NewMockConfigurer(ctrl)
	none.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "none",
		},
	).AnyTimes()
	none.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	su := mock_configure.NewMockConfigurer(ctrl)
	su.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "su",
		},
	).AnyTimes()
	su.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	sudo := mock_configure.NewMockConfigurer(ctrl)
	sudo.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "sudo",
		},
	).AnyTimes()
	sudo.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	setgid := mock_configure.NewMockConfigurer(ctrl)
	setgid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "setgid",
		},
	).AnyTimes()
	setgid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	setuid := mock_configure.NewMockConfigurer(ctrl)
	setuid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "setuid",
		},
	).AnyTimes()
	setuid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	// Mock authorizations
	pass := mock_authuser.NewMockAuthorized(ctrl)
	pass.EXPECT().Username().Return("user").AnyTimes()
	pass.EXPECT().HomeDir().Return("/home/user").AnyTimes()
	pass.EXPECT().BuildsDir().Return("/home/user/builds").AnyTimes()
	pass.EXPECT().CacheDir().Return("/home/user/cache").AnyTimes()
	pass.EXPECT().IntegerID().Return(1000, 1000).AnyTimes()
	pass.EXPECT().Groups().Return([]uint32{1000}).AnyTimes()

	// Mock concrete arguments
	authPrepare := arguments.ConcreteArgs{
		Prepare: &arguments.PrepareCmd{},
	}
	noAuthPrepare := arguments.ConcreteArgs{
		Prepare: &arguments.PrepareCmd{},
		NoAuth:  true,
	}

	tests := map[string]prepTests{
		"no configuration during authorization": {
			ae: &executors.AbstractExecutor{
				Cfg: missing,
				Msg: msg,
			},
			c: authPrepare,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid runner configuration, ensure supported downscope is defined")
			},
		},
		"no configuration during no-auth start": {
			ae: &executors.AbstractExecutor{
				Auth: pass,
				Cfg:  missing,
				Msg:  msg,
			},
			c:           noAuthPrepare,
			assertError: jacamartst.AssertNoError,
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, "/usr/bin/env", cmd.CopiedCmd().Path)
			},
		},
		"none downscope mechanism defined": {
			ae: &executors.AbstractExecutor{
				Cfg:  none,
				Auth: pass,
			},
			c:           authPrepare,
			assertError: jacamartst.AssertNoError,
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, "/usr/bin/jacamar", cmd.CopiedCmd().Path)
			},
		},
		"su downscope mechanism defined": {
			ae: &executors.AbstractExecutor{
				Cfg:  su,
				Auth: pass,
			},
			c:           authPrepare,
			assertError: jacamartst.AssertNoError,
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Contains(t, cmd.CopiedCmd().Path, "/bin/su")
			},
		},
		"sudo downscope mechanism defined": {
			ae: &executors.AbstractExecutor{
				Cfg:  sudo,
				Auth: pass,
			},
			c:           authPrepare,
			assertError: jacamartst.AssertNoError,
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Contains(t, cmd.CopiedCmd().Path, "sudo")
			},
		},
		"misspelled downscope in configuration with authorization": {
			ae: &executors.AbstractExecutor{
				Cfg: setgid,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid runner configuration, ensure supported downscope is defined")
			},
		},
		"configuration, setuid run mechanism": {
			ae: &executors.AbstractExecutor{
				Cfg:  setuid,
				Auth: pass,
			},
			c:           authPrepare,
			assertError: jacamartst.AssertNoError,
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, "/usr/bin/jacamar", cmd.CopiedCmd().Path)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			cmd, err := NewCommander(tt.ae, cancelCtx, tt.c)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertCommander != nil {
				tt.assertCommander(t, cmd)
			}
		})
	}
}

func Test_buildEnvCmdr(t *testing.T) {
	ctx := context.Background()
	cancelCtx, cancelFun := context.WithCancel(ctx)
	defer cancelFun()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Mock messaging
	msg := mock_logging.NewMockMessenger(ctrl)

	//  Mock configurations
	missing := mock_configure.NewMockConfigurer(ctrl)
	missing.EXPECT().Auth().Return(
		configure.Auth{},
	).AnyTimes()
	missing.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	setuid := mock_configure.NewMockConfigurer(ctrl)
	setuid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "setuid",
		},
	).AnyTimes()
	setuid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	// Mock authorizations
	pass := mock_authuser.NewMockAuthorized(ctrl)
	pass.EXPECT().Username().Return("user").AnyTimes()
	pass.EXPECT().HomeDir().Return("/home/user").AnyTimes()
	pass.EXPECT().BuildsDir().Return("/home/user/builds").AnyTimes()
	pass.EXPECT().CacheDir().Return("/home/user/cache").AnyTimes()
	pass.EXPECT().IntegerID().Return(1000, 1000).AnyTimes()
	pass.EXPECT().Groups().Return([]uint32{1000}).AnyTimes()

	runner := mock_runmechanisms.NewMockRunner(ctrl)
	runner.EXPECT().RequestContext().Return(cancelCtx).AnyTimes()

	tests := map[string]prepTests{
		"no configuration during authorization": {
			ae: &executors.AbstractExecutor{
				Cfg:    missing,
				Msg:    msg,
				Runner: runner,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid runner configuration, ensure supported downscope is defined")
			},
		},
		"configuration, setuid run mechanism": {
			ae: &executors.AbstractExecutor{
				Cfg:    setuid,
				Auth:   pass,
				Msg:    msg,
				Runner: runner,
			},
			message:     "$TEST",
			assertError: jacamartst.AssertNoError,
			assertCommander: func(t *testing.T, cmd command.Commander) {
				copied := cmd.CopiedCmd()
				assert.Equal(t, "/usr/bin/jacamar", copied.Path)
				assert.Contains(t, copied.Args, "env")
				assert.Contains(t, copied.Args, "'$TEST'")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			cmd, err := buildEnvCmdr(tt.ae, tt.message)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertCommander != nil {
				tt.assertCommander(t, cmd)
			}
		})
	}
}
