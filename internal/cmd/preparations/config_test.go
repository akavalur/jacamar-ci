package preparations

import (
	"errors"
	"io/ioutil"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_command"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func TestConfigExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfgNoVar := mock_configure.NewMockConfigurer(ctrl)
	cfgNoVar.EXPECT().General().Return(configure.General{
		VariableDataDir: false,
	}).AnyTimes()

	runner := mock_runmechanisms.NewMockRunner(ctrl)
	runner.EXPECT().BuildsDir().Return("/builds").AnyTimes()
	runner.EXPECT().CacheDir().Return("/cache").AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Stderr(gomock.Any(), gomock.Eq("error encountered expanding data_dir: encountered during command execution: error message"))
	msg.EXPECT().Stderr(gomock.Any(), gomock.Eq("error encountered expanding data_dir: unexpanded variable detected"))
	msg.EXPECT().Stderr(gomock.Any(), gomock.Eq("error encountered expanding data_dir: Key: 'Dir' Error:Field validation for 'Dir' failed on the 'directory' tag"))
	// We have other more comprehensive tests that verify stateful
	// variables are properly formatted in the workflow. In this case
	// we need to just ensure that stdout is generated upon success.
	msg.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()
	msg.EXPECT().Stderr(gomock.Any(), gomock.Any()).AnyTimes()

	ll := logrus.New()
	ll.Out = ioutil.Discard
	sysLog := logrus.NewEntry(ll)

	auth := mock_authuser.NewMockAuthorized(ctrl)
	auth.EXPECT().BuildState().Return(envparser.StatefulEnv{
		BaseDir:     "/base",
		BuildsDir:   "/builds",
		CacheDir:    "/cache",
		ScriptDir:   "/script",
		Username:    "user",
		ProjectPath: "group/project",
	}).AnyTimes()
	auth.EXPECT().BuildsDir().Return("/builds").AnyTimes()

	failAuth := mock_authuser.NewMockAuthorized(ctrl)
	failAuth.EXPECT().BuildState().Return(envparser.StatefulEnv{}).AnyTimes()
	failAuth.EXPECT().BuildsDir().Return("/builds").AnyTimes()

	cfgVar := mock_configure.NewMockConfigurer(ctrl)
	cfgVar.EXPECT().General().Return(configure.General{
		DataDir:         "$GPFS_TEST_DIR/.jacamar",
		VariableDataDir: true,
	}).AnyTimes()

	authVar := mock_authuser.NewMockAuthorized(ctrl)
	authVar.EXPECT().BuildState().Return(envparser.StatefulEnv{
		BaseDir:     "$GPFS_TEST_DIR/.jacamar/user",
		BuildsDir:   "$GPFS_TEST_DIR/.jacamar/user/builds",
		CacheDir:    "$GPFS_TEST_DIR/.jacamar/user/cache",
		ScriptDir:   "$GPFS_TEST_DIR/.jacamar/user/script",
		Username:    "user",
		ProjectPath: "group/project",
	}).AnyTimes()
	authVar.EXPECT().BaseDir().Return("$GPFS_TEST_DIR/.jacamar/user").AnyTimes()

	runnerVar := mock_runmechanisms.NewMockRunner(ctrl)
	runnerVar.EXPECT().BuildsDir().Return("$GPFS_TEST_DIR/.jacamar/user/builds").AnyTimes()
	runnerVar.EXPECT().CacheDir().Return("$GPFS_TEST_DIR/.jacamar/user/cache").AnyTimes()

	tests := map[string]prepTests{
		"configuration prep success": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			ae: &executors.AbstractExecutor{
				Auth:   auth,
				Cfg:    cfgNoVar,
				Msg:    msg,
				Runner: runner,
				SysLog: sysLog,
			},
		},
		"missing stateful variables during build": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			ae: &executors.AbstractExecutor{
				Auth:   failAuth,
				Cfg:    cfgNoVar,
				Msg:    msg,
				SysLog: sysLog,
			},
		},
		"successfully expand data_dir": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			ae: &executors.AbstractExecutor{
				Auth:   authVar,
				Cfg:    cfgVar,
				Msg:    msg,
				Runner: runner,
				SysLog: sysLog,
			},
			mockEnvCmdr: func(t *testing.T) {
				envCmdr = func(executor *executors.AbstractExecutor, s string) (command.Commander, error) {
					passCmdr := mock_command.NewMockCommander(ctrl)
					passCmdr.EXPECT().ReturnOutput(gomock.Eq("")).Return("/ci-test", nil)
					return passCmdr, nil
				}
			},
		},
		"error encountered expanding data_dir": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			ae: &executors.AbstractExecutor{
				Auth:   authVar,
				Cfg:    cfgVar,
				Msg:    msg,
				Runner: runner,
				SysLog: sysLog,
			},
			mockEnvCmdr: func(t *testing.T) {
				envCmdr = func(executor *executors.AbstractExecutor, s string) (command.Commander, error) {
					failCmdr := mock_command.NewMockCommander(ctrl)
					failCmdr.EXPECT().ReturnOutput(gomock.Eq("")).Return("", errors.New("error message"))
					return failCmdr, nil
				}
			},
		},
		"unresolved data directory": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			ae: &executors.AbstractExecutor{
				Auth:   authVar,
				Cfg:    cfgVar,
				Msg:    msg,
				Runner: runner,
				SysLog: sysLog,
			},
			mockEnvCmdr: func(t *testing.T) {
				envCmdr = func(executor *executors.AbstractExecutor, s string) (command.Commander, error) {
					missCmdr := mock_command.NewMockCommander(ctrl)
					missCmdr.EXPECT().ReturnOutput(gomock.Eq("")).Return("/.jacamar", nil)
					return missCmdr, nil
				}
			},
		},
		"invalid characters in expanded directory": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			ae: &executors.AbstractExecutor{
				Auth:   authVar,
				Cfg:    cfgVar,
				Msg:    msg,
				Runner: runner,
				SysLog: sysLog,
			},
			mockEnvCmdr: func(t *testing.T) {
				envCmdr = func(executor *executors.AbstractExecutor, s string) (command.Commander, error) {
					invalidCmdr := mock_command.NewMockCommander(ctrl)
					invalidCmdr.EXPECT().ReturnOutput(gomock.Eq("")).Return("$(example)/ci", nil)
					return invalidCmdr, nil
				}
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockEnvCmdr != nil {
				tt.mockEnvCmdr(t)
			}

			ConfigExec(tt.ae, tt.c, func() {})
		})
	}
}

func Test_buildState(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	passAuth := mock_authuser.NewMockAuthorized(ctrl)
	passAuth.EXPECT().BuildState().Return(envparser.StatefulEnv{
		BaseDir:     "/base",
		BuildsDir:   "/builds",
		CacheDir:    "/cache",
		ScriptDir:   "/script",
		Username:    "user",
		ProjectPath: "group/project",
	}).Times(1)

	failAuth := mock_authuser.NewMockAuthorized(ctrl)
	failAuth.EXPECT().BuildState().Return(envparser.StatefulEnv{})

	tests := map[string]prepTests{
		"missing stateful variables during build": {
			ae: &executors.AbstractExecutor{
				Auth: failAuth,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"completed stateful variables identified": {
			ae: &executors.AbstractExecutor{
				Auth: passAuth,
			},
			assertMap: func(t *testing.T, m map[string]string) {
				req := map[string]string{
					"JACAMAR_CI_AUTH_USERNAME": "user",
					"JACAMAR_CI_BASE_DIR":      "/base",
					"JACAMAR_CI_BUILDS_DIR":    "/builds",
					"JACAMAR_CI_CACHE_DIR":     "/cache",
					"JACAMAR_CI_SCRIPT_DIR":    "/script",
					"JACAMAR_CI_PROJECT_PATH":  "group/project",
				}
				assert.Equal(t, req, m)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := buildState(tt.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertMap != nil {
				tt.assertMap(t, got)
			}
		})
	}
}
