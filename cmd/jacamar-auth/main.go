package main

import (
	"os"

	"github.com/alexflint/go-arg"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/auth"
	"gitlab.com/ecp-ci/jacamar-ci/internal/version"
)

type args struct {
	// Core executor stages
	Config  *arguments.ConfigCmd  `arg:"subcommand:config" help:"Execute configuration stage (config_exec)"`
	Prepare *arguments.PrepareCmd `arg:"subcommand:prepare" help:"Execute preparation stage (prepare_exec)"`
	Run     *arguments.RunCmd     `arg:"subcommand:run" help:"Execute run stage (run_exec)"`
	Cleanup *arguments.CleanupCmd `arg:"subcommand:cleanup" help:"Execute the cleanup stage (cleanup_exec)"`

	// Argument flags
	UnobfuscatedError bool `arg:"-u, --unobfuscated" help:"By default all authorization level error message are obfuscated form the user, this is not required for standard deployments. This flag will remove this default behavior."`
}

func (args) Version() string {
	return version.Obtain()
}

func main() {
	var op args
	arg.MustParse(&op)

	auth.Start(arguments.ConcreteArgs{
		Config:            op.Config,
		Prepare:           op.Prepare,
		Run:               op.Run,
		Cleanup:           op.Cleanup,
		UnobfuscatedError: op.UnobfuscatedError,
	})

	os.Exit(0)
}
