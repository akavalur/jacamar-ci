package main

import (
	"log"
	"os"

	"github.com/alexflint/go-arg"
	"gitlab.com/ecp-ci/jacamar-ci/internal/utils"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/jacamar"
	"gitlab.com/ecp-ci/jacamar-ci/internal/version"
)

type args struct {
	// Core executor stages
	Config  *arguments.ConfigCmd  `arg:"subcommand:config" help:"Execute configuration stage (config_exec)"`
	Prepare *arguments.PrepareCmd `arg:"subcommand:prepare" help:"Execute preparation stage (prepare_exec)"`
	Run     *arguments.RunCmd     `arg:"subcommand:run" help:"Execute run stage (run_exec)"`
	Cleanup *arguments.CleanupCmd `arg:"subcommand:cleanup" help:"Execute the cleanup stage (cleanup_exec)"`

	// Additionally supported sub-commands
	Translate *arguments.TranslateCmd `arg:"subcommand:translate" help:"Translate a configuration TOML file previously used with the Runner fork to that of a supported Jacamar configuration."`
	Env       *arguments.EnvCmd       `arg:"subcommand:env" help:"Programmatic support for retrieving and resolving variables in a user's remote environment (for use with --no-auth only)."`

	// Argument flags
	NoAuth bool `arg:"-n, --no-auth" help:"Skip all authorization level functionality (controlled via the [auth] table in configuration). This is only advised if either downscoping has already occurred or jobs are meant to execute under the runner user."`
}

func (args) Version() string {
	return version.Obtain()
}

func main() {
	var op args
	arg.MustParse(&op)

	concrete := arguments.ConcreteArgs{
		Config:    op.Config,
		Prepare:   op.Prepare,
		Run:       op.Run,
		Cleanup:   op.Cleanup,
		Translate: op.Translate,
		EnvCmd:    op.Env,
		NoAuth:    op.NoAuth,
	}

	if op.Translate != nil || op.Env != nil {
		utils.Run(concrete)
		os.Exit(0)
	}

	if op.NoAuth {
		jacamar.Start(concrete)
	} else {
		// Avoid configurations where jacamar is used as opposed to jacamar-auth, leading
		// to a lack of downscoping.
		log.Fatal("Jacamar requires --no-auth in order to support execution requirements.")
	}

	os.Exit(0)
}
