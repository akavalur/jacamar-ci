%{!?_version: %define _version 13.12.0 }
%{!?_ref: %define _ref 7a6612da06043f908b740629bbe3f0d9c59a5dad }
%undefine _missing_build_ids_terminate_build

Name:           gitlab-runner-ecp
Version:        %{_version}
Release:        1%{?dist}
Summary:        GitLab Runner %{version} with ECP supported patches.
Vendor:         Exascale Computing Project - 2.4.4
Packager:       ECP CI Infrastructure <ecp-ci-infrastructure@elist.ornl.gov>

# License details: https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/LICENSE
License:        MIT
URL:            https://gitlab.com/gitlab-org/gitlab-runner
Patch0:         runner_trusted.patch

# Go is required for build but a newer version (see go.mod)
# than is available on most distributions.
BuildRequires:  bash
BuildRequires:  curl
BuildRequires:  make
BuildRequires:  tar

Requires:       curl
Requires:       git
Requires:       tar

Conflicts:      gitlab-runner-patched
Conflicts:      gitlab-runner-beta

Provides:       gitlab-runner-patched = %{version}-1

%define  gitDir  %{name}-%{version}

%description
The GitLab Runner is used in conjunctions with a GitLab server to
execute CI/CD jobs and convey ongoing results back. This release
contains ECP focused patches that enable support for the custom
executor driver Jacamar CI.

%prep

rm -rf %{gitDir}
git clone %{url}.git %{gitDir}
cd %{gitDir}
git checkout %{_ref} -b v%{version}

%patch0 -p1

%build

cd %{gitDir}
make runner-bin-host VERSION=%{version}.ecp

%install

mkdir -p %{buildroot}%{_bindir}

cd %{gitDir}
install -m 755 out/binaries/gitlab-runner %{buildroot}%{_bindir}/gitlab-runner-patched
ln -sf %{_bindir}/gitlab-runner-patched %{buildroot}%{_bindir}/gitlab-runner

%files

%defattr(-, root, root, -)
%{_bindir}
