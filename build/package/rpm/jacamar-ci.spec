%{!?_version: %define _version 0.7.0 }

# https://github.com/rpm-software-management/rpm/blob/master/macros.in
# build_id links are generated only when the __debug_package
# global is defined.
%define _build_id_links alldebug
%undefine _missing_build_ids_terminate_build

Name:           jacamar-ci
Version:        %{_version}
Release:        1%{?dist}
Summary:        HPC focused CI/CD driver for the GitLab custom executor model.
Vendor:         Exascale Computing Project - 2.4.4
Packager:       ECP CI Infrastructure <ecp-ci-infrastructure@elist.ornl.gov>

# License details: https://gitlab.com/ecp-ci/jacamar-ci/-/blob/develop/LICENSE
License:        MIT/Apache 2.0
URL:            https://gitlab.com/ecp-ci/%{name}
Source0:        https://gitlab.com/ecp-ci/%{name}/-/archive/v%{version}/%{name}-v%{version}.tar.gz

# Go is required for build but a newer version (see go.mod)
# than is available on most distributions.
BuildRequires:  bash
BuildRequires:  git
BuildRequires:  glibc
BuildRequires:  libseccomp
BuildRequires:  make

Requires:       bash
Requires:       gitlab-runner-ecp


%global debug_package %{nil}

%description
Jacamar CI is an open source project designed to provide a bridge
between traditional GitLab CI pipelines and HPC resources. All this
is accomplished by first conforming to the supported custom executor
model for executing jobs. Beyond this, Jacamar, manages additional
responsibilities; user authorization, downscoping mechanisms,
scheduler interactions, and a robust set of administrative
configuration options.

%prep
%if %{defined _sha256sum}
echo "%{_sha256sum}  %SOURCE0" | sha256sum -c -
%endif
%setup -q -n %{name}-v%{version}

%build

make build VERSION=%{version}

%install

mkdir -p %{buildroot}%{_bindir}

install -m 700 binaries/jacamar-auth %{buildroot}%{_bindir}
install -m 755 binaries/jacamar %{buildroot}%{_bindir}

%files

%attr(0700, root, root) %{_bindir}/jacamar-auth
%attr(0755, root, root) %{_bindir}/jacamar
