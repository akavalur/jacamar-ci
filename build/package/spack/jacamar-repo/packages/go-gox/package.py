# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os
from spack import *


class GoGox(Package):
    """A dead simple, no frills Go cross compile tool."""

    homepage = "https://github.com/mitchellh/gox"
    url      = "https://github.com/mitchellh/gox/archive/v1.0.1.tar.gz"
    git      = "https://github.com/mitchellh/gox.git"

    maintainers = ['paulbry']

    version('1.0.1', sha256='25aab55a4ba75653931be2a2b95e29216b54bd8fecc7931bd416efe49a388229')

    depends_on('go', type=('build'))

    def install(self, spec, prefix):
        env['GOPATH'] = '{0}/go-path'.format(self.stage.source_path)
        mkdir(env['GOPATH'])
        go = which('go')
        mkdir(prefix.bin)
        go('build', '-o', '{0}/gox'.format(prefix.bin))

    def test(self):
        """ Perform smoke test on the installation."""
        self.run_test('gox', ['--help'], [], status=0,
                      installed=True, purpose="test installation of Gox",
                      skip_missing=False, work_dir='.')
