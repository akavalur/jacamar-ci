# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os
import platform
import re
from spack import *

class GitlabRunner(MakefilePackage):
    """GitLab Runner is the open source project that is used to run your CI/CD jobs and send the results back to GitLab."""

    homepage = "https://gitlab.com/gitlab-org/gitlab-runner"
    url      = "https://gitlab.com/gitlab-org/gitlab-runner/-/archive/v13.2.1/gitlab-runner-v13.2.1.tar.gz"
    git      = "https://gitlab.com/gitlab-org/gitlab-runner.git"

    maintainers = ['paulbry']
    executables = ['gitlab-runner']

    # Using Git tagged releases as opposed tar going forward avoids issue found in version/revision identification.
    version('master', branch='master')
    version('13.12.0', commit='7a6612da06043f908b740629bbe3f0d9c59a5dad')
    version('13.11.0', sha256='7bc15d89f7b0551c4dd236d3ef846cf7840175fa1638fa58d0ccd12f3c04a56b')
    version('13.10.0', sha256='f179d6c51867c2a7dcda4a537d152214b25734f78dcfb7bb05fe07b67c1a9b17')
    version('13.8.0', sha256='0b573176b26a025a6ac0bbb91cbeb77ce544597b76a5ee53cadae717b66100c9')
    version('13.7.0', sha256='543d7849d887fcfdabb22ef768e720d34d558fbc0c73860849ceb042b3927b50')
    version('13.6.0', sha256='ae40744bb94f59ef91ec81ca79d79cd126daab865f8a342d1116075f263d7f29')

    depends_on('go@1.15:', type=('build'))
    depends_on('go-gox', type=('build'))
    depends_on('git', type=('run'))
    depends_on('tar', type=('run'))
    
    variant('jacamar', default=False, description='Support for Jacamar custom executor driver.')
    patch('jacamar_b28b9ee27.patch', when='+jacamar',
          sha256='238e97dd4cbca9d3ea36c2b590450e5d2491786164f41c923ee9bb7e8618dbc6')

    @classmethod
    def determine_spec_details(cls, prefix, exes_in_prefix):
        exe_to_path = dict(
            (os.path.basename(p), p) for p in exes_in_prefix
        )
        if 'gitlab-runner' not in exe_to_path:
            return None

        runner = spack.util.executable.Executable(exe_to_path['gitlab-runner'])
        output = runner('--version', output=str)
        if output:
            match = re.search(r'Version:\s*(\S+)', output)
            if match:
                version_str = match.group(1)
                return Spec('gitlab-runner@{0}'.format(version_str))

    def build(self, spec, prefix):
        env['GOPATH'] = '{0}/go-path'.format(self.stage.source_path)
        mkdir(env['GOPATH'])
        if platform.machine() == 'aarch64':
            make('runner-bin-host', 'VERSION={0}'.format(spec.version), 'ARCH=arm64')
        else:
            make('runner-bin-host', 'VERSION={0}'.format(spec.version))
    
    def install(self, spec, prefix):
        mkdirp(prefix.bin)
        cp = which("cp")
        cp('-f', 'out/binaries/gitlab-runner', prefix.bin)

    def test(self):
        """ Perform smoke test on the installation."""
        self.run_test('gitlab-runner', ['--version'], [], status=0,
                      installed=True, purpose="test installation of GitLab-Runner",
                      skip_missing=False, work_dir='.')
