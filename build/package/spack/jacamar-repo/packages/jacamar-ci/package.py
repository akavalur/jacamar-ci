# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os
import re
from spack import *


class JacamarCi(MakefilePackage):
    """HPC focused CI/CD driver for the GitLab custom executor."""

    homepage = "https://gitlab.com/ecp-ci/jacamar-ci"
    url      = "https://gitlab.com/ecp-ci/jacamar-ci/-/archive/v0.5.0/jacamar-ci-v0.5.0.tar.gz"
    git      = "https://gitlab.com/ecp-ci/jacamar-ci.git"

    maintainers = ['paulbry', 'akavalur']
    executables = ['jacamar', 'jacamar-auth']

    version('develop', branch='develop')
    version('main', branch='main')
    version('0.7.0', sha256='0b8556a848d24a6125c790d1acc2044194655e45a832f91a8aae9d6a4835a6a4')
    version('0.6.0', sha256='73094517048c8171cb6a1c55cba40ab64223ccd36133930ca9f15a26301075f3')
    version('0.5.0', sha256='b274fda04199b1258edad25323370b0b4cd62a9eed52490f9e550447b0c92af5')

    depends_on('go@1.16:', type=('build'))
    depends_on('pkgconfig',type=('build'))
    depends_on('libseccomp@2.3.3', type=('run'))
    depends_on('gitlab-runner+jacamar', type=('run'))
    
    conflicts('platform=darwin', msg='Jacamar CI does not support MacOS')
    
    @classmethod
    def determine_spec_details(cls, prefix, exes_in_prefix):
        exe_to_path = dict(
            (os.path.basename(p), p) for p in exes_in_prefix
        )
        if 'jacamar' not in exe_to_path:
            return None

        jacamar = spack.util.executable.Executable(exe_to_path['jacamar'])
        output = jacamar('--version', output=str)
        if output:
            match = re.search(r'Version:\s*(\S+)', output)
            if match:
                version_str = match.group(1)
                return Spec('jacamar@{0}'.format(version_str))

    def build(self, spec, prefix):
        env['GOPATH'] = '{0}/go-path'.format(self.stage.source_path)
        mkdir(env['GOPATH'])
        if spec.version == Version('develop'):
            make('build') 
        else:
            make('build', 'VERSION={0}'.format(spec.version))
    
    def install(self, spec, prefix):
        make('install', 'PREFIX={0}'.format(prefix))

    def _test_target(self, target_exe):
        reason = "test installation of {0}".format(target_exe)
        self.run_test(target_exe, ['--version'], [], status=0,
                      installed=True, purpose=reason, skip_missing=False, work_dir='.')

    def test(self):
        """ Perform smoke test on the installation."""
        self._test_target('jacamar-auth')
        self._test_target('jacamar')
